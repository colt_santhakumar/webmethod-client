package com.colt.novitas.connection.response;

import java.io.Serializable;
import java.util.List;

public class ConnectionResponseResource implements Serializable {

	private static final long serialVersionUID = 1400331818644454200L;

	private String circuitName;
	private String circuitType;
	private String evcId;
	private String status;
	private String fromPortId;
	private String toPortId;
	private String lastUpdatedAt;
	// VLAN Related
	private String fromVLANMapping;
	private String toVLANMapping;
	private String fromVLANType;
	private String toVLANType;
	private List<ResourceVLANIdRange> fromPortVLANIdRange;
	private List<ResourceVLANIdRange> toPortVLANIdRange;

	public ConnectionResponseResource() {
		super();
	}

	public ConnectionResponseResource(String circuitName, String circuitType, String evcId, String status,
			String fromPortId, String toPortId, String fromUni, String toUni, String lastUpdatedAt) {
		super();
		this.circuitName = circuitName;
		this.circuitType = circuitType;
		this.evcId = evcId;
		this.status = status;
		this.fromPortId = fromPortId;
		this.toPortId = toPortId;
		this.lastUpdatedAt = lastUpdatedAt;
	}

	public String getCircuitName() {
		return circuitName;
	}

	public void setCircuitName(String circuitName) {
		this.circuitName = circuitName;
	}

	public String getCircuitType() {
		return circuitType;
	}

	public void setCircuitType(String circuitType) {
		this.circuitType = circuitType;
	}

	public String getEvcId() {
		return evcId;
	}

	public void setEvcId(String evcId) {
		this.evcId = evcId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFromPortId() {
		return fromPortId;
	}

	public void setFromPortId(String fromPortId) {
		this.fromPortId = fromPortId;
	}

	public String getToPortId() {
		return toPortId;
	}

	public void setToPortId(String toPortId) {
		this.toPortId = toPortId;
	}

	public String getLastUpdatedAt() {
		return lastUpdatedAt;
	}

	public void setLastUpdatedAt(String lastUpdatedAt) {
		this.lastUpdatedAt = lastUpdatedAt;
	}

	public String getFromVLANMapping() {
		return fromVLANMapping;
	}

	public void setFromVLANMapping(String fromVLANMapping) {
		this.fromVLANMapping = fromVLANMapping;
	}

	public String getToVLANMapping() {
		return toVLANMapping;
	}

	public void setToVLANMapping(String toVLANMapping) {
		this.toVLANMapping = toVLANMapping;
	}

	public String getFromVLANType() {
		return fromVLANType;
	}

	public void setFromVLANType(String fromVLANType) {
		this.fromVLANType = fromVLANType;
	}

	public String getToVLANType() {
		return toVLANType;
	}

	public void setToVLANType(String toVLANType) {
		this.toVLANType = toVLANType;
	}

	public List<ResourceVLANIdRange> getFromPortVLANIdRange() {
		return fromPortVLANIdRange;
	}

	public void setFromPortVLANIdRange(List<ResourceVLANIdRange> fromPortVLANIdRange) {
		this.fromPortVLANIdRange = fromPortVLANIdRange;
	}

	public List<ResourceVLANIdRange> getToPortVLANIdRange() {
		return toPortVLANIdRange;
	}

	public void setToPortVLANIdRange(List<ResourceVLANIdRange> toPortVLANIdRange) {
		this.toPortVLANIdRange = toPortVLANIdRange;
	}

	@Override
	public String toString() {
		return "ConnectionResponseResource [circuitName=" + circuitName + ", circuitType=" + circuitType + ", evcId="
				+ evcId + ", status=" + status + ", fromPortId=" + fromPortId + ", toPortId=" + toPortId
				+ ", lastUpdatedAt=" + lastUpdatedAt + ", fromVLANMapping=" + fromVLANMapping + ", toVLANMapping="
				+ toVLANMapping + ", fromVLANType=" + fromVLANType + ", toVLANType=" + toVLANType
				+ ", fromPortVLANIdRange=" + fromPortVLANIdRange + ", toPortVLANIdRange=" + toPortVLANIdRange + "]";
	}

}
