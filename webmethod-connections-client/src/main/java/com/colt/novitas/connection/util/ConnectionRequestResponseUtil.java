package com.colt.novitas.connection.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.colt.novitas.connection.response.ConnectionResponseResource;
import com.colt.novitas.connection.response.GetCreateConnectionResponse;
import com.colt.novitas.connection.response.NNICapacity;
import com.colt.novitas.connection.response.NNICircuitPairResponse;
import com.colt.novitas.connection.response.ResourceVLANIdRange;

import net.colt.xng.messages.v1.AttributeType;
import net.colt.xng.messages.v1.CircuitAttribType;
import net.colt.xng.messages.v1.GetConnectionDetailsResponse;
import net.colt.xng.messages.v1.NNICapInfo;
import net.colt.xng.messages.v1.UdaGroupType;

public class ConnectionRequestResponseUtil {
	
	public static ConnectionResponseResource getConnectionResponse(GetConnectionDetailsResponse connResp){
		
		ConnectionResponseResource connectionResponse = new ConnectionResponseResource();
		connectionResponse.setEvcId(connResp.getEvcid());
		connectionResponse.setCircuitType(connResp.getCircuitType());
		connectionResponse.setFromPortId(connResp.getAPortID());
		connectionResponse.setToPortId(connResp.getBPortID());
		connectionResponse.setStatus(connResp.getCircuitStatus().name());
		connectionResponse.setLastUpdatedAt(connResp.getLastmodtime());
		
		List<UdaGroupType> udaGroupTypeList = connResp.getUdaGroups();
		
		for(UdaGroupType udaGroupType: udaGroupTypeList){
			if(udaGroupType.getUdaGroupName().equals("Path - ETH")){
				List<AttributeType> vlanAttributeList = udaGroupType.getAttributes();
				for(AttributeType vlanAttribute: vlanAttributeList){
					if(StringUtils.isNotBlank(vlanAttribute.getName()) && vlanAttribute.getName().equals("A-VLAN ID Mapping") 
					   && StringUtils.isNotBlank(vlanAttribute.getValue()) && !"null".equalsIgnoreCase(vlanAttribute.getValue())){
						connectionResponse.setFromPortVLANIdRange(convertStringToVlanIdRange(vlanAttribute.getValue()));
					}
					if(StringUtils.isNotBlank(vlanAttribute.getName()) && vlanAttribute.getName().equals("A-VLAN EtherType")){
						connectionResponse.setFromVLANType(vlanAttribute.getValue());						
					}
					if(StringUtils.isNotBlank(vlanAttribute.getName()) && vlanAttribute.getName().equals("A-Endpoint Type")){
						connectionResponse.setFromVLANMapping(vlanAttribute.getValue());
					}
					if(StringUtils.isNotBlank(vlanAttribute.getName()) && vlanAttribute.getName().equals("B-VLAN ID Mapping") 
							&& StringUtils.isNotBlank(vlanAttribute.getValue()) && !"null".equalsIgnoreCase(vlanAttribute.getValue())){
						connectionResponse.setToPortVLANIdRange(convertStringToVlanIdRange(vlanAttribute.getValue()));
					}
					if(StringUtils.isNotBlank(vlanAttribute.getName()) && StringUtils.isNotBlank(vlanAttribute.getName()) && vlanAttribute.getName().equals("B-VLAN EtherType")){
						connectionResponse.setToVLANType(vlanAttribute.getValue());						
					}
					if(StringUtils.isNotBlank(vlanAttribute.getName()) && vlanAttribute.getName().equals("B-Endpoint Type")){
						connectionResponse.setToVLANMapping(vlanAttribute.getValue());
					}
				}
			}
		}
		return connectionResponse;
	}
	
	public static GetCreateConnectionResponse getGetCreateConnectionResponse(net.colt.xng.messages.v1.GetCreateConnectionResponse connResp){
		
		GetCreateConnectionResponse getCreateConnectionResponse = new GetCreateConnectionResponse();
		getCreateConnectionResponse.setCircuitId(connResp.getCircuitID());
		getCreateConnectionResponse.setaSideBPOInfo(connResp.getASideBPOInfo());
		getCreateConnectionResponse.setbSideBPOInfo(connResp.getBSideBPOInfo());
		
		return getCreateConnectionResponse;
	}

	private static List<ResourceVLANIdRange> convertStringToVlanIdRange(String vlanIdString) {

		List<ResourceVLANIdRange> rangeList = new ArrayList<ResourceVLANIdRange>();

		List<String> splitListByComma = Arrays.asList(vlanIdString.split(","));

		for (String str : splitListByComma) {
			List<String> range = Arrays.asList(str.split("-"));
			if (!range.isEmpty()) {
				ResourceVLANIdRange vlanRange = new ResourceVLANIdRange();
				vlanRange.setFromIdRange(Integer.parseInt(range.get(0)));
				if (range.size() == 2) {
					vlanRange.setToIdRange(Integer.parseInt(range.get(1)));
				} else {
					vlanRange.setToIdRange(null);
				}
				rangeList.add(vlanRange);
			}
		}
		return rangeList;
	}
	
	public static List<NNICircuitPairResponse> getNNICircuitPairResponse(List<CircuitAttribType> nniParis){
		List<NNICircuitPairResponse> nniCircuitPairResponseList = new ArrayList<NNICircuitPairResponse>();
		for (CircuitAttribType circuitAttribType : nniParis) {
			NNICircuitPairResponse response = new NNICircuitPairResponse();
			response.setCircuitId(circuitAttribType.getCircuitID());
			response.setPortId(circuitAttribType.getPortID());
			response.setPortName(circuitAttribType.getPortName());
			response.setSmartPortname(circuitAttribType.getSmartsPortName());
			response.setSiteId(circuitAttribType.getSiteID());
			nniCircuitPairResponseList.add(response);
		}
		return nniCircuitPairResponseList;
		
	}
	
	public static List<NNICapacity> convertToNNICapacity(List<NNICapInfo> nniCapInfos){
		List<NNICapacity> nniCapacities = new ArrayList<>();
		for (NNICapInfo nniCapInfo : nniCapInfos) {
			NNICapacity capacity = new NNICapacity();
			capacity.setAvailCapacity(nniCapInfo.getAvailCapacity());
			capacity.setNniCircuitID(nniCapInfo.getNNICircuitID());
			capacity.setSiteID(nniCapInfo.getSiteID());
			nniCapacities.add(capacity);
		}
		
		return nniCapacities;
		
	}
}
