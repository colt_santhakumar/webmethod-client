package com.colt.novitas.connection.request;

import java.io.Serializable;
import java.util.List;

import com.colt.novitas.connection.response.ResourceVLANIdRange;

public class UpdateConnectionRequest implements Serializable {

	private static final long serialVersionUID = -2181691915773201409L;

	private String circuitID;
	private String bandWidth;
	private String status;
	private String fromVLANMapping;
	private String toVLANMapping;
	private String fromVLANType;
	private String toVLANType;
	private List<ResourceVLANIdRange> fromPortVLANIdRange;
	private List<ResourceVLANIdRange> toPortVLANIdRange;
    private String evcId;
    private String comments;
    
	public UpdateConnectionRequest() {
		super();
	}

	public String getCircuitID() {
		return circuitID;
	}

	public void setCircuitID(String circuitID) {
		this.circuitID = circuitID;
	}

	public String getBandWidth() {
		return bandWidth;
	}

	public void setBandWidth(String bandWidth) {
		this.bandWidth = bandWidth;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFromVLANMapping() {
		return fromVLANMapping;
	}

	public void setFromVLANMapping(String fromVLANMapping) {
		this.fromVLANMapping = fromVLANMapping;
	}

	public String getToVLANMapping() {
		return toVLANMapping;
	}

	public void setToVLANMapping(String toVLANMapping) {
		this.toVLANMapping = toVLANMapping;
	}

	public String getFromVLANType() {
		return fromVLANType;
	}

	public void setFromVLANType(String fromVLANType) {
		this.fromVLANType = fromVLANType;
	}

	public String getToVLANType() {
		return toVLANType;
	}

	public void setToVLANType(String toVLANType) {
		this.toVLANType = toVLANType;
	}

	public List<ResourceVLANIdRange> getFromPortVLANIdRange() {
		return fromPortVLANIdRange;
	}

	public void setFromPortVLANIdRange(List<ResourceVLANIdRange> fromPortVLANIdRange) {
		this.fromPortVLANIdRange = fromPortVLANIdRange;
	}

	public List<ResourceVLANIdRange> getToPortVLANIdRange() {
		return toPortVLANIdRange;
	}

	public void setToPortVLANIdRange(List<ResourceVLANIdRange> toPortVLANIdRange) {
		this.toPortVLANIdRange = toPortVLANIdRange;
	}

	/**
	 * @return the evcId
	 */
	public String getEvcId() {
		return evcId;
	}

	/**
	 * @param evcId the evcId to set
	 */
	public void setEvcId(String evcId) {
		this.evcId = evcId;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	@Override
	public String toString() {
		return "UpdateConnectionRequest [circuitID=" + circuitID
				+ ", bandWidth=" + bandWidth + ", status=" + status
				+ ", fromVLANMapping=" + fromVLANMapping + ", toVLANMapping="
				+ toVLANMapping + ", fromVLANType=" + fromVLANType
				+ ", toVLANType=" + toVLANType + ", fromPortVLANIdRange="
				+ fromPortVLANIdRange + ", toPortVLANIdRange="
				+ toPortVLANIdRange + ", evcId=" + evcId + ", comments="
				+ comments + "]";
	}


}
