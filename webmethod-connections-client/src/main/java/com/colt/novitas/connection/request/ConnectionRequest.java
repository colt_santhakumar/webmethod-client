package com.colt.novitas.connection.request;

import java.io.Serializable;
import java.util.List;

import com.colt.novitas.connection.response.ResourceVLANIdRange;

public class ConnectionRequest implements Serializable {

	private static final long serialVersionUID = 4651521608395281519L;

	private String serviceConnectionId; // ServiceId
	private String customerName;
	private String customerOCN;
	private String bandwidth;
	private String fromPortId;
	private String toPortId;
	private String evcId;
	private String fromVLANMapping;
	private String toVLANMapping;
	private String fromVLANType;
	private String toVLANType;
	private List<ResourceVLANIdRange> fromPortVLANIdRange;
	private List<ResourceVLANIdRange> toPortVLANIdRange;
	private String urlCallback;
	private String circuitName;
	private String csp;
	private String serviceKey;
	private String fromLogicalPortId;
	private String circuitType;
	
	public String getServiceConnectionId() {
		return serviceConnectionId;
	}

	public void setServiceConnectionId(String serviceConnectionId) {
		this.serviceConnectionId = serviceConnectionId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerOCN() {
		return customerOCN;
	}

	public void setCustomerOCN(String customerOCN) {
		this.customerOCN = customerOCN;
	}

	public String getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(String bandwidth) {
		this.bandwidth = bandwidth;
	}

	public String getFromPortId() {
		return fromPortId;
	}

	public void setFromPortId(String fromPortId) {
		this.fromPortId = fromPortId;
	}

	public String getToPortId() {
		return toPortId;
	}

	public void setToPortId(String toPortId) {
		this.toPortId = toPortId;
	}

	public String getEvcId() {
		return evcId;
	}

	public void setEvcId(String evcId) {
		this.evcId = evcId;
	}

	public String getFromVLANMapping() {
		return fromVLANMapping;
	}

	public void setFromVLANMapping(String fromVLANMapping) {
		this.fromVLANMapping = fromVLANMapping;
	}

	public String getToVLANMapping() {
		return toVLANMapping;
	}

	public void setToVLANMapping(String toVLANMapping) {
		this.toVLANMapping = toVLANMapping;
	}

	public String getFromVLANType() {
		return fromVLANType;
	}

	public void setFromVLANType(String fromVLANType) {
		this.fromVLANType = fromVLANType;
	}

	public String getToVLANType() {
		return toVLANType;
	}

	public void setToVLANType(String toVLANType) {
		this.toVLANType = toVLANType;
	}

	public List<ResourceVLANIdRange> getFromPortVLANIdRange() {
		return fromPortVLANIdRange;
	}

	public void setFromPortVLANIdRange(List<ResourceVLANIdRange> fromPortVLANIdRange) {
		this.fromPortVLANIdRange = fromPortVLANIdRange;
	}

	public List<ResourceVLANIdRange> getToPortVLANIdRange() {
		return toPortVLANIdRange;
	}

	public void setToPortVLANIdRange(List<ResourceVLANIdRange> toPortVLANIdRange) {
		this.toPortVLANIdRange = toPortVLANIdRange;
	}

	public String getUrlCallback() {
		return urlCallback;
	}

	public void setUrlCallback(String urlCallback) {
		this.urlCallback = urlCallback;
	}
    
	
	public String getCircuitName() {
		return circuitName;
	}

	public void setCircuitName(String circuitName) {
		this.circuitName = circuitName;
	}

	public String getCsp() {
		return csp;
	}

	public void setCsp(String csp) {
		this.csp = csp;
	}

	public String getServiceKey() {
		return serviceKey;
	}

	public void setServiceKey(String serviceKey) {
		this.serviceKey = serviceKey;
	}

	public String getFromLogicalPortId() {
		return fromLogicalPortId;
	}

	public void setFromLogicalPortId(String fromLogicalPortId) {
		this.fromLogicalPortId = fromLogicalPortId;
	}

	public String getCircuitType() {
		return circuitType;
	}

	public void setCircuitType(String circuitType) {
		this.circuitType = circuitType;
	}

	@Override
	public String toString() {
		return "XNG ConnectionRequest [serviceConnectionId=" + serviceConnectionId
				+ ", customerName=" + customerName + ", customerOCN="
				+ customerOCN + ", bandwidth=" + bandwidth + ", fromPortId="
				+ fromPortId + ", toPortId=" + toPortId + ", evcId=" + evcId
				+ ", fromVLANMapping=" + fromVLANMapping + ", toVLANMapping="
				+ toVLANMapping + ", fromVLANType=" + fromVLANType
				+ ", toVLANType=" + toVLANType + ", fromPortVLANIdRange="
				+ fromPortVLANIdRange + ", toPortVLANIdRange="
				+ toPortVLANIdRange + ", urlCallback=" + urlCallback
				+ ", circuitName=" + circuitName + ", csp=" + csp
				+ ", serviceKey=" + serviceKey + ", fromLogicalPortId="
				+ fromLogicalPortId + ", circuitType=" + circuitType + "]";
	}

	
    
	

	
}
