package com.colt.novitas.connection.response;

import java.io.Serializable;

public class NNICapacity implements Serializable {
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String nniCircuitID;
    private String availCapacity;
    private String siteID;
    
	public String getNniCircuitID() {
		return nniCircuitID;
	}
	public void setNniCircuitID(String nniCircuitID) {
		this.nniCircuitID = nniCircuitID;
	}
	public String getAvailCapacity() {
		return availCapacity;
	}
	public void setAvailCapacity(String availCapacity) {
		this.availCapacity = availCapacity;
	}
	public String getSiteID() {
		return siteID;
	}
	public void setSiteID(String siteID) {
		this.siteID = siteID;
	}
	
	@Override
	public String toString() {
		return "NNICapacity [nniCircuitID=" + nniCircuitID + ", availCapacity="
				+ availCapacity + ", siteID=" + siteID + "]";
	}
    
    

}
