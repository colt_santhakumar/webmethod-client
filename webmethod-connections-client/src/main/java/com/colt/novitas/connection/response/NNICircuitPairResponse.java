package com.colt.novitas.connection.response;

import java.io.Serializable;

public class NNICircuitPairResponse implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2524644332454891125L;
	
	private String circuitId;
	private String portId;
	private String portName;
	private String smartPortname;
	private String siteId;
	/**
	 * @return the circuitId
	 */
	public String getCircuitId() {
		return circuitId;
	}
	/**
	 * @param circuitId the circuitId to set
	 */
	public void setCircuitId(String circuitId) {
		this.circuitId = circuitId;
	}
	/**
	 * @return the portId
	 */
	public String getPortId() {
		return portId;
	}
	/**
	 * @param portId the portId to set
	 */
	public void setPortId(String portId) {
		this.portId = portId;
	}
	/**
	 * @return the portName
	 */
	public String getPortName() {
		return portName;
	}
	/**
	 * @param portName the portName to set
	 */
	public void setPortName(String portName) {
		this.portName = portName;
	}
	/**
	 * @return the smartPortname
	 */
	public String getSmartPortname() {
		return smartPortname;
	}
	/**
	 * @param smartPortname the smartPortname to set
	 */
	public void setSmartPortname(String smartPortname) {
		this.smartPortname = smartPortname;
	}
	public String getSiteId() {
		return siteId;
	}
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	@Override
	public String toString() {
		return "NNICircuitPairResponse [circuitId=" + circuitId + ", portId="
				+ portId + ", portName=" + portName + ", smartPortname="
				+ smartPortname + ", siteId=" + siteId + "]";
	}
	
	
	
	

}
