package com.colt.novitas.connection.response;

import java.io.Serializable;

public class GetCreateConnectionResponse implements Serializable {

	private static final long serialVersionUID = 1400331818644454200L;

	private String circuitId;
	private String aSideBPOInfo;
	private String bSideBPOInfo;

	public GetCreateConnectionResponse() {
		super();
	}

	public GetCreateConnectionResponse(String circuitId, String aSideBPOInfo, String bSideBPOInfo) {
		super();
		this.circuitId = circuitId;
		this.aSideBPOInfo = aSideBPOInfo;
		this.bSideBPOInfo = bSideBPOInfo;
	}

	public String getCircuitId() {
		return circuitId;
	}

	public void setCircuitId(String circuitId) {
		this.circuitId = circuitId;
	}

	public String getaSideBPOInfo() {
		return aSideBPOInfo;
	}

	public void setaSideBPOInfo(String aSideBPOInfo) {
		this.aSideBPOInfo = aSideBPOInfo;
	}

	public String getbSideBPOInfo() {
		return bSideBPOInfo;
	}

	public void setbSideBPOInfo(String bSideBPOInfo) {
		this.bSideBPOInfo = bSideBPOInfo;
	}

	@Override
	public String toString() {
		return "GetCreateConnectionResponse [circuitId=" + circuitId + ", aSideBPOInfo=" + aSideBPOInfo
				+ ", bSideBPOInfo=" + bSideBPOInfo + "]";
	}
}
