package com.colt.novitas.connection.client;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;

import javax.xml.ws.BindingProvider;

import net.colt.novitasresourceinventory.connection.v1.CeaseConnectionShellRequest;
import net.colt.novitasresourceinventory.connection.v1.CeaseConnectionShellResponse;
import net.colt.novitasresourceinventory.connection.v1.Connection;
import net.colt.novitasresourceinventory.connection.v1.ConnectionPortType;
import net.colt.novitasresourceinventory.connection.v1.CreateConnectionShellRequest;
import net.colt.novitasresourceinventory.connection.v1.GetCSPNNISitesRequest;
import net.colt.novitasresourceinventory.connection.v1.GetConnectionDetailsRequest;
import net.colt.novitasresourceinventory.connection.v1.GetCreateConnectionRequest;
import net.colt.novitasresourceinventory.connection.v1.UpdateConnectionShellRequest;
import net.colt.novitasresourceinventory.connection.v1.UpdateConnectionShellResponse;
import net.colt.xml.ns.cse.v1.HeaderType;
import net.colt.xml.ns.cse.v1.StatusType;
import net.colt.xng.messages.v1.AttributeType;
import net.colt.xng.messages.v1.CircuitStatusEnumeration;
import net.colt.xng.messages.v1.GetAvailableCapacityRequest;
import net.colt.xng.messages.v1.GetAvailableCapacityResponse;
import net.colt.xng.messages.v1.GetCSPNNISitesResponse;
import net.colt.xng.messages.v1.GetConnectionDetailsResponse;
import net.colt.xng.messages.v1.GetNNICircuitsRequest;
import net.colt.xng.messages.v1.GetNNICircuitsResponse;
import net.colt.xng.messages.v1.InputHeaderType;
import net.colt.xng.messages.v1.OrganisationPartyType;
import net.colt.xng.messages.v1.UdaGroupType;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.colt.novitas.connection.request.ConnectionRequest;
import com.colt.novitas.connection.request.UpdateConnectionRequest;
import com.colt.novitas.connection.response.ConnectionResponseResource;
import com.colt.novitas.connection.response.GetCreateConnectionResponse;
import com.colt.novitas.connection.response.ResourceVLANIdRange;
import com.colt.novitas.connection.util.ConnectionRequestResponseUtil;
import com.colt.novitas.property.SystemName;
import com.colt.novitas.soap.SoapClient;

public class ConnectionAPIClient {

	private String CONNECTON_SOAP_URL = "http://amsnov02:8080/webmethod-mock-api/xngconnection?wsdl";
	private String CONNECTION_SOAP_USERNAME = "jBPMUser";
	private String CONNECTION_SOAP_PASSWORD = "jBPMUser";
	private static final Logger LOG = Logger.getLogger("Webmethod Client|"+ConnectionAPIClient.class.getSimpleName());
    private SoapClient soapClient =new SoapClient();
	public ConnectionAPIClient(final String RESOURCE_SOAP_URL) {
		this.CONNECTON_SOAP_URL = RESOURCE_SOAP_URL;
	}

	public ConnectionAPIClient(String CONNECTON_SOAP_URL, String CONNECTION_SOAP_USERNAME, String CONNECTION_SOAP_PASSWORD) {
		this.CONNECTON_SOAP_URL = CONNECTON_SOAP_URL;
		this.CONNECTION_SOAP_USERNAME = CONNECTION_SOAP_USERNAME;
		this.CONNECTION_SOAP_PASSWORD = CONNECTION_SOAP_PASSWORD;
	}

	public ConnectionAPIClient() {
	}

	public Object retrieveConnection(String circuitId) {

		LOG.info("Start >>>  RetrieveConnection");
		GetConnectionDetailsRequest getConnectionDetailsRequest = new GetConnectionDetailsRequest();
		net.colt.xng.messages.v1.GetConnectionDetailsRequest getConnectionDetails = new net.colt.xng.messages.v1.GetConnectionDetailsRequest();
		getConnectionDetailsRequest.setGetConnectionDetailsRequest(getConnectionDetails);
		InputHeaderType inputHeaderType = new InputHeaderType();
		inputHeaderType.setSenderSystem("Novitas");
		getConnectionDetails.setServiceHeader(inputHeaderType);
		if (circuitId == null)
			return new ConnectionResponseResource();
		getConnectionDetails.setCircuitID(circuitId);

		try {
			ConnectionPortType serviceConnection = soapClient.getServiceObject(CONNECTON_SOAP_URL, Connection.SERVICE,
					ConnectionPortType.class);
			Map<String, Object> req_ctx = ((BindingProvider) serviceConnection).getRequestContext();
			soapClient.setAuthenticationToken(req_ctx, CONNECTION_SOAP_USERNAME, CONNECTION_SOAP_PASSWORD);
			soapClient.addLogHandler(serviceConnection);
			soapClient.setTimeOutValues(req_ctx, SystemName.XNG_CONNECTION);
			
			GetConnectionDetailsResponse response = serviceConnection.retrieveConnection(getConnectionDetailsRequest).getGetConnectionDetailsResponse();
			HeaderType outputHeader = response.getOutputHeader();
			LOG.info("RetrieveConnection details response status :: " + outputHeader.getStatus());
			if(outputHeader.getStatus().equals(StatusType.SUCCESS)){
			   return ConnectionRequestResponseUtil.getConnectionResponse(response);
			}else{
				LOG.info("Failed to RetrieveConnection details ::"+outputHeader.getErrorMessage().getErrorDescription());
				return null;
			}
			
		} catch (Exception ex) {
			LOG.error("Error >>>  RetrieveConnection" + ex.getMessage());
			return null;
		}
	}

	public String createConnection(ConnectionRequest connectionRequest) {
		LOG.info("Start >>>  CreateConnection");

		CreateConnectionShellRequest createConnectionShellRequest = new CreateConnectionShellRequest();

		net.colt.xng.messages.v1.CreateConnectionShellRequest connRequest = new net.colt.xng.messages.v1.CreateConnectionShellRequest();
		createConnectionShellRequest.setCreateConnectionShellRequest(connRequest);

		InputHeaderType inputHeaderType = new InputHeaderType();
		inputHeaderType.setSenderSystem("Novitas");
		connRequest.setServiceHeader(inputHeaderType);

		connRequest.setTopology("P");
		connRequest.setSlaId("1");
		connRequest.setCircuitType("LANLINK");
		connRequest.setNumberOfCircuits(BigInteger.valueOf(1));
		connRequest.setNbrChannels("0");
		connRequest.setServiceID(connectionRequest.getServiceConnectionId());
		if(StringUtils.isNotBlank(connectionRequest.getFromPortId())){
		  connRequest.setAPortID(connectionRequest.getFromPortId());
		}
		connRequest.setBPortID(connectionRequest.getToPortId().toString());
		connRequest.setBandWidth(connectionRequest.getBandwidth());
		connRequest.setUrlCallback(connectionRequest.getUrlCallback());
		connRequest.setEvcid(connectionRequest.getEvcId());
		if(StringUtils.isNotBlank(connectionRequest.getFromLogicalPortId())){
		  connRequest.setALogicalPortID(connectionRequest.getFromLogicalPortId());
		}
		OrganisationPartyType orgPartyType = new OrganisationPartyType();
		orgPartyType.setPartyNumber(connectionRequest.getCustomerOCN());
		orgPartyType.setPartyName(connectionRequest.getCustomerName());
		connRequest.setLegalCustomerParty(orgPartyType);
		
		if(StringUtils.isNotBlank(connectionRequest.getCsp()) && StringUtils.isNotBlank(connectionRequest.getServiceKey())){
			
			UdaGroupType allCircuitGroupType = new UdaGroupType();
			allCircuitGroupType.setUdaGroupName("All Circuit Paths");
			List<AttributeType> allCircuitsList = allCircuitGroupType.getAttributes();

			List<UdaGroupType> allCircuitList = connRequest.getUdaGroups();
			allCircuitList.add(allCircuitGroupType);
			
			AttributeType cspName = new AttributeType();
			cspName.setName("Cloud Service Provider Name");
			if("AZURE_EXPRESS".equals(connectionRequest.getCsp())){
				cspName.setValue("Microsoft Azure"); 
			}else if("AWS_HOSTED".equals(connectionRequest.getCsp()) || "AWS_DEDICATED".equals(connectionRequest.getCsp())){
				 cspName.setValue("Amazon Web Services");
			}
			allCircuitsList.add(cspName);
			
			AttributeType serviceKey = new AttributeType();
			serviceKey.setName("Cloud Service Provider Cust Id");
			serviceKey.setValue(connectionRequest.getServiceKey());
			allCircuitsList.add(serviceKey);
			
		}

		if (connectionRequest.getFromVLANMapping() != null || connectionRequest.getToVLANMapping() != null
				|| connectionRequest.getFromVLANType() != null || connectionRequest.getToVLANType() != null
				|| connectionRequest.getFromPortVLANIdRange() !=  null && connectionRequest.getFromPortVLANIdRange().size() > 0
				|| connectionRequest.getToPortVLANIdRange() != null && connectionRequest.getToPortVLANIdRange().size() > 0) {

			UdaGroupType udaGroupType = new UdaGroupType();
			udaGroupType.setUdaGroupName("Path - ETH");
			List<AttributeType> attributeList = udaGroupType.getAttributes();

			List<UdaGroupType> udaGroupTypeList = connRequest.getUdaGroups();
			udaGroupTypeList.add(udaGroupType);

			if (connectionRequest.getFromVLANType() != null) {
				AttributeType vlanAttribute1 = new AttributeType();
				vlanAttribute1.setName("A-VLAN EtherType");
				vlanAttribute1.setValue(connectionRequest.getFromVLANType());
				attributeList.add(vlanAttribute1);
			}
			if (connectionRequest.getFromPortVLANIdRange() != null && connectionRequest.getFromPortVLANIdRange().size() > 0) {
				AttributeType vlanAttribute2 = new AttributeType();
				vlanAttribute2.setName("A-VLAN ID Mapping");
				if (connectionRequest.getFromPortVLANIdRange().size() > 0) {
					vlanAttribute2.setValue(convertVlanIdRangesToString(connectionRequest.getFromPortVLANIdRange()));
				} else
					vlanAttribute2.setValue(null);
				attributeList.add(vlanAttribute2);
			}
			if (connectionRequest.getFromVLANMapping() != null) {
				AttributeType vlanAttribute3 = new AttributeType();
				vlanAttribute3.setName("A-Endpoint Type");
				vlanAttribute3.setValue(connectionRequest.getFromVLANMapping());
				attributeList.add(vlanAttribute3);
			}
			if (connectionRequest.getToVLANType() != null) {
				AttributeType vlanAttribute4 = new AttributeType();
				vlanAttribute4.setName("B-VLAN EtherType");
				vlanAttribute4.setValue(connectionRequest.getToVLANType());
				attributeList.add(vlanAttribute4);
			}
			if (connectionRequest.getToPortVLANIdRange() != null && connectionRequest.getToPortVLANIdRange().size() > 0) {
				AttributeType vlanAttribute5 = new AttributeType();
				vlanAttribute5.setName("B-VLAN ID Mapping");
				if (connectionRequest.getToPortVLANIdRange().size() > 0) {
					vlanAttribute5.setValue(convertVlanIdRangesToString(connectionRequest.getToPortVLANIdRange()));
				} else
					vlanAttribute5.setValue(null);
				attributeList.add(vlanAttribute5);
			}
			
			if (connectionRequest.getToVLANMapping() != null) {
				AttributeType vlanAttribute6 = new AttributeType();
				vlanAttribute6.setName("B-Endpoint Type");
				vlanAttribute6.setValue(connectionRequest.getToVLANMapping());
				attributeList.add(vlanAttribute6);
			}
			
		}
		try {
			ConnectionPortType serviceConnection = soapClient.getServiceObject(CONNECTON_SOAP_URL, Connection.SERVICE,
					ConnectionPortType.class);
			Map<String, Object> req_ctx = ((BindingProvider) serviceConnection).getRequestContext();
			soapClient.setAuthenticationToken(req_ctx, CONNECTION_SOAP_USERNAME, CONNECTION_SOAP_PASSWORD);
			soapClient.addLogHandler(serviceConnection);
			soapClient.setTimeOutValues(req_ctx, SystemName.XNG_CONNECTION);
			HeaderType outputHeader  = serviceConnection.createConnection(createConnectionShellRequest).getCreateConnectionShellResponse().getOutputHeader();
            if(outputHeader.getStatus().equals(StatusType.SUCCESS) && StringUtils.isNotBlank(outputHeader.getTransactionId())){
            	LOG.info("CreateConnection Transaction ID |" + outputHeader.getTransactionId());
            	return outputHeader.getTransactionId();
            }
            LOG.info("Failed to CreateConnection|"+outputHeader.getErrorMessage().getErrorDescription());
			return "FAILED";

		} catch (Exception ex) {
			LOG.error("Error >>> CreateConnection|" + ex.getMessage());
			return "FAILED";
		}

	}

	public Object updateConnection(UpdateConnectionRequest updateConnectionRequest) {
		LOG.info("Start >>>  ConnectionAPIClient.updateConnection()");

		UpdateConnectionShellRequest updateConnectionShellRequest = new UpdateConnectionShellRequest();

		net.colt.xng.messages.v1.UpdateConnectionShellRequest connRequest = new net.colt.xng.messages.v1.UpdateConnectionShellRequest();
		updateConnectionShellRequest.setUpdateConnectionShellRequest(connRequest);

		InputHeaderType inputHeaderType = new InputHeaderType();
		inputHeaderType.setSenderSystem("Novitas");
		connRequest.setServiceHeader(inputHeaderType);
        if(StringUtils.isNotBlank(updateConnectionRequest.getStatus())){
        	connRequest.setCktStatus(CircuitStatusEnumeration.valueOf(CircuitStatusEnumeration.class, updateConnectionRequest.getStatus().toUpperCase()));
        }
		
		connRequest.setCircuitID(updateConnectionRequest.getCircuitID());
		connRequest.setBandWidth(updateConnectionRequest.getBandWidth());
		//connRequest.setEvcid(updateConnectionRequest.getEvcId());
		
		if(StringUtils.isNotBlank(updateConnectionRequest.getComments())){
			connRequest.setComments(updateConnectionRequest.getComments());
		}
	
		List<UdaGroupType> allCircuitList = connRequest.getUdaGroups();
		UdaGroupType allCircuitGroupType = new UdaGroupType();
		allCircuitGroupType.setUdaGroupName("All Circuit Paths");
		List<AttributeType> allCircuitsList = allCircuitGroupType.getAttributes();
		
		if (updateConnectionRequest.getEvcId() != null) {
			AttributeType evc = new AttributeType();
			evc.setName("Network Object ID");
			evc.setValue(updateConnectionRequest.getEvcId());
			allCircuitsList.add(evc);
			allCircuitList.add(allCircuitGroupType);
		}
		
		if (updateConnectionRequest.getFromVLANMapping() != null || updateConnectionRequest.getToVLANMapping() != null
				|| updateConnectionRequest.getFromVLANType() != null || updateConnectionRequest.getToVLANType() != null) {

			UdaGroupType udaGroupType = new UdaGroupType();
			udaGroupType.setUdaGroupName("Path - ETH");
			List<AttributeType> vlanAttributeList = udaGroupType.getAttributes();

			List<UdaGroupType> udaGroupTypeList = connRequest.getUdaGroups();
			udaGroupTypeList.add(udaGroupType);

			if (updateConnectionRequest.getFromVLANType() != null) {
				AttributeType vlanAttribute1 = new AttributeType();
				vlanAttribute1.setName("A-VLAN EtherType");
				vlanAttribute1.setValue(updateConnectionRequest.getFromVLANType());
				vlanAttributeList.add(vlanAttribute1);
			}
			if (updateConnectionRequest.getFromPortVLANIdRange() != null && updateConnectionRequest.getFromPortVLANIdRange().size() > 0) {
				AttributeType vlanAttribute2 = new AttributeType();
				vlanAttribute2.setName("A-VLAN ID Mapping");
				vlanAttribute2.setValue(convertVlanIdRangesToString(updateConnectionRequest.getFromPortVLANIdRange()));
				vlanAttributeList.add(vlanAttribute2);
			}
			if (updateConnectionRequest.getFromVLANMapping() != null) {
				AttributeType vlanAttribute3 = new AttributeType();
				vlanAttribute3.setName("A-Endpoint Type");
				vlanAttribute3.setValue(updateConnectionRequest.getFromVLANMapping());
				vlanAttributeList.add(vlanAttribute3);
				if("P".equalsIgnoreCase(updateConnectionRequest.getFromVLANMapping())){
					AttributeType vlanAttribute2 = new AttributeType();
					vlanAttribute2.setName("A-VLAN ID Mapping");
					vlanAttribute2.setValue("null");
					vlanAttributeList.add(vlanAttribute2);
					
					AttributeType vlanAttribute1 = new AttributeType();
					vlanAttribute1.setName("A-VLAN EtherType");
					vlanAttribute1.setValue("null");
					vlanAttributeList.add(vlanAttribute1);
				}
			}
			if (updateConnectionRequest.getToVLANType() != null) {
				AttributeType vlanAttribute4 = new AttributeType();
				vlanAttribute4.setName("B-VLAN EtherType");
				vlanAttribute4.setValue(updateConnectionRequest.getToVLANType());
				vlanAttributeList.add(vlanAttribute4);
			}
			if (updateConnectionRequest.getToPortVLANIdRange() != null && updateConnectionRequest.getToPortVLANIdRange().size() > 0) {
				AttributeType vlanAttribute5 = new AttributeType();
				vlanAttribute5.setName("B-VLAN ID Mapping");
				vlanAttribute5.setValue(convertVlanIdRangesToString(updateConnectionRequest.getToPortVLANIdRange()));
				vlanAttributeList.add(vlanAttribute5);
			}
			if (updateConnectionRequest.getToVLANMapping() != null) {
				AttributeType vlanAttribute6 = new AttributeType();
				vlanAttribute6.setName("B-Endpoint Type");
				vlanAttribute6.setValue(updateConnectionRequest.getToVLANMapping());
				vlanAttributeList.add(vlanAttribute6);
				if("P".equalsIgnoreCase(updateConnectionRequest.getToVLANMapping())){
					AttributeType vlanAttribute5 = new AttributeType();
					vlanAttribute5.setName("B-VLAN ID Mapping");
					vlanAttribute5.setValue("null");
					vlanAttributeList.add(vlanAttribute5);
					
					AttributeType vlanAttribute4 = new AttributeType();
					vlanAttribute4.setName("B-VLAN EtherType");
					vlanAttribute4.setValue("null");
					vlanAttributeList.add(vlanAttribute4);
				}
			}
			
		}

		try {
			ConnectionPortType serviceConnection = soapClient.getServiceObject(CONNECTON_SOAP_URL, Connection.SERVICE,
					ConnectionPortType.class);
			Map<String, Object> req_ctx = ((BindingProvider) serviceConnection).getRequestContext();
			soapClient.setAuthenticationToken(req_ctx, CONNECTION_SOAP_USERNAME, CONNECTION_SOAP_PASSWORD);
			soapClient.addLogHandler(serviceConnection);
			soapClient.setTimeOutValues(req_ctx, SystemName.XNG_CONNECTION);
			
			UpdateConnectionShellResponse updateConnectionShellResponse = serviceConnection
					.updateConnection(updateConnectionShellRequest);

			HeaderType header = updateConnectionShellResponse.getUpdateConnectionShellResponse().getOutputHeader();
			if(header.getStatus().equals(StatusType.SUCCESS)){
				return "SUCCESS";
			}else{
				LOG.info("Failed to Update Connection ::"+header.getErrorMessage().getErrorDescription());
				return "FAILED";
			}

		} catch (Exception ex) {
			LOG.error("Error >>>  ConnectionAPIClient.updateConnection()" + ex.getMessage());
			return "FAILED";
		}
	}

	public String ceaseConnection(String circuitId) {
		LOG.info("Start >>>  ConnectionAPIClient.ceaseConnection()");

		CeaseConnectionShellRequest ceaseConnectionShellRequest = new CeaseConnectionShellRequest();

		net.colt.xng.messages.v1.CeaseConnectionShellRequest ceaseConnRequest = new net.colt.xng.messages.v1.CeaseConnectionShellRequest();
		ceaseConnectionShellRequest.setCeaseConnectionShellRequest(ceaseConnRequest);

		InputHeaderType inputHeaderType = new InputHeaderType();
		inputHeaderType.setSenderSystem("Novitas");
		ceaseConnRequest.setServiceHeader(inputHeaderType);
		ceaseConnRequest.setCircuitID(circuitId);

		try {
			ConnectionPortType serviceConnection = soapClient.getServiceObject(CONNECTON_SOAP_URL, Connection.SERVICE,
					                                                               ConnectionPortType.class);
			Map<String, Object> req_ctx = ((BindingProvider) serviceConnection).getRequestContext();
			soapClient.setAuthenticationToken(req_ctx, CONNECTION_SOAP_USERNAME, CONNECTION_SOAP_PASSWORD);
			soapClient.addLogHandler(serviceConnection);
			soapClient.setTimeOutValues(req_ctx, SystemName.XNG_CONNECTION);
			CeaseConnectionShellResponse ceaseConnectionShellResponse = serviceConnection.ceaseConnection(ceaseConnectionShellRequest);
			HeaderType headerType = ceaseConnectionShellResponse.getCeaseConnectionShellResponse().getOutputHeader();
			if(headerType.getStatus().equals(StatusType.SUCCESS)){
			   	return headerType.getStatus().name();
			}
			LOG.info("Failed to cease the Connection ::"+headerType.getErrorMessage().getErrorDescription());
			return "FAILED";
		} catch (Exception ex) {
			LOG.error("Error >>>  ConnectionAPIClient.ceaseConnection()" + ex.getMessage());
			return "FAILED";
		}
	}

	public Object getCreateConnectionStatusRequest(String transactionId){
		LOG.info("Start >>>  ConnectionAPIClient.getCreateConnectionStatusRequest()");

		GetCreateConnectionRequest getCreateConnectionRequest = new GetCreateConnectionRequest();

		net.colt.xng.messages.v1.GetCreateConnectionRequest getCreateConnectionRequestStatus = new net.colt.xng.messages.v1.GetCreateConnectionRequest();
		getCreateConnectionRequest.setGetCreateConnectionRequest(getCreateConnectionRequestStatus);

		InputHeaderType inputHeaderType = new InputHeaderType();
		inputHeaderType.setSenderSystem("Novitas");
		getCreateConnectionRequestStatus.setServiceHeader(inputHeaderType);
		getCreateConnectionRequestStatus.setTransactionID(transactionId);

		try {
			ConnectionPortType serviceConnection = soapClient.getServiceObject(CONNECTON_SOAP_URL, Connection.SERVICE,
					ConnectionPortType.class);
			Map<String, Object> req_ctx = ((BindingProvider) serviceConnection).getRequestContext();
			soapClient.setAuthenticationToken(req_ctx, CONNECTION_SOAP_USERNAME, CONNECTION_SOAP_PASSWORD);
			soapClient.setTimeOutValues(req_ctx, SystemName.XNG_CONNECTION);
			soapClient.addLogHandler(serviceConnection);
			 net.colt.xng.messages.v1.GetCreateConnectionResponse response = serviceConnection.getCreateConnectionStatusRequest(getCreateConnectionRequest).getGetCreateConnectionResponse();
            if(response.getOutputHeader().getStatus().equals(StatusType.SUCCESS) && StringUtils.isNotBlank(response.getCircuitID())){
            	LOG.info("Circuit Id :: "+ response.getCircuitID());
            	LOG.info("A Side BPO Info :: "+ response.getASideBPOInfo());
            	LOG.info("B Side BPO Info :: "+ response.getBSideBPOInfo());
            	return  ConnectionRequestResponseUtil.getGetCreateConnectionResponse(response);
            }
            LOG.info("Failed to get the Circuit ID ::"+response.getOutputHeader().getErrorMessage().getErrorDescription());
			return "FAILED";

		} catch (Exception ex) {
			LOG.error("Error >>>  ConnectionAPIClient.getCreateConnectionStatusRequest()" + ex.getMessage());
			return "FAILED";
		}
	}
	
	public Object getNNICircuitPair(String priInterfaceName,String secInterfacename){
		
		LOG.info("Start >>>GetNNICircuits");
		net.colt.novitasresourceinventory.connection.v1.GetNNICircuitsRequest getNNICircuitPairRequest = new  net.colt.novitasresourceinventory.connection.v1.GetNNICircuitsRequest();
		GetNNICircuitsRequest nniCircuitPairRequest = new GetNNICircuitsRequest(); 
		getNNICircuitPairRequest.setGetNNICircuitsRequest(nniCircuitPairRequest);

		InputHeaderType inputHeaderType = new InputHeaderType();
		inputHeaderType.setSenderSystem("Novitas");
		nniCircuitPairRequest.setServiceHeader(inputHeaderType);
		nniCircuitPairRequest.setPriInterfaceName(priInterfaceName);
		if(StringUtils.isNotBlank(secInterfacename))
		   nniCircuitPairRequest.setSecInterfaceName(secInterfacename);
		
		try {
			ConnectionPortType serviceConnection = soapClient.getServiceObject(CONNECTON_SOAP_URL, Connection.SERVICE,
					                                                           ConnectionPortType.class);
			Map<String, Object> req_ctx = ((BindingProvider) serviceConnection).getRequestContext();
			soapClient.setAuthenticationToken(req_ctx, CONNECTION_SOAP_USERNAME, CONNECTION_SOAP_PASSWORD);
			soapClient.setTimeOutValues(req_ctx, SystemName.XNG_CONNECTION);
			soapClient.addLogHandler(serviceConnection);
			GetNNICircuitsResponse  response = serviceConnection.getNNICircuits(getNNICircuitPairRequest).getGetNNICircuitsResponse();
            if(response.getOutputHeader().getStatus().equals(StatusType.SUCCESS) && !response.getNNICircuit().isEmpty() && response.getNNICircuit().size() > 0){
            	LOG.info("Received Circuit Id are:: "+ response.getNNICircuit().size());
            	return ConnectionRequestResponseUtil.getNNICircuitPairResponse(response.getNNICircuit());
            }
            LOG.info("Failed to get GetNNICircuits|"+response.getOutputHeader().getErrorMessage().getErrorDescription());
			return "FAILED";

		} catch (Exception ex) {
			LOG.error("Error >>>GetNNICircuits|" + ex.getMessage());
			return "FAILED";
		}
		
		
	}
	
	  
	public Object getCSPNNISites(String cspName,String cspOption){
		
		GetCSPNNISitesRequest cSPNNISitesRequest = new GetCSPNNISitesRequest();
		net.colt.xng.messages.v1.GetCSPNNISitesRequest getCSPNNISitesRequest = new net.colt.xng.messages.v1.GetCSPNNISitesRequest ();
		cSPNNISitesRequest.setGetCSPNNISitesRequest(getCSPNNISitesRequest);
		InputHeaderType inputHeaderType = new InputHeaderType();
		inputHeaderType.setSenderSystem("Novitas");
		getCSPNNISitesRequest.setServiceHeader(inputHeaderType);
		if(StringUtils.isBlank(cspName) || StringUtils.isBlank(cspOption)){
			LOG.error("Error >>>  CSPName or CSP Option should not be empty");
			return "FAILED";
		}
		getCSPNNISitesRequest.setCspName(cspName);
		getCSPNNISitesRequest.setCspOption(cspOption);
		try {
			ConnectionPortType serviceConnection = soapClient.getServiceObject(CONNECTON_SOAP_URL, Connection.SERVICE,
					                                                           ConnectionPortType.class);
			Map<String, Object> req_ctx = ((BindingProvider) serviceConnection).getRequestContext();
			soapClient.setAuthenticationToken(req_ctx, CONNECTION_SOAP_USERNAME, CONNECTION_SOAP_PASSWORD);
			soapClient.setTimeOutValues(req_ctx, SystemName.XNG_CONNECTION);
			soapClient.addLogHandler(serviceConnection);
			GetCSPNNISitesResponse  response = serviceConnection.getCSPNNISites(cSPNNISitesRequest).getGetCSPNNISitesResponse();
            if(response.getOutputHeader().getStatus().equals(StatusType.SUCCESS) && !response.getRegion().isEmpty()){
            	LOG.info("Received CSP NNI Sites size :: "+ response.getRegion().size());
            	return response.getRegion();
            }
            LOG.info("Failed to get GetCSPNNISites ::"+response.getOutputHeader().getErrorMessage().getErrorDescription());
			return "FAILED";

		} catch (Exception ex) {
			LOG.error("Error >>>  GetCSPNNISites" + ex.getMessage());
			return "FAILED";
		}
		
	}

	    
     public Object getAvailableCapacity(String regionName,String siteId){
    	 net.colt.novitasresourceinventory.connection.v1.GetAvailableCapacityRequest getAvailableCapacityRequest = 
    			 new net.colt.novitasresourceinventory.connection.v1.GetAvailableCapacityRequest ();
    	 GetAvailableCapacityRequest request = new GetAvailableCapacityRequest();
    	 getAvailableCapacityRequest.setGetAvailableCapacityRequest(request);
    	 InputHeaderType inputHeaderType = new InputHeaderType();
 		 inputHeaderType.setSenderSystem("Novitas");
 		 request.setServiceHeader(inputHeaderType);
 		 if(StringUtils.isBlank(siteId) || StringUtils.isBlank(regionName)){
 			LOG.error("Error >>>  Region name or Site Id Should not be empty");
			return "FAILED"; 
 		 }
 		 request.setRegionName(regionName);
 		 request.setSiteID(siteId);
 		try {
			ConnectionPortType serviceConnection = soapClient.getServiceObject(CONNECTON_SOAP_URL, Connection.SERVICE,
					                                                           ConnectionPortType.class);
			Map<String, Object> req_ctx = ((BindingProvider) serviceConnection).getRequestContext();
			soapClient.setAuthenticationToken(req_ctx, CONNECTION_SOAP_USERNAME, CONNECTION_SOAP_PASSWORD);
			soapClient.setTimeOutValues(req_ctx, SystemName.XNG_CONNECTION);
			soapClient.addLogHandler(serviceConnection);
			GetAvailableCapacityResponse  response = serviceConnection.getAvailableCapacity(getAvailableCapacityRequest).getGetAvailableCapacityResponse();
            if(response.getOutputHeader().getStatus().equals(StatusType.SUCCESS) && !response.getNNI().isEmpty()){
            	LOG.info("Received Available Capacity :: "+ response.getNNI().size());
            	return ConnectionRequestResponseUtil.convertToNNICapacity(response.getNNI());
            }
            LOG.info("Failed to get the GetAvailableCapacity ::"+response.getOutputHeader().getErrorMessage().getErrorDescription());
			return "FAILED";

		} catch (Exception ex) {
			LOG.error("Error >>>  GetAvailableCapacity()" + ex.getMessage());
			return "FAILED";
		}
 		 
     }
	
	private String convertVlanIdRangesToString(List<ResourceVLANIdRange> vlanRanges) {

		StringBuffer sb = new StringBuffer();
		String comma = null;
		for (ResourceVLANIdRange range : vlanRanges) {
			if (comma != null) {
				sb.append(comma);
			}
			sb.append(range.getFromIdRange());
			if (range.getToIdRange() != null) {
				sb.append("-");
				sb.append(range.getToIdRange());
			}
			comma = ",";
		}
		return sb.toString();
	}
	
	public static void main(String[] args) {
		
		ConnectionAPIClient conn = new ConnectionAPIClient();
		
		//System.out.println("Hell"+StatusType.SUCCESS.equals(StatusType.SUCCESS));
		
		
		ConnectionRequest connectionRequest = new ConnectionRequest();
		connectionRequest.setCustomerName("TEST CUSTOMER");
		connectionRequest.setCustomerOCN("TEST OCN");
		connectionRequest.setServiceConnectionId("101");
		connectionRequest.setFromPortId("3");
		connectionRequest.setToPortId("4");
		connectionRequest.setBandwidth("1000");
		connectionRequest.setFromVLANMapping("P");
		connectionRequest.setUrlCallback("URL:http://ABC");	
		
		//conn.createConnection(connectionRequest);
		//conn.getCreateConnectionStatusRequest("b229d5c0-87dd-415c-8655-73a924d52804");
		conn.getNNICircuitPair("FRA00095/002", "");
		
		/*UpdateConnectionRequest updateConn = new UpdateConnectionRequest();
		updateConn.setCircuitID("26");
		updateConn.setBandWidth("2000");*/
		
		//conn.updateConnection(updateConn);
		
		//conn.ceaseConnection("26");
	}

}
