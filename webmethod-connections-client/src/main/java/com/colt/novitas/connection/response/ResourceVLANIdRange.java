package com.colt.novitas.connection.response;

import java.io.Serializable;

public class ResourceVLANIdRange implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2469315164425642767L;
	private Integer fromIdRange;
	private Integer toIdRange;

	public ResourceVLANIdRange() {
		super();
	}

	public Integer getFromIdRange() {
		return fromIdRange;
	}

	public void setFromIdRange(Integer fromIdRange) {
		this.fromIdRange = fromIdRange;
	}

	public Integer getToIdRange() {
		return toIdRange;
	}

	public void setToIdRange(Integer toIdRange) {
		this.toIdRange = toIdRange;
	}

	public ResourceVLANIdRange(Integer fromIdRange, Integer toIdRange) {
		super();
		this.fromIdRange = fromIdRange;
		this.toIdRange = toIdRange;
	}

	@Override
	public String toString() {
		return "ResourceVLANIdRange [fromIdRange=" + fromIdRange
				+ ", toIdRange=" + toIdRange + "]";
	}
	
	
}
