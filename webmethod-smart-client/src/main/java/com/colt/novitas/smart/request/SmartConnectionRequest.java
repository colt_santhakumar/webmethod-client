package com.colt.novitas.smart.request;

import java.io.Serializable;

public class SmartConnectionRequest implements Serializable {

	private static final long serialVersionUID = 4651521608395281519L;

	private String operationType;
	private String evcServiceID;
	private String circuitID;
	private String serviceType;
	private String serviceTopology;
	private String serviceStatus;
	private String customerOCN;
	private String customerName;
	private String urlCallback;
	private String aDeviceID;
	private String aDeviceIPAddress;
	private String bDeviceID;
	private String bDeviceIPAddress;
	private String aPortID;
	private String aSmartsPortName;
	private String bPortID;
	private String bSmartsPortName;
	private String aDeviceCountryCode;
	private String aDeviceCityCode;
	private String bDeviceCountryCode;
	private String bDeviceCityCode;

	public SmartConnectionRequest() {
		super();
	}

	public String getEvcServiceID() {
		return evcServiceID;
	}

	public void setEvcServiceID(String evcServiceID) {
		this.evcServiceID = evcServiceID;
	}

	public String getCircuitID() {
		return circuitID;
	}

	public void setCircuitID(String circuitID) {
		this.circuitID = circuitID;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getServiceTopology() {
		return serviceTopology;
	}

	public void setServiceTopology(String serviceTopology) {
		this.serviceTopology = serviceTopology;
	}

	public String getServiceStatus() {
		return serviceStatus;
	}

	public void setServiceStatus(String serviceStatus) {
		this.serviceStatus = serviceStatus;
	}

	public String getCustomerOCN() {
		return customerOCN;
	}

	public void setCustomerOCN(String customerOCN) {
		this.customerOCN = customerOCN;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getUrlCallback() {
		return urlCallback;
	}

	public void setUrlCallback(String urlCallback) {
		this.urlCallback = urlCallback;
	}

	public String getOperationType() {
		return operationType;
	}

	public void setOperationType(String operationType) {
		this.operationType = operationType;
	}

	public String getaDeviceID() {
		return aDeviceID;
	}

	public void setaDeviceID(String aDeviceID) {
		this.aDeviceID = aDeviceID;
	}

	public String getaDeviceIPAddress() {
		return aDeviceIPAddress;
	}

	public void setaDeviceIPAddress(String aDeviceIPAddress) {
		this.aDeviceIPAddress = aDeviceIPAddress;
	}

	public String getbDeviceID() {
		return bDeviceID;
	}

	public void setbDeviceID(String bDeviceID) {
		this.bDeviceID = bDeviceID;
	}

	public String getbDeviceIPAddress() {
		return bDeviceIPAddress;
	}

	public void setbDeviceIPAddress(String bDeviceIPAddress) {
		this.bDeviceIPAddress = bDeviceIPAddress;
	}

	public String getaPortID() {
		return aPortID;
	}

	public void setaPortID(String aPortID) {
		this.aPortID = aPortID;
	}

	public String getaSmartsPortName() {
		return aSmartsPortName;
	}

	public void setaSmartsPortName(String aSmartsPortName) {
		this.aSmartsPortName = aSmartsPortName;
	}

	public String getbPortID() {
		return bPortID;
	}

	public void setbPortID(String bPortID) {
		this.bPortID = bPortID;
	}

	public String getbSmartsPortName() {
		return bSmartsPortName;
	}

	public void setbSmartsPortName(String bSmartsPortName) {
		this.bSmartsPortName = bSmartsPortName;
	}

	public String getaDeviceCountryCode() {
		return aDeviceCountryCode;
	}

	public void setaDeviceCountryCode(String aDeviceCountryCode) {
		this.aDeviceCountryCode = aDeviceCountryCode;
	}

	public String getaDeviceCityCode() {
		return aDeviceCityCode;
	}

	public void setaDeviceCityCode(String aDeviceCityCode) {
		this.aDeviceCityCode = aDeviceCityCode;
	}

	public String getbDeviceCountryCode() {
		return bDeviceCountryCode;
	}

	public void setbDeviceCountryCode(String bDeviceCountryCode) {
		this.bDeviceCountryCode = bDeviceCountryCode;
	}

	public String getbDeviceCityCode() {
		return bDeviceCityCode;
	}

	public void setbDeviceCityCode(String bDeviceCityCode) {
		this.bDeviceCityCode = bDeviceCityCode;
	}

	@Override
	public String toString() {
		return "SmartConnectionRequest [operationType=" + operationType
				+ ", evcServiceID=" + evcServiceID + ", circuitID=" + circuitID
				+ ", serviceType=" + serviceType + ", serviceTopology="
				+ serviceTopology + ", serviceStatus=" + serviceStatus
				+ ", customerOCN=" + customerOCN + ", customerName="
				+ customerName + ", urlCallback=" + urlCallback
				+ ", aDeviceID=" + aDeviceID + ", aDeviceIPAddress="
				+ aDeviceIPAddress + ", bDeviceID=" + bDeviceID
				+ ", bDeviceIPAddress=" + bDeviceIPAddress + ", aPortID="
				+ aPortID + ", aSmartsPortName=" + aSmartsPortName
				+ ", bPortID=" + bPortID + ", bSmartsPortName="
				+ bSmartsPortName + ", aDeviceCountryCode="
				+ aDeviceCountryCode + ", aDeviceCityCode=" + aDeviceCityCode
				+ ", bDeviceCountryCode=" + bDeviceCountryCode
				+ ", bDeviceCityCode=" + bDeviceCityCode + "]";
	}

	

}
