package com.colt.novitas.smart.client;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;

import net.colt.novitasserviceassurance.createservicesarequest.v1.CreateServiceSARequest2;
import net.colt.novitasserviceassurance.createservicesarequest.v1.CreateServiceSARequestPortType;
import net.colt.novitasserviceassurance.createservicesarequest.v1.CreateServiceSARequest_Service;
import net.colt.novitasserviceassurance.createservicesarequest.v1.CreateServiceSAResponse;
import net.colt.xml.ns.tns.v1.ComplexHeaderType;
import net.colt.xml.ns.tns.v1.CreateSARequestType;
import net.colt.xml.ns.tns.v1.CreateServiceSARequestType;
import net.colt.xml.ns.tns.v1.DeviceEnd;
import net.colt.xml.ns.tns.v1.DeviceObjectType;
import net.colt.xml.ns.tns.v1.OperationType;
import net.colt.xml.ns.tns.v1.PortObjectType;
import net.colt.xml.ns.tns.v1.PortRole;
import net.colt.xml.ns.tns.v1.ServiceObjectType;
import net.colt.xml.ns.tns.v1.ServiceStatus;
import net.colt.xml.ns.tns.v1.ServiceTopology;
import net.colt.xml.ns.tns.v1.ServiceType;
import net.colt.xml.ns.tns.v1.SimpleHeaderType;
import net.colt.xml.ns.tns.v1.ValidationStatus;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.colt.novitas.smart.request.SmartConnectionRequest;
import com.colt.novitas.soap.SoapClient;

public class SmartsAPIClient {

	private String SMART_SOAP_URL = "http://WBMEAIIS31:6666/ws/ColtNovitasServiceAssurance.common.webSvcProvider:createServiceSARequest/ColtNovitasServiceAssurance_common_webSvcProvider_createServiceSARequest_PortSmarts?wsdl";
	private String SMART_SOAP_USERNAME = "jBPMUser";
	private String SMART_SOAP_PASSWORD = "jBPMUser";
	private static final Logger LOG = Logger.getLogger(SmartsAPIClient.class);
   
	private SoapClient soapClient = new SoapClient();
	public SmartsAPIClient(final String RESOURCE_SOAP_URL) {
		this.SMART_SOAP_URL = RESOURCE_SOAP_URL;
	}

	public SmartsAPIClient(String SMART_SOAP_URL, String SMART_SOAP_USERNAME, String SMART_SOAP_PASSWORD) {
		this.SMART_SOAP_URL = SMART_SOAP_URL;
		this.SMART_SOAP_USERNAME = SMART_SOAP_USERNAME;
		this.SMART_SOAP_PASSWORD = SMART_SOAP_PASSWORD;
	}

	public SmartsAPIClient() {
	}

	public String createSmartServiceRequest(SmartConnectionRequest req) {
		LOG.info("Start >>>  SmartsAPIClient.createSmartServiceRequest()");

		CreateServiceSARequest2 createServiceSARequest = new CreateServiceSARequest2();

		CreateSARequestType createSARequestType = new CreateSARequestType();
		createServiceSARequest.setCreateServiceSARequest(createSARequestType);

		CreateServiceSARequestType createServiceSARequestType = new CreateServiceSARequestType();

		createSARequestType.setRequestPayload(createServiceSARequestType);

		ComplexHeaderType complexHeaderType = new ComplexHeaderType();
		SimpleHeaderType simpleHeaderType = new SimpleHeaderType();
		simpleHeaderType.setSenderSystem("Novitas");
		simpleHeaderType.setRequestCreationTime(getDateAsXMLGregorian());
		complexHeaderType.setHeader(simpleHeaderType);
		createServiceSARequestType.setHeader(complexHeaderType);
		createServiceSARequestType.setOperationType(OperationType.fromValue(req.getOperationType()));
		createServiceSARequestType.setCallBackURL(req.getUrlCallback());

		ServiceObjectType serviceObjectType = new ServiceObjectType();
		serviceObjectType.setCircuitID(req.getCircuitID());
		serviceObjectType.setCustomerName(req.getCustomerName());
		serviceObjectType.setCustomerOCN(req.getCustomerOCN());
		serviceObjectType.setEVCServiceID(req.getEvcServiceID());
		serviceObjectType.setServiceStatus(ServiceStatus.fromValue(req.getServiceStatus()));
		serviceObjectType.setServiceTopology(ServiceTopology.P_2_P);
		serviceObjectType.setServiceType(ServiceType.fromValue(req.getServiceType()));

		createServiceSARequestType.setService(serviceObjectType);

		PortObjectType aPortObjectType = new PortObjectType();
		aPortObjectType.setPortID(req.getaPortID());
		aPortObjectType.setSmartsPortName(req.getaSmartsPortName() != null ? req.getaSmartsPortName() : "  " );
		aPortObjectType.setPortRole(PortRole.UNI);

		PortObjectType bPortObjectType = new PortObjectType();
		bPortObjectType.setPortID(req.getbPortID());
		bPortObjectType.setSmartsPortName(req.getbSmartsPortName() != null ? req.getbSmartsPortName() : "  ");
		bPortObjectType.setPortRole(PortRole.UNI);

		DeviceObjectType aDeviceObjectType = new DeviceObjectType();
		aDeviceObjectType.setDeviceID(req.getaDeviceID());
		aDeviceObjectType.setDeviceIPAddress(req.getaDeviceIPAddress());
		aDeviceObjectType.setDeviceEnd(DeviceEnd.A_END);
		if(StringUtils.isNotBlank(req.getaDeviceCountryCode()))
		  aDeviceObjectType.setDeviceCountryCode(req.getaDeviceCountryCode());
		if(StringUtils.isNotBlank(req.getaDeviceCityCode()))
		   aDeviceObjectType.setDeviceCityCode(req.getaDeviceCityCode());

		DeviceObjectType bDeviceObjectType = new DeviceObjectType();
		bDeviceObjectType.setDeviceID(req.getbDeviceID());
		bDeviceObjectType.setDeviceIPAddress(req.getbDeviceIPAddress());
		bDeviceObjectType.setDeviceEnd(DeviceEnd.B_END);
		if(StringUtils.isNotBlank(req.getbDeviceCountryCode()))
		   bDeviceObjectType.setDeviceCountryCode(req.getbDeviceCountryCode());
		if(StringUtils.isNotBlank(req.getbDeviceCityCode()))
		   bDeviceObjectType.setDeviceCityCode(req.getbDeviceCityCode());

		List<PortObjectType> aPortObjectTypeList = aDeviceObjectType.getDevicePort();
		aPortObjectTypeList.add(aPortObjectType);

		List<PortObjectType> bPortObjectTypeList = bDeviceObjectType.getDevicePort();
		bPortObjectTypeList.add(bPortObjectType);

		List<DeviceObjectType> deviceObjectTypeList = createServiceSARequestType.getDevice();
		deviceObjectTypeList.add(aDeviceObjectType);
		deviceObjectTypeList.add(bDeviceObjectType);

		try {
			LOG.info("createSmartServiceRequest  calling SMARTS system >>>>> ::::" + SMART_SOAP_URL);
			CreateServiceSARequestPortType serviceSmartConnection = soapClient.getServiceObject(SMART_SOAP_URL,
					CreateServiceSARequest_Service.SERVICE, CreateServiceSARequestPortType.class);
			Map<String, Object> req_ctx = ((BindingProvider) serviceSmartConnection).getRequestContext();
			soapClient.setAuthenticationToken(req_ctx, SMART_SOAP_USERNAME, SMART_SOAP_PASSWORD);
            
			soapClient.addLogHandler(serviceSmartConnection);
			
			CreateServiceSAResponse createServiceSAResponse = serviceSmartConnection
					.createServiceSARequest(createServiceSARequest);
			String status = createServiceSAResponse.getCreateServiceSARequest().getValidationResponse().getValidationStatus().value();
			String correlationId = createServiceSAResponse.getCreateServiceSARequest().getValidationResponse().getHeader().getCorrelationID();
			LOG.info("SMARTS system  response is  ::::" + status);
			LOG.info("SMARTS system  Correlation ID is  ::::" + correlationId);
			if (createServiceSAResponse.getCreateServiceSARequest().getValidationResponse().getValidationStatus()
					.equals(ValidationStatus.VALIDREQUEST) && null != correlationId) {

				return correlationId;
			}
			return "FAILED";

		} catch (Exception ex) {
			LOG.error("Error >>>  SmartsAPIClient.createSmartServiceRequest()" ,ex);
			return "FAILED";
		}
	}

	private XMLGregorianCalendar getDateAsXMLGregorian(){
		GregorianCalendar c = new GregorianCalendar();
		c.setTime(new Date());
		try {
			return  DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
		} catch (DatatypeConfigurationException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static void main(String[] args) {

		SmartConnectionRequest smart = new SmartConnectionRequest();

		smart.setaDeviceID("A DEVICE ID");
		smart.setaDeviceIPAddress("A IP ADDRESS");
		smart.setaPortID("A PORT ID");
		smart.setaSmartsPortName("A SMART PORT NAME");
		smart.setbDeviceID("B DEVICE ID");
		smart.setbDeviceIPAddress("B IP ADDRESS");
		smart.setbPortID("B PORT ID");
		smart.setbSmartsPortName("B SMART PORT NAME");
		smart.setCircuitID("CIRCUIT ID");
		smart.setCustomerName("CUSTOMER NAME");
		smart.setCustomerOCN("OCN");
		smart.setEvcServiceID("EVC SERVICE ID");
		smart.setOperationType("Add");
		smart.setServiceStatus("Live");
		smart.setServiceTopology("P2P");
		smart.setServiceType("DCNetaaS");
		smart.setUrlCallback("http://");

		SmartsAPIClient client = new SmartsAPIClient();
		System.out.println(client.createSmartServiceRequest(smart));

	}

	

}
