package com.colt.novitas.resource.location.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.resource.location.response.LocationResponse;

import net.colt.xml.ns.cbe.cdd.v1.LocationAddressType;
import net.colt.xml.ns.cbe.cdd.v1.SiteStatusType;
import net.colt.xml.ns.cbe.cdd.v1.SiteTypeType;
import net.colt.xml.ns.pm.common.v1.PremiseMasterLocationAddressType;
import net.colt.xml.ns.pm.common.v1.PremiseMasterSiteInfoWithLocationAddressType;
import net.colt.xml.ns.pm.common.v1.PremiseMasterSiteType;

public class LocationResponseUtil {
	
	private static final Logger logger = LoggerFactory.getLogger(LocationResponseUtil.class);
	private static final List<SiteTypeType> privateSiteType = new ArrayList<>();
	private static final String PRIVATE_BD_CATEGORY_KEY = "private.building.category.key";
	private static final String PUBLIC_BD_CATEGORY_KEY = "public.building.category.key";
	
	static{
		privateSiteType.add(SiteTypeType.CUST_PRIVATE);
		privateSiteType.add(SiteTypeType.OFFNET_CUST);
		privateSiteType.add(SiteTypeType.CUST_PRIVATE_VIA_LEASED_BEARER);
		privateSiteType.add(SiteTypeType.CUST_PRIVATE_VIA_LEASED_FIBRE);
		privateSiteType.add(SiteTypeType.CARRIER_HOTEL_CUSTOMER_PRIVATE);
	}
	
    public static LocationResponse getLocationResponse( PremiseMasterSiteInfoWithLocationAddressType premiseMasterSiteInfoWithLocationAddressType) {

    	LocationResponse location = new LocationResponse();

        String streetName = "";
        String city = "";
        String country = "";
        String postalZipCode = "";

        PremiseMasterSiteType premiseMasterSiteType = premiseMasterSiteInfoWithLocationAddressType.getSite();
        PremiseMasterLocationAddressType premiseMasterLocationAddressType = premiseMasterSiteInfoWithLocationAddressType
                .getLocationAddress();
        if(StringUtils.isNotBlank(premiseMasterSiteType.getSite().getSiteId().getSiteId()) && SiteStatusType.LIVE.equals(premiseMasterSiteType.getSite().getSiteStatus())
           && ! "NULL".equals(premiseMasterSiteType.getSite().getSiteId().getSiteId())){
        	if (StringUtils.isNotBlank(premiseMasterLocationAddressType.getLocationAddress().getStreetName())) {
                streetName = premiseMasterLocationAddressType.getLocationAddress().getStreetName();
            }
            if (StringUtils.isNotBlank(premiseMasterLocationAddressType.getLocationAddress().getCountry())) {
                country = premiseMasterLocationAddressType.getLocationAddress().getCountry();
            }
            if (StringUtils.isNotBlank(premiseMasterLocationAddressType.getLocationAddress().getCityTown())) {
                city = premiseMasterLocationAddressType.getLocationAddress().getCityTown();
            }
            if (StringUtils.isNotBlank(premiseMasterLocationAddressType.getLocationAddress().getPostalZipCode())) {
                postalZipCode = premiseMasterLocationAddressType.getLocationAddress().getPostalZipCode();
            }

            /*// Append streetName, postalZipCode. city, country. in address
            StringBuffer finalAddress = new StringBuffer();
            if (StringUtils.isNotBlank(streetName))
                finalAddress.append(streetName);
            if (StringUtils.isNotBlank(postalZipCode))
                if (StringUtils.isNotBlank(finalAddress.toString()))
                    finalAddress.append(", ");
                finalAddress.append(postalZipCode);
            if (StringUtils.isNotBlank(finalAddress.toString()))
                finalAddress.append(". ");   
            if (StringUtils.isNotBlank(city))
                finalAddress.append(city);
            if (StringUtils.isNotBlank(country))
                if (StringUtils.isNotBlank(finalAddress.toString()))
                    finalAddress.append(", ");
                finalAddress.append(country);
            if (StringUtils.isNotBlank(finalAddress.toString()))
                finalAddress.append(".");   
            location.setAddress(finalAddress.toString());*/
            
            if (premiseMasterLocationAddressType.getLocationAddress() != null) {
            	location.setAddress(AddressFormatHelper.formatAddress(premiseMasterLocationAddressType.getLocationAddress()));
            }
            
            if (StringUtils.isNotBlank(premiseMasterSiteType.getSite().getBuildingId().getBuildingId())) {
            	location.setBuildingId(premiseMasterSiteType.getSite().getBuildingId().getBuildingId());
            }

            /*if (StringUtils.isNotBlank(premiseMasterLocationAddressType.getLocationAddress().getBuildingName())) {
            	location.setBuildingName(premiseMasterLocationAddressType.getLocationAddress().getBuildingName());
            }*/
            
            if (StringUtils.isNotBlank(premiseMasterLocationAddressType.getLocationAddress().getStreetName())) {
            	location.setStreetName(premiseMasterLocationAddressType.getLocationAddress().getStreetName());
            }
            if (StringUtils.isNotBlank(premiseMasterSiteType.getSite().getSiteId().getSiteId())) {
            	location.setId(premiseMasterSiteType.getSite().getSiteId().getSiteId());
            }
            if (StringUtils.isNotBlank(premiseMasterSiteType.getSite().getInventorySiteName())) {
            	location.setName(premiseMasterSiteType.getSite().getInventorySiteName());
            }
            if (StringUtils.isNotBlank(premiseMasterSiteType.getSite().getSiteType().name())) {
            	location.setSiteType(premiseMasterSiteType.getSite().getSiteType().value());
            	if(privateSiteType.contains(premiseMasterSiteType.getSite().getSiteType())){
            		location.setPrivateSite(true);
            	}
            }
            if (StringUtils.isNotBlank(premiseMasterLocationAddressType.getLatitude())) {
            	location.setLatitude(Float.valueOf(premiseMasterLocationAddressType.getLatitude()));
            }
            if (StringUtils.isNotBlank(premiseMasterLocationAddressType.getLongitude())) {
            	location.setLongitude(Float.valueOf(premiseMasterLocationAddressType.getLongitude()));
            }
            if (StringUtils.isNotBlank(premiseMasterLocationAddressType.getLocationAddress().getCountry())) {
            	location.setCountry(premiseMasterLocationAddressType.getLocationAddress().getCountry());
            }
            if (StringUtils.isNotBlank(premiseMasterLocationAddressType.getLocationAddress().getCityTown())) {
            	location.setCity(premiseMasterLocationAddressType.getLocationAddress().getCityTown());
            }
            if (StringUtils.isNotBlank(premiseMasterLocationAddressType.getLocationAddress().getState())) {
            	location.setState(premiseMasterLocationAddressType.getLocationAddress().getState());
            }
            if (StringUtils.isNotBlank(premiseMasterLocationAddressType.getLocationAddress().getPostalZipCode())) {
            	location.setPostalZipCode(premiseMasterLocationAddressType.getLocationAddress().getPostalZipCode());
            }
            if (StringUtils.isNotBlank(premiseMasterLocationAddressType.getLocationAddress().getPremisesNumber())) {
            	location.setPremisesNumber(premiseMasterLocationAddressType.getLocationAddress().getPremisesNumber());
            }
            if (StringUtils.isNotBlank(premiseMasterSiteType.getSite().getRoomName())) {
            	location.setRoomName(premiseMasterSiteType.getSite().getRoomName());
            }
            if (StringUtils.isNotBlank(premiseMasterSiteType.getSite().getFloor())) {
            	location.setFloor(premiseMasterSiteType.getSite().getFloor());
            }
            if (StringUtils.isNotBlank(premiseMasterSiteType.getSite().getFloor())) {
            	location.setCityCode(premiseMasterSiteType.getSite().getManCityCode());
            }
            
            if (StringUtils.isNotBlank(premiseMasterSiteType.getSite().getFloor())) {
            	location.setCountryCode(premiseMasterLocationAddressType.getLocationAddress().getCountryCode());
            }
            //String sitetype = getSiteTypePrefix(premiseMasterSiteType.getSite().getBuildingId().getBuildingId(),country,client, type,location) + "-" + country + "-" + city;
            //location.setType(sitetype.toUpperCase());
            //location.setColtRegion(APIHelper.getRegionForCountry(location.getCountry(), baseUrl, userName, password));
            location.setLocalBuildingName(premiseMasterLocationAddressType.getBuildingNameLocalLanguage());//TO-DO
            return location;
       }
        return null;
    }
    
    /**
	 * <p> Formats the address and returns in String format </p>
	 * 
	 * @param LocationAddressType site
	 * @return buildingName - premisesNumber streetName postalZipCode. cityTown, country.
	 * */
	/*private static String formatAddress(LocationAddressType site){
		String seperator = "";
		StringBuffer formattedAddress = new StringBuffer();
		
		//create first part of the address
		//premisesNumber, streetName, postalZipCode.
		if (StringUtils.isNotEmpty(site.getPremisesNumber())
				|| StringUtils.isNotEmpty(site.getStreetName()) || StringUtils.isNotEmpty(site.getPostalZipCode())){
			if (StringUtils.isNotEmpty(site.getPremisesNumber()) || StringUtils.isNotEmpty(site.getStreetName())){
				if (StringUtils.isNotEmpty(site.getPremisesNumber())){
					formattedAddress.append(site.getPremisesNumber());
					seperator = " ";
				}
				if (StringUtils.isNotEmpty(site.getStreetName())){
					if (StringUtils.isNotEmpty(seperator)){
						formattedAddress.append(seperator);
						seperator = "";
					}
					formattedAddress.append(site.getStreetName());
				}
				seperator = " ";
			} else {
				if (StringUtils.isNotEmpty(seperator)){
					seperator = " ";
				}
			}
			if (StringUtils.isNotEmpty(site.getPostalZipCode())){
				if (StringUtils.isNotEmpty(seperator)){
					formattedAddress.append(seperator);
					seperator = "";
				}
				formattedAddress.append(site.getPostalZipCode());
			}
			formattedAddress.append(".");
			seperator = " ";
		}
		
		//create second part of the address
		//cityTown, country.
		if (StringUtils.isNotEmpty(site.getCityTown()) || StringUtils.isNotEmpty(site.getCountry())){
			if (StringUtils.isNotEmpty(seperator)){
				formattedAddress.append(seperator);
				seperator = "";
			}
			if (StringUtils.isNotEmpty(site.getCityTown())){
				formattedAddress.append(site.getCityTown());
				seperator = ", ";
			}
			if (StringUtils.isNotEmpty(site.getCountry())){
				if (StringUtils.isNotEmpty(seperator)){
					formattedAddress.append(seperator);
					seperator = "";
				}
				formattedAddress.append(site.getCountry());
			}
			formattedAddress.append(".");
		}
		
		return formattedAddress.toString();
	}*/
	

	
	

	
	 
}
