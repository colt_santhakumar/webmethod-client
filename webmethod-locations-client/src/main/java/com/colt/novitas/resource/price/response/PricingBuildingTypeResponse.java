package com.colt.novitas.resource.price.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PricingBuildingTypeResponse {

	@JsonProperty("building_id")
	private String buildingId;

	@JsonProperty("site_type")
	private String siteType;

	@JsonProperty("cross_connect_allowed")
	private Boolean crossConnectAllowed;

	public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}

	public String getSiteType() {
		return siteType;
	}

	public void setSiteType(String siteType) {
		this.siteType = siteType;
	}

	public Boolean getCrossConnectAllowed() {
		return crossConnectAllowed;
	}

	public void setCrossConnectAllowed(Boolean crossConnectAllowed) {
		this.crossConnectAllowed = crossConnectAllowed;
	}

	@Override
	public String toString() {
		return "PricingBuildingTypeResponse [buildingId=" + buildingId + ", siteType=" + siteType
				+ ", crossConnectAllowed=" + crossConnectAllowed + "]";
	}

}
