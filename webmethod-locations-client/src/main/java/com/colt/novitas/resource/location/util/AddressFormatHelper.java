package com.colt.novitas.resource.location.util;

import java.util.HashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.resource.location.response.CountryRegionResponse;

import net.colt.xml.ns.cbe.cdd.v1.LocationAddressType;

public class AddressFormatHelper {
	
	private static final Logger log = LoggerFactory.getLogger("WM Location client :::> "+AddressFormatHelper.class.getName());
	
	public static String formatAddress(LocationAddressType site) {
		
		try {
		
			String finalFormat = null;
	
			if (StringUtils.isBlank(site.getCountry()))
				return formatAddressOld(site);
	
			// Get address format from Country Region table
			CountryRegionResponse countryRegion = APIHelper.getCountryRegion(site.getCountry());
	
			if (null == countryRegion || null == countryRegion.getAddressFormat()) {
				log.warn("Address format is empty for country: " + site.getCountry());
				return formatAddressOld(site);
			}
			
			String raw = countryRegion.getAddressFormat();
	
			if (StringUtils.isNotBlank(raw))
				raw = StringUtils.replace(raw, "[", "").replace("]", ""); // Remove square brackets
			String[] format = new String[StringUtils.countMatches(raw, "|")];
			format = raw.split("[\\|]");
	
			Map<Integer, String> formatMap = new HashMap<Integer, String>();
			for (String s : format) {
				formatMap.put(StringUtils.countMatches(s, "{{"), s);
			}
			NavigableMap<Integer, String> sortedMap = new TreeMap<Integer, String>(formatMap);
	
			int availableCount = 0;
	
			if (StringUtils.isNotBlank(site.getBuildingName())) availableCount++;
			if (StringUtils.isNotBlank(site.getFloorSuite())) availableCount++;
			//if (StringUtils.isNotBlank(site.getRoomName())) availableCount++;
			if (StringUtils.isNotBlank(site.getPremisesNumber())) availableCount++;
			if (StringUtils.isNotBlank(site.getStreetName())) availableCount++;
			if (StringUtils.isNotBlank(site.getCityTown())) availableCount++;
			if (StringUtils.isNotBlank(site.getPostalZipCode())) availableCount++;
			if (StringUtils.isNotBlank(site.getState())) availableCount++;
			if (StringUtils.isNotBlank(site.getCountry())) availableCount++;
			
			for(int key : sortedMap.keySet()) {
				if(availableCount <= key) {
					finalFormat = sortedMap.get(key);
					break;
				}
			}
			
			if(finalFormat == null) finalFormat = sortedMap.lastEntry().getValue();
			
			finalFormat = null != site.getBuildingName() ? finalFormat.replace("{{building-name}}", site.getBuildingName().trim()) : finalFormat.replace("{{building-name}}", "");		
			finalFormat = null != site.getFloorSuite() ? finalFormat.replace("{{floor}}", site.getFloorSuite().trim()) : finalFormat.replace("{{floor}}", "");
			//finalFormat = null != site.getRoomName() ? finalFormat.replace("{{room-name}}", site.getRoomName().trim()) : finalFormat.replace("{{room-name}}", "");
			finalFormat = null != site.getPremisesNumber() ? finalFormat.replace("{{premises-number}}", site.getPremisesNumber().trim()) : finalFormat.replace("{{premises-number}}", "");
			finalFormat = null != site.getStreetName() ? finalFormat.replace("{{street-name}}", site.getStreetName().trim()) : finalFormat.replace("{{street-name}}", "");
			finalFormat = null != site.getCityTown() ? finalFormat.replace("{{city-town}}", site.getCityTown().trim()) : finalFormat.replace("{{city-town}}", "");
			finalFormat = null != site.getPostalZipCode() ? finalFormat.replace("{{postal-code}}", site.getPostalZipCode().trim()) : finalFormat.replace("{{postal-code}}", "");
			finalFormat = null != site.getState() ? finalFormat.replace("{{state}}", site.getState().trim()) : finalFormat.replace("{{state}}", "");
			finalFormat = null != site.getCountry() ? finalFormat.replace("{{country}}", site.getCountry().trim()) : finalFormat.replace("{{country}}", "");
			
			// Remove extra whitespace, comma, dot,
			return finalFormat.replaceAll(" ,", "").replaceAll("\\ \\.", "").replaceAll(",$", "").trim();
		} catch (Exception ex) {
			ex.printStackTrace();
			return formatAddressOld(site);
		}
	}
	
	private static String formatAddressOld(LocationAddressType site) {
		String seperator = "";
		StringBuffer formattedAddress = new StringBuffer();
		
		//create first part of the address
		//buildingName - premisesNumber streetName, postalZipCode.
		if (StringUtils.isNotEmpty(site.getPremisesNumber()) || StringUtils.isNotEmpty(site.getStreetName()) || StringUtils.isNotEmpty(site.getPostalZipCode())){
			if (StringUtils.isNotEmpty(site.getBuildingName())){
				formattedAddress.append(site.getBuildingName().trim());
				seperator = " - ";
			}
			if (StringUtils.isNotEmpty(site.getPremisesNumber()) || StringUtils.isNotEmpty(site.getStreetName())){
				if (StringUtils.isNotEmpty(seperator)){
					formattedAddress.append(seperator);
					seperator = "";
				}
				if (StringUtils.isNotEmpty(site.getPremisesNumber())){
					formattedAddress.append(site.getPremisesNumber().trim());
					seperator = " ";
				}
				if (StringUtils.isNotEmpty(site.getStreetName())){
					if (StringUtils.isNotEmpty(seperator)){
						formattedAddress.append(seperator);
						seperator = "";
					}
					formattedAddress.append(site.getStreetName().trim());
				}
				seperator = ", ";
			} else {
				if (StringUtils.isNotEmpty(seperator)){
					seperator = ", ";
				}
			}
			if (StringUtils.isNotEmpty(site.getPostalZipCode())){
				if (StringUtils.isNotEmpty(seperator)){
					formattedAddress.append(seperator);
					seperator = "";
				}
				formattedAddress.append(site.getPostalZipCode().trim());
			}
			formattedAddress.append(".");
			seperator = " ";
		}
		
		//create second part of the address
		//cityTown, country.
		if (StringUtils.isNotEmpty(site.getCityTown()) || StringUtils.isNotEmpty(site.getCountry())){
			if (StringUtils.isNotEmpty(seperator)){
				formattedAddress.append(seperator);
				seperator = "";
			}
			if (StringUtils.isNotEmpty(site.getCityTown())){
				formattedAddress.append(site.getCityTown().trim());
				seperator = ", ";
			}
			if (StringUtils.isNotEmpty(site.getCountry())){
				if (StringUtils.isNotEmpty(seperator)){
					formattedAddress.append(seperator);
					seperator = "";
				}
				formattedAddress.append(site.getCountry().trim());
			}
			formattedAddress.append(".");
		}
		return formattedAddress.toString();
	}
	
	/*public static void main(String[] args) {
	  
		LocationAddressType site = new LocationAddressType();
		/*site.setBuildingName("Colt House");
		//site.setFloor("3");
		//site.setRoomName("RoomName");
		site.setPremisesNumber("20");
		//site.setStreetName("Great Eastern Street");
		site.setCityTown("London");
		site.setPostalZipCode("EC2A 3EH");
		site.setState(null);
		site.setCountry("United Kingdom");
		
		site.setBuildingName("Tokyo House");
		//site.setFloor("3");
		//site.setRoomName("RoomName");
		site.setPremisesNumber("2-14-1");
		site.setStreetName("Higashigotanda");
		site.setCityTown("Shinagawa-ku");
		site.setPostalZipCode("141-0022");
		//site.setState(null);
		site.setCountry("Japan");
		
		System.out.println(formatAddress(site));
	} */
	

}
