package com.colt.novitas.resource.location.client;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.ws.BindingProvider;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.property.SystemName;
import com.colt.novitas.resource.location.response.BuildingResponse;
import com.colt.novitas.resource.location.response.LocationResponse;
import com.colt.novitas.resource.location.util.APIHelper;
import com.colt.novitas.resource.location.util.BuildingResponseUtil;
import com.colt.novitas.resource.location.util.ConfigReader;
import com.colt.novitas.resource.location.util.LocationResponseUtil;
import com.colt.novitas.resource.price.response.PricingBuildingTypeResponse;
import com.colt.novitas.resource.service.ConfigurationService;
import com.colt.novitas.soap.SoapClient;

import net.colt.coltpremisemaster.common.websvcprovider.eorder.EOrder;
import net.colt.coltpremisemaster.common.websvcprovider.eorder.EOrderPortType;
import net.colt.coltpremisemaster.common.websvcprovider.eorder.SearchRequest;
import net.colt.coltpremisemaster.common.websvcprovider.eorder.SearchResponse2;
import net.colt.xml.ns.cbe.cdd.v1.AccessTechType;
import net.colt.xml.ns.cbe.cdd.v1.BuildingIdType;
import net.colt.xml.ns.cbe.cdd.v1.BuildingIdTypeType;
import net.colt.xml.ns.cbe.cdd.v1.BuildingType;
import net.colt.xml.ns.cbe.cdd.v1.LocationAddressType;
import net.colt.xml.ns.cbe.cdd.v1.ProactivelyCapacityManagedType;
import net.colt.xml.ns.cbe.cdd.v1.SiteIdType;
import net.colt.xml.ns.cbe.cdd.v1.SiteIdTypeType;
import net.colt.xml.ns.cbe.cdd.v1.SiteType;
import net.colt.xml.ns.pm.common.v1.PremiseMasterBuildingInfoWithLocationAddressType;
import net.colt.xml.ns.pm.common.v1.PremiseMasterBuildingType;
import net.colt.xml.ns.pm.common.v1.PremiseMasterLocationAddressListType;
import net.colt.xml.ns.pm.common.v1.PremiseMasterLocationAddressType;
import net.colt.xml.ns.pm.common.v1.PremiseMasterResponseDataType;
import net.colt.xml.ns.pm.common.v1.PremiseMasterSearchFilterOptionsType;
import net.colt.xml.ns.pm.common.v1.PremiseMasterSiteInfoWithLocationAddressType;
import net.colt.xml.ns.pm.common.v1.PremiseMasterSiteType;
import net.colt.xml.ns.pm.common.v1.PremiseMasterSourceSystemType;
import net.colt.xml.ns.pm.pm2cc.v1.SearchResponse;


/**
 * @author drao
 *
 */
public class LocationAPIClient {
	
	private static final Logger logger = LoggerFactory.getLogger(LocationAPIClient.class);

	private final static short SEARCH_MODE_1 = 1; //For getting building info
	private final static short SEARCH_MODE_2 = 2; //For getting site info
	private static final String PRIVATE_BD_CATEGORY_KEY = "private.building.category.key";
	private static final String PUBLIC_BD_CATEGORY_KEY = "public.building.category.key";
	private static String PRICE_API_URL = "price.api.url";
	private static String PRICE_API_USERNAME = "price.api.username";
	private static String PRICE_API_PASSWORD = "price.api.password";
	
	private String RESOURCE_SOAP_LOCATION_URL = "http://wmisb131.internal.colt.net/ws/ColtPremiseMaster.common.webSvcProvider:eOrder/ColtPremiseMaster_common_webSvcProvider_eOrder_Port?wsdl";
	private String RESOURCE_SOAP_USERNAME = "jBPMUser";
	private String RESOURCE_SOAP_PASSWORD = "jBPMUser";
	private SoapClient soapClient = new SoapClient();
    

	public LocationAPIClient(String RESOURCE_SOAP_URL, String RESOURCE_SOAP_USERNAME, String RESOURCE_SOAP_PASSWORD) {
		this.RESOURCE_SOAP_LOCATION_URL = RESOURCE_SOAP_URL;
		this.RESOURCE_SOAP_USERNAME = RESOURCE_SOAP_USERNAME;
		this.RESOURCE_SOAP_PASSWORD = RESOURCE_SOAP_PASSWORD;
	}

	public LocationAPIClient(){
		
	}
	
	/**
	 * Method is used for port creation from JBPM
	 *
	 */
	public Object getSiteByLocationId(String ocn, String locationId,String type) {

		 try{
			LocationResponse loc = LocationResponseUtil.getLocationResponse(getLocationforSiteIdAndOcn(locationId, ocn));
			if( null ==loc) return null;
			BuildingResponse buildResp = getBuildingInfoByBuildingId(loc.getBuildingId(), loc.getCountry()).get(0);
			
			List<PricingBuildingTypeResponse> pricingBuildingList = getPricingBuildingType(loc.getBuildingId());
			PricingBuildingTypeResponse pricingBuilding = null != pricingBuildingList && pricingBuildingList.size() > 0 ? pricingBuildingList.get(0) : null;

			updateSiteTypePrefix(loc.getBuildingId(), loc.getCountry(), buildResp, type, loc, pricingBuilding,loc.getCity());

			loc.setColtRegion(APIHelper.getRegionForCountry(loc.getCountry()));
			return loc;
		} catch (Exception ex) {
			logger.error("Failed to get location from Premise Master. Error detail: " + ex);
			return "FAILED";
		}
	}
	
	public Object getSiteByLocationId(String ocn, String locationId) {

		 try{
			LocationResponse loc = LocationResponseUtil.getLocationResponse(getLocationforSiteIdAndOcn(locationId, ocn));
			if( null ==loc) return null;
			BuildingResponse buildResp = getBuildingInfoByBuildingId(loc.getBuildingId(), loc.getCountry()).get(0);

			PricingBuildingTypeResponse pricingBuilding = null;
			
			List<PricingBuildingTypeResponse> pricingBuildingList = getPricingBuildingType(loc.getBuildingId());
			if(null != pricingBuildingList) {
				pricingBuilding = pricingBuildingList.size() > 0 ? pricingBuildingList.get(0) : null;
			}

			updateSiteTypePrefix(loc.getBuildingId(), loc.getCountry(), buildResp, null, loc, pricingBuilding,loc.getCity());
			
			loc.setColtRegion(APIHelper.getRegionForCountry(loc.getCountry()));
			return loc;
		} catch (Exception ex) {
			logger.error("Failed to get location from Premise Master. Error detail: " + ex);
			return "FAILED";
		}
	}
	
    public List<LocationResponse> getAllLocations(String ocn, Map<String, Object> requestParamMap) {

        net.colt.coltpremisemaster.common.websvcprovider.eorder.SearchRequest searchRequest = new net.colt.coltpremisemaster.common.websvcprovider.eorder.SearchRequest();

        net.colt.xml.ns.pm.pm2cc.v1.SearchRequest searchRequestInternal = new net.colt.xml.ns.pm.pm2cc.v1.SearchRequest();
        searchRequest.setSearchRequest(searchRequestInternal);
        searchRequestInternal.setSourceSystem(PremiseMasterSourceSystemType.NOVITAS);
        searchRequestInternal.setSearchMode(SEARCH_MODE_2);

        List<PremiseMasterSiteInfoWithLocationAddressType> premiseMasterSiteInfoWithLocationAddressTypeList = searchRequestInternal
                .getSiteInfoWithAddress();

        PremiseMasterSiteInfoWithLocationAddressType premiseMasterSiteInfoWithLocationAddressType = new PremiseMasterSiteInfoWithLocationAddressType();

        PremiseMasterSiteType premiseMasterSiteType = new PremiseMasterSiteType();

        premiseMasterSiteInfoWithLocationAddressType.setSite(premiseMasterSiteType);
        PremiseMasterLocationAddressType locationAddress = new PremiseMasterLocationAddressType();
        premiseMasterSiteInfoWithLocationAddressType.setLocationAddress(locationAddress);

        premiseMasterSiteInfoWithLocationAddressTypeList.add(premiseMasterSiteInfoWithLocationAddressType);

        LocationAddressType locationAddressType = new LocationAddressType();
        locationAddressType.setCityTown(requestParamMap.get("city").toString());
        locationAddressType.setCountry(requestParamMap.get("country").toString());
        if (requestParamMap.containsKey("post_code") && null != requestParamMap.get("post_code")) {
            locationAddressType.setPostalZipCode(requestParamMap.get("post_code").toString());
        }
        if (requestParamMap.containsKey("street_name") && null != requestParamMap.get("street_name")) {
            locationAddressType.setStreetName(requestParamMap.get("street_name").toString());
        }

        locationAddress.setLocationAddress(locationAddressType);

        SiteType siteType = new SiteType();
        premiseMasterSiteType.setSite(siteType);
        List<String> ocnList = new ArrayList<String>();
        if (requestParamMap.containsKey("ocn") && requestParamMap.get("ocn") != null) {
            ocnList.add(requestParamMap.get("ocn").toString());
            premiseMasterSiteType.getOCN().addAll(ocnList);
        }

        SiteIdType siteIdType = new SiteIdType();
        siteIdType.setSiteId("-1");

        if (requestParamMap.containsKey("site_name") && null != requestParamMap.get("site_name")) {
            siteType.setInventorySiteName(requestParamMap.get("site_name").toString());
        }

        siteIdType.setSiteIdType(SiteIdTypeType.SITE_INVENTORY_ID);
        siteType.setSiteId(siteIdType);

        PremiseMasterSearchFilterOptionsType filterOptions = new PremiseMasterSearchFilterOptionsType();
		filterOptions.getAccessTech().add(AccessTechType.ETHERNET_OVER_MMSP);
		//searchRequestInternal.setFilterOptions(filterOptions);

		return callPremiseMasterForSiteInfo(searchRequest);

    }
    
    public List<LocationResponse> getAllLocations() {

        net.colt.coltpremisemaster.common.websvcprovider.eorder.SearchRequest searchRequest = new net.colt.coltpremisemaster.common.websvcprovider.eorder.SearchRequest();

        net.colt.xml.ns.pm.pm2cc.v1.SearchRequest searchRequestInternal = new net.colt.xml.ns.pm.pm2cc.v1.SearchRequest();
        searchRequest.setSearchRequest(searchRequestInternal);
        searchRequestInternal.setSourceSystem(PremiseMasterSourceSystemType.NOVITAS);
        searchRequestInternal.setSearchMode(SEARCH_MODE_2);

        List<PremiseMasterSiteInfoWithLocationAddressType> premiseMasterSiteInfoWithLocationAddressTypeList = searchRequestInternal
                .getSiteInfoWithAddress();

        PremiseMasterSiteInfoWithLocationAddressType premiseMasterSiteInfoWithLocationAddressType = new PremiseMasterSiteInfoWithLocationAddressType();

        PremiseMasterSiteType premiseMasterSiteType = new PremiseMasterSiteType();

        premiseMasterSiteInfoWithLocationAddressType.setSite(premiseMasterSiteType);
        PremiseMasterLocationAddressType locationAddress = new PremiseMasterLocationAddressType();
        premiseMasterSiteInfoWithLocationAddressType.setLocationAddress(locationAddress);

        premiseMasterSiteInfoWithLocationAddressTypeList.add(premiseMasterSiteInfoWithLocationAddressType);

        SiteType siteType = new SiteType();
        premiseMasterSiteType.setSite(siteType);

        SiteIdType siteIdType = new SiteIdType();
        siteIdType.setSiteId("-1");

        siteIdType.setSiteIdType(SiteIdTypeType.SITE_INVENTORY_ID);
        siteType.setSiteId(siteIdType);

        PremiseMasterSearchFilterOptionsType filterOptions = new PremiseMasterSearchFilterOptionsType();
        filterOptions.getAccessTech().add(AccessTechType.ETHERNET_OVER_MMSP);
		filterOptions.getProactivelyCapacityManaged().add(ProactivelyCapacityManagedType.ETHERNET_OVER_MMSP);
		//searchRequestInternal.setFilterOptions(filterOptions);

		return callPremiseMasterForSiteInfo(searchRequest);

    }
	
    public List<BuildingResponse> getAllBuildingInfo(Map<String, Object> requestParamMap) {
    	
    	System.out.println("Location Client map : " +requestParamMap.toString());

        net.colt.coltpremisemaster.common.websvcprovider.eorder.SearchRequest searchRequest = new net.colt.coltpremisemaster.common.websvcprovider.eorder.SearchRequest();

        net.colt.xml.ns.pm.pm2cc.v1.SearchRequest searchRequestInternal = new net.colt.xml.ns.pm.pm2cc.v1.SearchRequest();
        searchRequest.setSearchRequest(searchRequestInternal);
        searchRequestInternal.setSourceSystem(PremiseMasterSourceSystemType.NOVITAS);
        searchRequestInternal.setSearchMode(SEARCH_MODE_1);//For getting building info

        PremiseMasterBuildingInfoWithLocationAddressType premiseMasterBuildingInfoWithLocationAddressType = new PremiseMasterBuildingInfoWithLocationAddressType();
        searchRequestInternal.setBuildingInfoWithAddress(premiseMasterBuildingInfoWithLocationAddressType);
        
        PremiseMasterBuildingType premiseMasterBuildingType = new PremiseMasterBuildingType();
        premiseMasterBuildingInfoWithLocationAddressType.setBuilding(premiseMasterBuildingType);
        
        BuildingType buildingType = new BuildingType();
        premiseMasterBuildingType.setBuilding(buildingType);
        
        BuildingIdType buildingIdType = new BuildingIdType();
        buildingType.setBuildingId(buildingIdType);
        buildingIdType.setBuildingIdType(BuildingIdTypeType.BUILDING_INVENTORY_ID);
        buildingIdType.setBuildingId("-1");
       
        PremiseMasterLocationAddressListType premiseMasterLocationAddressListType = new PremiseMasterLocationAddressListType();
        premiseMasterBuildingInfoWithLocationAddressType.setAssociatedLocationAddressList(premiseMasterLocationAddressListType);

        List<PremiseMasterLocationAddressType> premiseMasterLocationAddressTypeList = premiseMasterLocationAddressListType.getLocationAddress();
        PremiseMasterLocationAddressType premiseMasterLocationAddressType = new PremiseMasterLocationAddressType();
        premiseMasterLocationAddressTypeList.add(premiseMasterLocationAddressType);
        
		/*if (requestParamMap.containsKey("local_city") && null != requestParamMap.get("local_city"))
			premiseMasterLocationAddressType.setCityNameLocalLanguage(requestParamMap.get("local_city").toString());
		if (requestParamMap.containsKey("local_street_name") && null != requestParamMap.get("local_street_name"))
			premiseMasterLocationAddressType.setStreetNameLocalLanguage(requestParamMap.get("local_street_name").toString());*/

        LocationAddressType locationAddressType = new LocationAddressType();
        premiseMasterLocationAddressType.setLocationAddress(locationAddressType);
        if (requestParamMap.containsKey("city") && null != requestParamMap.get("city"))
        	locationAddressType.setCityTown(requestParamMap.get("city").toString());
        locationAddressType.setCountry(requestParamMap.get("country").toString());
        if (requestParamMap.containsKey("post_code") && null != requestParamMap.get("post_code")) {
            locationAddressType.setPostalZipCode(requestParamMap.get("post_code").toString());
        }
        if (requestParamMap.containsKey("street_name") && null != requestParamMap.get("street_name")) {
            locationAddressType.setStreetName(requestParamMap.get("street_name").toString());
        }
        
        if (requestParamMap.containsKey("building_name") && null != requestParamMap.get("building_name")) {
            locationAddressType.setBuildingName(requestParamMap.get("building_name").toString());
        }
        
       
        //PremiseMasterSearchFilterOptionsType filterOptions = new PremiseMasterSearchFilterOptionsType();
        //filterOptions.getReachable().add(ReachableType.ETHERNET_OVER_MMSP);
		//searchRequestInternal.setFilterOptions(filterOptions);

		return callPremiseMasterForBuildingInfo(searchRequest);

    }

	private List<BuildingResponse> callPremiseMasterForBuildingInfo(
			net.colt.coltpremisemaster.common.websvcprovider.eorder.SearchRequest searchRequest) {
		try {
			
			EOrderPortType eOrderPortTypeCallback = soapClient.getServiceObject(RESOURCE_SOAP_LOCATION_URL,
					EOrder.SERVICE, EOrderPortType.class);
			Map<String, Object> req_ctx = ((BindingProvider) eOrderPortTypeCallback).getRequestContext();
			soapClient.setAuthenticationToken(req_ctx, RESOURCE_SOAP_USERNAME, RESOURCE_SOAP_PASSWORD);
			soapClient.setTimeOutValues(req_ctx, SystemName.PREMISE_MASTER);
		    soapClient.addLogHandler(eOrderPortTypeCallback);
			SearchResponse2 searchResponse2 = eOrderPortTypeCallback.search(searchRequest);
			
			List<BuildingResponse> buildingList = new ArrayList<BuildingResponse>();
			buildingList = BuildingResponseUtil.getBuildingInfo(searchResponse2.getSearchResponse());
			
			List<PricingBuildingTypeResponse> pricingBuildingList = null;
			
			if (buildingList != null && buildingList.size() > 0) {
				pricingBuildingList = getPricingBuildingType(null);
				
				if(pricingBuildingList != null && pricingBuildingList.size() > 0) {
					for(BuildingResponse b : buildingList) {
						pricingBuildingList.stream()
								.filter(building -> b.getBuildingId().equalsIgnoreCase(building.getBuildingId()))
								.findAny().ifPresent(a -> b.setCrossConnectAllowed(a.getCrossConnectAllowed() ? true: false));
					}
				} else {
					return buildingList;
				}
			}
			
			return buildingList;
			
		} catch (Exception ex) {
			logger.error("Failed to get building details from Premise Master. Error detail: " + ex);
			return null;
		}
	}
    
    public List<BuildingResponse> getBuildingInfoByBuildingId(String buildingId, String country) {

        net.colt.coltpremisemaster.common.websvcprovider.eorder.SearchRequest searchRequest = new net.colt.coltpremisemaster.common.websvcprovider.eorder.SearchRequest();

        net.colt.xml.ns.pm.pm2cc.v1.SearchRequest searchRequestInternal = new net.colt.xml.ns.pm.pm2cc.v1.SearchRequest();
        searchRequest.setSearchRequest(searchRequestInternal);
        searchRequestInternal.setSourceSystem(PremiseMasterSourceSystemType.NOVITAS);
        searchRequestInternal.setSearchMode(SEARCH_MODE_1);//For getting building info

        PremiseMasterBuildingInfoWithLocationAddressType premiseMasterBuildingInfoWithLocationAddressType = new PremiseMasterBuildingInfoWithLocationAddressType();
        searchRequestInternal.setBuildingInfoWithAddress(premiseMasterBuildingInfoWithLocationAddressType);
        
        PremiseMasterBuildingType premiseMasterBuildingType = new PremiseMasterBuildingType();
        premiseMasterBuildingInfoWithLocationAddressType.setBuilding(premiseMasterBuildingType);
        
        BuildingType buildingType = new BuildingType();
        premiseMasterBuildingType.setBuilding(buildingType);
        
        BuildingIdType buildingIdType = new BuildingIdType();
        buildingType.setBuildingId(buildingIdType);
        buildingIdType.setBuildingIdType(BuildingIdTypeType.BUILDING_INVENTORY_ID);
        buildingIdType.setBuildingId(buildingId);
        
        PremiseMasterLocationAddressListType premiseMasterLocationAddressListType = new PremiseMasterLocationAddressListType();
        premiseMasterBuildingInfoWithLocationAddressType.setAssociatedLocationAddressList(premiseMasterLocationAddressListType);
        
        List<PremiseMasterLocationAddressType> premiseMasterLocationAddressTypeList = premiseMasterLocationAddressListType.getLocationAddress();
        PremiseMasterLocationAddressType premiseMasterLocationAddressType = new PremiseMasterLocationAddressType();
        premiseMasterLocationAddressTypeList.add(premiseMasterLocationAddressType);
        
        LocationAddressType locationAddressType = new LocationAddressType();
        premiseMasterLocationAddressType.setLocationAddress(locationAddressType);
        locationAddressType.setCountry(country);        
       
        //PremiseMasterSearchFilterOptionsType filterOptions = new PremiseMasterSearchFilterOptionsType();
    	//filterOptions.getReachable().add(ReachableType.ETHERNET_OVER_MMSP);
		//searchRequestInternal.setFilterOptions(filterOptions);

		return callPremiseMasterForBuildingInfo(searchRequest);

    }
    
    /**
     * @param requestParamMap contains ocn & buildingId
     * @return
     */
    public List<LocationResponse> getSiteInfoByBuildingId(Map<String, Object> requestParamMap) {
    	
    	String buildingId = null;
    	if (requestParamMap.containsKey("building_id") && null != requestParamMap.get("building_id")) {
    		buildingId = requestParamMap.get("building_id").toString();
        }

        net.colt.coltpremisemaster.common.websvcprovider.eorder.SearchRequest searchRequest = new net.colt.coltpremisemaster.common.websvcprovider.eorder.SearchRequest();

        net.colt.xml.ns.pm.pm2cc.v1.SearchRequest searchRequestInternal = new net.colt.xml.ns.pm.pm2cc.v1.SearchRequest();
        searchRequest.setSearchRequest(searchRequestInternal);
        searchRequestInternal.setSourceSystem(PremiseMasterSourceSystemType.NOVITAS);
        searchRequestInternal.setSearchMode(SEARCH_MODE_2);//For getting site info
        List<PremiseMasterSiteInfoWithLocationAddressType> siteInfoWithAddresses = searchRequestInternal.getSiteInfoWithAddress();
        PremiseMasterSiteInfoWithLocationAddressType premiseMasterSiteInfoWithLocationAddressType = new PremiseMasterSiteInfoWithLocationAddressType();
        siteInfoWithAddresses.add(premiseMasterSiteInfoWithLocationAddressType);
        PremiseMasterSiteType premiseMasterSiteType =new PremiseMasterSiteType();
        SiteType siteType = new SiteType();
        
        premiseMasterSiteInfoWithLocationAddressType.setSite(premiseMasterSiteType);

        premiseMasterSiteType.setSite(siteType);
        List<String> ocnList = new ArrayList<String>();
        if (requestParamMap.containsKey("ocn") && requestParamMap.get("ocn") != null) {
            ocnList.add(requestParamMap.get("ocn").toString());
            premiseMasterSiteType.getOCN().addAll(ocnList);
        }

        SiteIdType siteIdType = new SiteIdType();
        siteIdType.setSiteId("-1");

        siteIdType.setSiteIdType(SiteIdTypeType.SITE_INVENTORY_ID);
        siteType.setSiteId(siteIdType);
        
        BuildingIdType buildingIdType = new BuildingIdType();
        buildingIdType.setBuildingIdType(BuildingIdTypeType.BUILDING_INVENTORY_ID);
        buildingIdType.setBuildingId(buildingId);
        siteType.setBuildingId(buildingIdType);

        //PremiseMasterSearchFilterOptionsType filterOptions = new PremiseMasterSearchFilterOptionsType();
        //filterOptions.getAccessTech().add(AccessTechType.ETHERNET_OVER_MMSP);
		//searchRequestInternal.setFilterOptions(filterOptions);

		return callPremiseMasterForSiteInfo(searchRequest);

    }

	private List<LocationResponse> callPremiseMasterForSiteInfo(
			net.colt.coltpremisemaster.common.websvcprovider.eorder.SearchRequest searchRequest) {
		try {
			
			EOrderPortType eOrderPortTypeCallback = soapClient.getServiceObject(RESOURCE_SOAP_LOCATION_URL,
					EOrder.SERVICE, EOrderPortType.class);
			Map<String, Object> req_ctx = ((BindingProvider) eOrderPortTypeCallback).getRequestContext();
			soapClient.setAuthenticationToken(req_ctx, RESOURCE_SOAP_USERNAME, RESOURCE_SOAP_PASSWORD);
			soapClient.setTimeOutValues(req_ctx, SystemName.PREMISE_MASTER);
			soapClient.addLogHandler(eOrderPortTypeCallback);
			SearchResponse2 searchResponse2 = eOrderPortTypeCallback.search(searchRequest);
			
			return getLocations(searchResponse2.getSearchResponse(),this);
			
		} catch (Exception ex) {
			logger.error("Failed to get site details from Premise Master. Error detail: " + ex);
			return null;
		}
	}
    
	public List<LocationResponse> getLocationsBySiteIds(String ocn, List<String> siteIds) {

		logger.info("Calling search()------------------------------------------------------ to get locations for OCN: " + ocn + " , Site Ids: " + siteIds.toString());

		net.colt.coltpremisemaster.common.websvcprovider.eorder.SearchRequest searchRequest = new net.colt.coltpremisemaster.common.websvcprovider.eorder.SearchRequest();

		net.colt.xml.ns.pm.pm2cc.v1.SearchRequest searchRequestInternal = new net.colt.xml.ns.pm.pm2cc.v1.SearchRequest();
		searchRequest.setSearchRequest(searchRequestInternal);
		searchRequestInternal.setSourceSystem(PremiseMasterSourceSystemType.NOVITAS);
		searchRequestInternal.setSearchMode(SEARCH_MODE_2);

		List<PremiseMasterSiteInfoWithLocationAddressType> premiseMasterSiteInfoWithLocationAddressTypeList = searchRequestInternal
				.getSiteInfoWithAddress();

		for (String siteId : siteIds) {

			PremiseMasterSiteInfoWithLocationAddressType premiseMasterSiteInfoWithLocationAddressType = new PremiseMasterSiteInfoWithLocationAddressType();
			PremiseMasterSiteType premiseMasterSiteType = new PremiseMasterSiteType();

			SiteType siteType = new SiteType();
			premiseMasterSiteType.setSite(siteType);
			premiseMasterSiteType.getOCN().addAll(Collections.singletonList(ocn));

			SiteIdType siteIdType = new SiteIdType();
			siteIdType.setSiteId(siteId);
			siteIdType.setSiteIdType(SiteIdTypeType.SITE_INVENTORY_ID);
			siteType.setSiteId(siteIdType);

			premiseMasterSiteInfoWithLocationAddressType.setSite(premiseMasterSiteType);
			premiseMasterSiteInfoWithLocationAddressTypeList.add(premiseMasterSiteInfoWithLocationAddressType);
		}

		searchRequestInternal.getSiteInfoWithAddress();

		PremiseMasterSearchFilterOptionsType filterOptions = new PremiseMasterSearchFilterOptionsType();
		filterOptions.getAccessTech().add(AccessTechType.ETHERNET_OVER_MMSP);
		//searchRequestInternal.setFilterOptions(filterOptions);

		return callPremiseMasterForSiteInfo(searchRequest);
	}

	
	private PremiseMasterSiteInfoWithLocationAddressType getLocationforSiteIdAndOcn(String locationId,String ocn) throws Exception{
		
		SearchRequest searchRequest = new SearchRequest();
		net.colt.xml.ns.pm.pm2cc.v1.SearchRequest searchRequestInternal = new net.colt.xml.ns.pm.pm2cc.v1.SearchRequest();
		searchRequest.setSearchRequest(searchRequestInternal);
		
		List<PremiseMasterSiteInfoWithLocationAddressType> premiseMasterSiteInfoWithLocationAddressTypeList = searchRequestInternal.getSiteInfoWithAddress();
		PremiseMasterSiteInfoWithLocationAddressType premiseMasterSiteInfoWithLocationAddressType = new PremiseMasterSiteInfoWithLocationAddressType();
		premiseMasterSiteInfoWithLocationAddressTypeList.add(premiseMasterSiteInfoWithLocationAddressType);
		
		searchRequestInternal.setSourceSystem(PremiseMasterSourceSystemType.NOVITAS);
		searchRequestInternal.setSearchMode(SEARCH_MODE_2);

		PremiseMasterSiteType premiseMasterSiteType = new PremiseMasterSiteType();
		SiteType siteType = new SiteType();
		SiteIdType siteIdType = new SiteIdType();
		siteIdType.setSiteId(locationId);
		siteIdType.setSiteIdType(SiteIdTypeType.SITE_INVENTORY_ID);
		siteType.setSiteId(siteIdType);
        
		if(ocn==null) ocn="";
		
		premiseMasterSiteType.getOCN().addAll(Collections.singletonList(ocn));
		premiseMasterSiteType.setSite(siteType);
		premiseMasterSiteInfoWithLocationAddressType.setSite(premiseMasterSiteType);

		
		PremiseMasterSearchFilterOptionsType filterOptions = new PremiseMasterSearchFilterOptionsType();
		filterOptions.getAccessTech().add(AccessTechType.ETHERNET_OVER_MMSP);
		//searchRequestInternal.setFilterOptions(filterOptions);

		try {
			EOrderPortType eOrderPortTypeCallback = soapClient.getServiceObject(RESOURCE_SOAP_LOCATION_URL,
					EOrder.SERVICE, EOrderPortType.class);
			Map<String, Object> req_ctx = ((BindingProvider) eOrderPortTypeCallback).getRequestContext();
			soapClient.setAuthenticationToken(req_ctx, RESOURCE_SOAP_USERNAME, RESOURCE_SOAP_PASSWORD);
			soapClient.setTimeOutValues(req_ctx, SystemName.PREMISE_MASTER);
			soapClient.addLogHandler(eOrderPortTypeCallback);
			return eOrderPortTypeCallback.search(searchRequest).getSearchResponse()
					.getSearchResults().getSiteInfoWithAddress().get(0);
				
			
		} catch (Exception ex) {
			logger.error("Failed to get location from Premise Master. Error detail: " + ex);
			throw new Exception(ex);
		}
	}
	
	// Calling from JBPM
    public Object getLocationforSourceOrder(String requestId) throws Exception{
		
		SearchRequest searchRequest = new SearchRequest();
		net.colt.xml.ns.pm.pm2cc.v1.SearchRequest searchRequestInternal = new net.colt.xml.ns.pm.pm2cc.v1.SearchRequest();
		searchRequest.setSearchRequest(searchRequestInternal);
		List<PremiseMasterSiteInfoWithLocationAddressType> premiseMasterSiteInfoWithLocationAddressTypeList = searchRequestInternal.getSiteInfoWithAddress();
		PremiseMasterSiteInfoWithLocationAddressType premiseMasterSiteInfoWithLocationAddressType = new PremiseMasterSiteInfoWithLocationAddressType();
		premiseMasterSiteInfoWithLocationAddressTypeList.add(premiseMasterSiteInfoWithLocationAddressType);
		searchRequestInternal.setSourceSystemOrderNumber(requestId);
		searchRequestInternal.setSourceSystem(PremiseMasterSourceSystemType.NOVITAS);
		searchRequestInternal.setSearchMode(SEARCH_MODE_2);
        
		PremiseMasterSiteType premiseMasterSiteType = new PremiseMasterSiteType();
		SiteType siteType = new SiteType();
		SiteIdType siteIdType = new SiteIdType();
		siteIdType.setSiteId("-1");
		siteIdType.setSiteIdType(SiteIdTypeType.SITE_INVENTORY_ID);
		siteType.setSiteId(siteIdType);
		premiseMasterSiteType.setSite(siteType);
		premiseMasterSiteInfoWithLocationAddressType.setSite(premiseMasterSiteType);

		PremiseMasterSearchFilterOptionsType filterOptions = new PremiseMasterSearchFilterOptionsType();
		filterOptions.getAccessTech().add(AccessTechType.ETHERNET_OVER_MMSP);
		//searchRequestInternal.setFilterOptions(filterOptions);

		try {
			EOrderPortType eOrderPortTypeCallback = soapClient.getServiceObject(RESOURCE_SOAP_LOCATION_URL,
					EOrder.SERVICE, EOrderPortType.class);
			Map<String, Object> req_ctx = ((BindingProvider) eOrderPortTypeCallback).getRequestContext();
			soapClient.setAuthenticationToken(req_ctx, RESOURCE_SOAP_USERNAME, RESOURCE_SOAP_PASSWORD);
			soapClient.setTimeOutValues(req_ctx, SystemName.PREMISE_MASTER);
			soapClient.addLogHandler(eOrderPortTypeCallback);
            SearchResponse2 searchResponse2 = eOrderPortTypeCallback.search(searchRequest);
            List<PremiseMasterSiteInfoWithLocationAddressType>  sites  = searchResponse2.getSearchResponse().getSearchResults().getSiteInfoWithAddress();
			if(null == sites || sites.isEmpty() ){
				logger.error("Premise Master response is null or empty|"+requestId);
				return "FAILED";
			}
			LocationResponse loc = LocationResponseUtil.getLocationResponse(sites.get(0)); 
			if(null == loc) return null;
			BuildingResponse buildResp = getBuildingInfoByBuildingId(loc.getBuildingId(), loc.getCountry()).get(0);
			
			List<PricingBuildingTypeResponse> pricingBuildingList = getPricingBuildingType(loc.getBuildingId());
			PricingBuildingTypeResponse pricingBuilding = null != pricingBuildingList && pricingBuildingList.size() > 0 ?  pricingBuildingList.get(0) : null;
						
			updateSiteTypePrefix(loc.getBuildingId(), loc.getCountry(), buildResp, "", loc, pricingBuilding, loc.getCity());
					
			loc.setColtRegion(APIHelper.getRegionForCountry(loc.getCountry()));
			return loc;
		} catch (Exception ex) {
			logger.error("Failed to get location from Premise Master. Error detail: " + ex);
			throw new Exception(ex);
		}
	}
    
    
    private static void updateSiteTypePrefix(String buildingId,String country,BuildingResponse buildResp,String type,LocationResponse locaResp,PricingBuildingTypeResponse buildingType,String city){
    	
   	 // Use the proactive management flag passed for each site from Premise Master to determine if the site 
       // is a DCNet or LANLink site. Change the code to construct the PM_COMPOSITE_SITE_TYPE from the new 
       // proactive management flag instead of the SITE_TYPE - e.g. instead of constructing a composite site 
       // type value of "CEA-GERMANY-FRANKFURT", use "PROACTIVE-GERMANY-FRANKFURT". Sites that are proactively
       // managed will be DCNET sites. Sites that are *not* proactively managed will be LANLINK sites.
	   	String category = null;
	  // 	BuildingResponse buildResp = getBuildingInfoByBuildingId(buildingId, country).get(0);
		if (null != buildResp) {
			locaResp.setBuildingType(buildResp.getBuildingCategory());
			category = buildResp.getBuildingCategory();
			if (StringUtils.isNotBlank(buildResp.getBuildingName())) {
				locaResp.setBuildingName(buildResp.getBuildingName());
			}
		}
		
		if (null != buildingType && buildingType.getCrossConnectAllowed()) {
			buildResp.setCrossConnectAllowed(true);
			locaResp.setCrossConnectAllowed(true);
		} else {
			buildResp.setCrossConnectAllowed(false);
			locaResp.setCrossConnectAllowed(false);
		}
		
       String prefix = "";
       //Passing type as null assuming it's for Standard
       if(StringUtils.isBlank(type)){
    	   prefix = getSiteTypeByBuildingCategory(category,buildingType);
        }else{
        	if("AZURE_EXPRESS".equals(type) || "AWS_HOSTED".equals(type) || "AWS_DEDICATED".equals(type) ){
        		prefix = type;
        	}
        }
       String siteType= prefix+"-" + country + "-" + city;
       locaResp.setType(siteType.toUpperCase());
       
    }
    
	private static String getSiteTypeByBuildingCategory(String category,PricingBuildingTypeResponse buildingType){
	    String prefix="";
	    ConfigReader pr = new ConfigReader();
	    ConfigurationService config = pr.getConfigClient();
	    if(null !=config){
	    	 Map<String, String> prop = config.getProperties(PRIVATE_BD_CATEGORY_KEY,PUBLIC_BD_CATEGORY_KEY);
	    	 List<String> privateBD = Arrays.asList(prop.get(PRIVATE_BD_CATEGORY_KEY).split("\\s*,\\s*"));
	    	 List<String> publicBD = Arrays.asList(prop.get(PUBLIC_BD_CATEGORY_KEY).split("\\s*,\\s*"));
	    	 
			if (buildingType != null && "KDC".equals(buildingType.getSiteType())) {
				prefix = "KDC";
			} else if (privateBD.contains(category)) {
				prefix = "L"; // P
			} else if (publicBD.contains(category)) {
				prefix = "D"; // NP
			}
		}
	    
	    return prefix;
	}
	
	private List<PricingBuildingTypeResponse> getPricingBuildingType(String buildingId) {
		ConfigReader pr = new ConfigReader();
		ConfigurationService config = pr.getConfigClient();
		List<PricingBuildingTypeResponse> buildingTypeList = null;
		if (null != config) {
			Map<String, String> prop = config.getProperties(PRICE_API_URL, PRICE_API_USERNAME, PRICE_API_PASSWORD);
			buildingTypeList = APIHelper.getPricingBuildingType(prop.get(PRICE_API_URL), prop.get(PRICE_API_USERNAME),
					prop.get(PRICE_API_PASSWORD), buildingId);
		}
		return buildingTypeList;
	}
	
	public List<LocationResponse> getLocations(SearchResponse searchResponse, LocationAPIClient client) {

		List<LocationResponse> locationList = new ArrayList<LocationResponse>();
		PremiseMasterResponseDataType response = searchResponse.getResponse();
		if (response.equals(PremiseMasterResponseDataType.SUCCESS)) {
			logger.info("Received site from PM");
			List<PremiseMasterSiteInfoWithLocationAddressType> siteInfoWithAddressList = searchResponse.getSearchResults().getSiteInfoWithAddress();
			BuildingResponse buildResp = null;

			List<PricingBuildingTypeResponse> pricingBuildingList = null;
			
			if (siteInfoWithAddressList != null && siteInfoWithAddressList.size() > 0) {
				pricingBuildingList = getPricingBuildingType(null);
			}
			
			String region = null;
			for (PremiseMasterSiteInfoWithLocationAddressType siteInfoWithAddress : siteInfoWithAddressList) {

				if (siteInfoWithAddress.getSite().getSite().getSiteType() != null) {
					LocationResponse loc = LocationResponseUtil.getLocationResponse(siteInfoWithAddress);
					if (null != loc) {
						if (null == buildResp) {
							buildResp = getBuildingInfoByBuildingId(loc.getBuildingId(), loc.getCountry()).get(0);
						}

						if (null == region) {
							region = APIHelper.getRegionForCountry(loc.getCountry());
						}

						updateSiteTypePrefix(loc.getBuildingId(), loc.getCountry(), buildResp, null, loc,
								getBuildingTypeFromList(pricingBuildingList, loc.getBuildingId()), loc.getCity());

						locationList.add(loc);
						loc.setColtRegion(region);
					}
				}
			}

		} else {
			logger.error("No Sites Found");
		}

		logger.info("locationList size" + locationList.size());

		return locationList;
	}
	
	private PricingBuildingTypeResponse getBuildingTypeFromList(List<PricingBuildingTypeResponse> pricingBuildingList,
			String buildingId) {
       if(null !=pricingBuildingList) {
		PricingBuildingTypeResponse pricingBuilding = pricingBuildingList.stream()
				.filter(building -> buildingId.equalsIgnoreCase(building.getBuildingId()))
				.findAny()
				.orElse(null);
		return pricingBuilding;
       }
       return null;
	}
	
	
/*	public static void main(String[] args) throws Exception {
	
	LocationAPIClient client = new LocationAPIClient("http://amsnov02:8080/webmethod-mock-api/premisemasterlocation?wsdl","jBPMUser","jBPMUser"); //LON00001/001, LON01595/009

	Map<String, Object> req = new HashMap<>();
	req.put("building_id", "UKLON-0000005703");
	req.put("ocn", null);
	
	//System.out.println(client.getBuildingInfoByBuildingId("UKLON-0000005722", "United Kingdom"));
	Map<String, Object> requestParamMap = new HashMap<>();
	//requestParamMap.put("country", "United Kingdom");
	//requestParamMap.put("city", "London");
	requestParamMap.put("building_id", "UKLON-0000005702");
	//System.out.println(client.getAllBuildingInfo(requestParamMap));
	
	System.out.println(client.getSiteInfoByBuildingId(requestParamMap));
	
	//System.out.println(client.getLocationforSourceOrder("235103"));
	//client.getSiteByLocationId("", "LON0000017947/009");
	}*/
	
	public static void main(String[] args) throws Exception {

		LocationAPIClient client = new LocationAPIClient(
				"http://wmisb131.internal.colt.net/ws/ColtPremiseMaster.common.webSvcProvider:eOrder/ColtPremiseMaster_common_webSvcProvider_eOrder_Port?wsdl",
				"jBPMUser", "jBPMUser"); // LON00001/001, LON01595/009

		Map<String, Object> req = new HashMap<>();
		client.getLocationforSourceOrder("12133");
	}

}
