package com.colt.novitas.resource.location.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;

public class BuildingResponse implements Serializable, Comparable<BuildingResponse> {

	private static final long serialVersionUID = -1020745780038246984L;

	private String buildingId;
	private String address;
	private String country;
	private String city;
	private String buildingName;
	private String floorSuite;
	private String streetName;
	private String premisesNumber;
	private String state;
	private String postalZipCode;
	private String buildingCategory;
    private String departmentBranch;
    private Float latitude;
	private Float longitude;
	private String coltRegion;
	private String localCity;
	private String localStreetName;
	private String localBuildingName;
	//private List<String> reachableTypeList;
	private Boolean crossConnectAllowed = false;
	private Boolean buildingReachable;
	
	public BuildingResponse() {
		super();
	}

	public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}
   

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getFloorSuite() {
		return floorSuite;
	}

	public void setFloorSuite(String floorSuite) {
		this.floorSuite = floorSuite;
	}

	public String getDepartmentBranch() {
		return departmentBranch;
	}

	public void setDepartmentBranch(String departmentBranch) {
		this.departmentBranch = departmentBranch;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country
	 *            the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}


	public String getBuildingName() {
		return buildingName;
	}

	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}


	public String getPremisesNumber() {
		return premisesNumber;
	}

	public void setPremisesNumber(String premisesNumber) {
		this.premisesNumber = premisesNumber;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPostalZipCode() {
		return postalZipCode;
	}

	public void setPostalZipCode(String postalZipCode) {
		this.postalZipCode = postalZipCode;
	}

	public String getBuildingCategory() {
		return buildingCategory;
	}

	public void setBuildingCategory(String buildingCategory) {
		this.buildingCategory = buildingCategory;
	}
	
	public Float getLatitude() {
		return latitude;
	}

	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}

	public Float getLongitude() {
		return longitude;
	}

	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}
	
	public String getLocalCity() {
		return localCity;
	}

	public String getColtRegion() {
		return coltRegion;
	}

	public void setColtRegion(String coltRegion) {
		this.coltRegion = coltRegion;
	}


	public void setLocalCity(String localCity) {
		this.localCity = localCity;
	}

	public String getLocalStreetName() {
		return localStreetName;
	}

	public void setLocalStreetName(String localStreetName) {
		this.localStreetName = localStreetName;
	}
	

	public String getLocalBuildingName() {
		return localBuildingName;
	}

	public void setLocalBuildingName(String localBuildingName) {
		this.localBuildingName = localBuildingName;
	}

/*	public List<String> getReachableTypeList() {
		if(reachableTypeList==null){
			reachableTypeList = new ArrayList<String>();
		}
		return reachableTypeList;
	}

	public void  addReachableType(String reachableType){
		
		getReachableTypeList().add(reachableType);
	}*/
	
	public Boolean getCrossConnectAllowed() {
		return crossConnectAllowed;
	}

	public void setCrossConnectAllowed(Boolean crossConnectAllowed) {
		this.crossConnectAllowed = crossConnectAllowed;
	}

	public Boolean getBuildingReachable() {
		return buildingReachable;
	}

	public void setBuildingReachable(Boolean buildingReachable) {
		this.buildingReachable = buildingReachable;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	@Override
	public int compareTo(BuildingResponse building) {
		int res = this.buildingId.compareTo(building.buildingId);
		if (res != 0) {
			return res;
		}
		return this.buildingName.compareTo(building.buildingName);
	}
	
}
