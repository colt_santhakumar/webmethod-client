package com.colt.novitas.resource.location.util;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.resource.location.response.CountryRegionResponse;
import com.colt.novitas.resource.price.response.PricingBuildingTypeResponse;
import com.colt.novitas.resource.service.ConfigurationService;
import com.fasterxml.jackson.databind.ObjectMapper;

public class APIHelper {
	private static final Logger log = LoggerFactory.getLogger("WM Location client|"+APIHelper.class.getName());
	private static final String UNKNWN = "UNKNOWN";
	
	private static final String SERVICE_BASE = "serviceinventory.baseurl";
	private static final String USER_NAME ="serviceinventory.username";
	private static final String PASSWD = "serviceinventory.password";
	
	
	public static String getRegionForCountry(String countryName) {

		String region = UNKNWN;
		CountryRegionResponse response = null;
		try {
			response = getCountryRegion(countryName);
			if (null != response)
				region = response.getRegion();
		} catch (Exception e) {
			log.error("Error occured while getting country region from Service API", e);
			return UNKNWN;
		}
		return region;
	}
	
	public static CountryRegionResponse getCountryRegion(String countryName) {
		ConfigReader pr = new ConfigReader();
		ConfigurationService config = pr.getConfigClient();
		Map<String, String> prop = config.getProperties(SERVICE_BASE, USER_NAME, PASSWD);
		String baseUrl = prop.get(SERVICE_BASE);
		String userName = prop.get(USER_NAME);
		String password = prop.get(PASSWD);

		CountryRegionResponse region = null;

		CloseableHttpClient httpclient = HttpClients.createDefault();
		CloseableHttpResponse response = null;
		try {
			baseUrl = baseUrl + "/region/" + URLEncoder.encode(countryName, "UTF-8").replaceAll("\\+", "%20");
			HttpGet httpGet = new HttpGet(baseUrl);
			if (userName != null && password != null) {
				String authString = userName + ":" + password;
				byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
				httpGet.addHeader("Authorization", "Basic " + new String(authEncBytes));
			}
			httpGet.addHeader("Content-Type", "application/json;charset=UTF-8");
			response = httpclient.execute(httpGet);
			HttpEntity entity = response.getEntity();
			String responseBody = EntityUtils.toString(entity, Charset.forName("UTF-8"));
			ObjectMapper mapper = new ObjectMapper();
			region = mapper.readValue(responseBody, CountryRegionResponse.class);
			EntityUtils.consume(entity);
		} catch (Exception e) {
			log.error("Error occured while getting country region detail from Service API", e);
			return region;
		} finally {
			if (null != response) {
				try {
					response.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return region;
	}
	
	
	public static List<PricingBuildingTypeResponse> getPricingBuildingType(String url, String username, String password,
			String buildingId) {
		HttpGet httpGet = null;
		CloseableHttpClient httpClient = null;
		httpClient = HttpClients.createDefault();
		
		if (null != buildingId)
			url = url + "/key_building?building_id=" + buildingId;
		else
			url = url + "/key_building";
		
		httpGet = new HttpGet(url);
		
		byte[] encodedBytes = Base64.encodeBase64((username + ":" + password).getBytes());
		httpGet.addHeader("Authorization", new String(encodedBytes));
		log.info("Getting PriceBuilding type from price-api uri=" + url);
		CloseableHttpResponse response = null;
		try {
			response = httpClient.execute(httpGet);

			if (response.getStatusLine().getStatusCode() == 200) {

				HttpEntity httpEntity = response.getEntity();
				if (httpEntity != null) {
					String retSrc = EntityUtils.toString(httpEntity);
					ObjectMapper objectMapper = new ObjectMapper();
					if (null != buildingId) {
						PricingBuildingTypeResponse buildingType = objectMapper.readValue(retSrc,
								PricingBuildingTypeResponse.class);
						return new ArrayList<>(Arrays.asList(buildingType));
					} else {
						PricingBuildingTypeResponse[] buildingTypeArray = objectMapper.readValue(retSrc, PricingBuildingTypeResponse[].class);
						return Arrays.asList(buildingTypeArray);
					}
				}

			} else {
				log.error("NOT OK - response.getStatusLine().getStatusCode() =" + response.getStatusLine().getStatusCode());
			}
		} catch (Exception e) {
			log.error("Error while calling pricing api to GET building type", e.getMessage());
		} finally {
			if (null != response) {
				try {
					response.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return null;
	}

}
