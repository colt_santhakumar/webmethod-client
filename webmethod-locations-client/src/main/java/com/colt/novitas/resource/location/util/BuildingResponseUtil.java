package com.colt.novitas.resource.location.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.colt.novitas.resource.location.response.BuildingResponse;
import com.colt.novitas.resource.service.ConfigurationService;

import net.colt.xml.ns.cbe.cdd.v1.BuildingType;
import net.colt.xml.ns.cbe.cdd.v1.LocationAddressType;
import net.colt.xml.ns.pm.common.v1.PremiseMasterBuildingInfoWithLocationAddressType;
import net.colt.xml.ns.pm.common.v1.PremiseMasterLocationAddressType;
import net.colt.xml.ns.pm.common.v1.PremiseMasterResponseDataType;
import net.colt.xml.ns.pm.pm2cc.v1.SearchResponse;

public class BuildingResponseUtil {
	private static final Logger log = LoggerFactory.getLogger("Webmethod Location|"+BuildingResponseUtil.class.getSimpleName());
	private static final String PRIVATE_BD_CATEGORY_KEY = "private.building.category.key";
	private static final String PUBLIC_BD_CATEGORY_KEY = "public.building.category.key";
	
	public static BuildingResponse getBuildingResponse(PremiseMasterBuildingInfoWithLocationAddressType buildingAddress) {

    	try{
    		BuildingResponse building = new BuildingResponse();
    		BuildingType build = buildingAddress.getBuilding().getBuilding();
    		if(!checkBuildingCategory(build.getBuildingCategory().value())){
    			log.warn("Building category is not matched with configured|"+build.getBuildingCategory().value());
    			return null;
    		}
 	        building.setBuildingId(build.getBuildingId().getBuildingId());
	        List<PremiseMasterLocationAddressType> buildingLocs = buildingAddress.getAssociatedLocationAddressList().getLocationAddress();
	         if(null != build.getBuildingCategory() && buildingLocs.size() > 0){
	        	 building.setBuildingCategory(build.getBuildingCategory().value());
	        	 PremiseMasterLocationAddressType masterLoc = buildingLocs.get(0);
				 if (StringUtils.isNotBlank(masterLoc.getLatitude()) && !masterLoc.getLatitude().equalsIgnoreCase("null"))
					building.setLatitude(Float.valueOf(masterLoc.getLatitude()));
				 if (StringUtils.isNotBlank(masterLoc.getLongitude()) && !masterLoc.getLongitude().equalsIgnoreCase("null"))
					building.setLongitude(Float.valueOf(masterLoc.getLongitude()));
	        	 LocationAddressType buildingAdd =  masterLoc.getLocationAddress();
	        	 building.setAddress(AddressFormatHelper.formatAddress(buildingAdd));
	        	 building.setFloorSuite(buildingAdd.getFloorSuite());
	        	 building.setPremisesNumber(buildingAdd.getPremisesNumber());
	        	 building.setPostalZipCode(buildingAdd.getPostalZipCode());
	        	 building.setStreetName(buildingAdd.getStreetName());
	        	 building.setState(buildingAdd.getState());
	        	 building.setCity(buildingAdd.getCityTown());
	        	 building.setCountry(buildingAdd.getCountry());
	        	 building.setDepartmentBranch(buildingAdd.getDepartmentBranch());
				if (null != build.getCarrierHotel() && StringUtils.isNotBlank(build.getCarrierHotel().getName())) {
					building.setBuildingName(build.getCarrierHotel().getName());
				} else if (StringUtils.isNotBlank(buildingAdd.getBuildingName())) {
					building.setBuildingName(buildingAdd.getBuildingName());
				} 
				
				/*else {  commented as per Javier request
				building.setBuildingName(buildingAdd.getPremisesNumber() + " " + buildingAdd.getStreetName() + " "	+ buildingAdd.getPostalZipCode());
			    }*/
				
	        	 building.setLocalCity(masterLoc.getCityNameLocalLanguage());
	        	 building.setLocalStreetName(masterLoc.getStreetNameLocalLanguage());
	        	 //building.setColtRegion(APIHelper.getRegionForCountry(building.getCountry(), url, userName, password));
	        	 building.setLocalBuildingName(masterLoc.getBuildingNameLocalLanguage());
	        	 
	        	 
	        	 building.setBuildingReachable(build.getReachable().stream().anyMatch(r -> r.name().equals("ETHERNET_OVER_MMSP")));
	        	 
	        	 /*log.info("building getReachable size="+build.getReachable().size());
	        	 build.getReachable()
	        	 				.stream()
	        	 					.map(e->e.toString())
	        	 						.forEach(building::addReachableType);
	        	 log.info("response building getReachable size="+building.getReachableTypeList().size()+ " value=" + building.getReachableTypeList() );*/
	        	 return building; 
	         }
    	}catch(Exception ex){
    		log.error("Error while converting response",ex);
    		return null;
    	}
    	return null;
         
    }
	
	public static List<BuildingResponse> getBuildingInfo(SearchResponse searchResponse) {
		List<BuildingResponse> buildingList = new ArrayList<BuildingResponse>();
		PremiseMasterResponseDataType response = searchResponse.getResponse();
		if (response.equals(PremiseMasterResponseDataType.SUCCESS)) {
			
			List<PremiseMasterBuildingInfoWithLocationAddressType> premiseMasterBuildingInfoWithLocationAddressTypeList = searchResponse
					.getSearchResults().getBuildingInfoWithAddress();
			String region = null;
			
			for (PremiseMasterBuildingInfoWithLocationAddressType buildingInfoWithAddress : premiseMasterBuildingInfoWithLocationAddressTypeList) {
				if (buildingInfoWithAddress.getBuilding() != null) {
					BuildingResponse resp = getBuildingResponse(buildingInfoWithAddress);
					if (null != resp) {
						if (null == region)
							region = APIHelper.getRegionForCountry(resp.getCountry());
						resp.setColtRegion(region);
						buildingList.add(resp);
					}
				}
			}
		} else {
			log.error("PM Get BuildingInfo call failed");
		}
		return buildingList;
	}
    
    /**
	 * <p> Formats the address and returns in String format </p>
	 * 
	 * @param LocationAddressType site
	 * @return buildingName - premisesNumber streetName, postalZipCode. cityTown, country.
	 * */
/*	private static String formatAddress(LocationAddressType site){
		String seperator = "";
		StringBuffer formattedAddress = new StringBuffer();
		
		//create first part of the address
		//premisesNumber, streetName, postalZipCode.
		if (StringUtils.isNotEmpty(site.getPremisesNumber())
				|| StringUtils.isNotEmpty(site.getStreetName()) || StringUtils.isNotEmpty(site.getPostalZipCode())){
			if (StringUtils.isNotEmpty(site.getPremisesNumber()) || StringUtils.isNotEmpty(site.getStreetName())){
				if (StringUtils.isNotEmpty(site.getPremisesNumber())){
					formattedAddress.append(site.getPremisesNumber());
					seperator = " ";
				}
				if (StringUtils.isNotEmpty(site.getStreetName())){
					if (StringUtils.isNotEmpty(seperator)){
						formattedAddress.append(seperator);
						seperator = "";
					}
					formattedAddress.append(site.getStreetName());
				}
				seperator = " ";
			} else {
				if (StringUtils.isNotEmpty(seperator)){
					seperator = ", ";
				}
			}
			if (StringUtils.isNotEmpty(site.getPostalZipCode())){
				if (StringUtils.isNotEmpty(seperator)){
					formattedAddress.append(seperator);
					seperator = "";
				}
				formattedAddress.append(site.getPostalZipCode());
			}
			formattedAddress.append(".");
			seperator = " ";
		}
		
		//create second part of the address 
		//cityTown, country.
		if (StringUtils.isNotEmpty(site.getCityTown()) || StringUtils.isNotEmpty(site.getCountry())){
			if (StringUtils.isNotEmpty(seperator)){
				formattedAddress.append(seperator);
				seperator = "";
			}
			if (StringUtils.isNotEmpty(site.getCityTown())){
				formattedAddress.append(site.getCityTown());
				seperator = ", ";
			}
			if (StringUtils.isNotEmpty(site.getCountry())){
				if (StringUtils.isNotEmpty(seperator)){
					formattedAddress.append(seperator);
					seperator = "";
				}
				formattedAddress.append(site.getCountry());
			}
			formattedAddress.append(".");
		}
		
		return formattedAddress.toString();
	}*/
	
	private static boolean checkBuildingCategory(String buildingCategory){
	    boolean valid =false;
	    ConfigReader pr = new ConfigReader();
	    ConfigurationService config = pr.getConfigClient();
	    if(null !=config){
	    	 Map<String, String> prop = config.getProperties(PRIVATE_BD_CATEGORY_KEY,PUBLIC_BD_CATEGORY_KEY);
	    	 List<String> privateBD = Arrays.asList(prop.get(PRIVATE_BD_CATEGORY_KEY).split("\\s*,\\s*"));
	    	 List<String> publicBD = Arrays.asList(prop.get(PUBLIC_BD_CATEGORY_KEY).split("\\s*,\\s*"));
	    	 if(privateBD.contains(buildingCategory) || publicBD.contains(buildingCategory)){
					valid = true;
				}
	        } 
	    return valid;
	   }
	 

}
