package com.colt.novitas.resource.location.response;

import java.io.Serializable;

import org.apache.commons.lang.builder.ToStringBuilder;

public class LocationResponse implements Serializable {

	private static final long serialVersionUID = -1020745780038246984L;

	private String id;
	private String name;
	private String address;
	private String type;

	private String siteType;
	private String country;
	private String city;

	private Float latitude;
	private Float longitude;
	private String buildingName;
	private String streetName;
	private String floor;
	private String roomName;
	private String premisesNumber;
	private String state;
	private String postalZipCode;
	private String buildingId;
	private String buildingType;
	private String coltRegion;
	private String localBuildingName;
	private String cityCode;
	private String countryCode;
	private Boolean crossConnectAllowed;

	/**
	 * The following values from PM should result in a TRUE value in the new
	 * column: "CUST PRIVATE" ,"OFFNET CUST" ,"CUST PRIVATE VIA LEASED BEARER"
	 * ,"CUST PRIVATE VIA LEASED FIBRE" "CARRIER HOTEL-CUSTOMER PRIVATE"
	 */
	private boolean isPrivateSite;

	public LocationResponse() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * @return the siteType
	 */
	public String getSiteType() {
		return siteType;
	}

	/**
	 * @param siteType
	 *            the siteType to set
	 */
	public void setSiteType(String siteType) {
		this.siteType = siteType;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country
	 *            the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	public Float getLatitude() {
		return latitude;
	}

	public void setLatitude(Float latitude) {
		this.latitude = latitude;
	}

	public Float getLongitude() {
		return longitude;
	}

	public void setLongitude(Float longitude) {
		this.longitude = longitude;
	}

	public String getBuildingName() {
		return this.buildingName;
	}

	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

	public String getStreetName() {
		return streetName;
	}

	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	public String getFloor() {
		return floor;
	}

	public void setFloor(String floor) {
		this.floor = floor;
	}

	public String getRoomName() {
		return roomName;
	}

	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	public String getPremisesNumber() {
		return premisesNumber;
	}

	public void setPremisesNumber(String premisesNumber) {
		this.premisesNumber = premisesNumber;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPostalZipCode() {
		return postalZipCode;
	}

	public void setPostalZipCode(String postalZipCode) {
		this.postalZipCode = postalZipCode;
	}

	/**
	 * The following values from PM should result in a TRUE value in the new
	 * column: "CUST PRIVATE" ,"OFFNET CUST" ,"CUST PRIVATE VIA LEASED BEARER"
	 * ,"CUST PRIVATE VIA LEASED FIBRE"
	 * 
	 * @return true or false
	 */
	public boolean isPrivateSite() {
		return isPrivateSite;
	}

	public void setPrivateSite(boolean isPrivateSite) {
		this.isPrivateSite = isPrivateSite;
	}

	public String getBuildingId() {
		return buildingId;
	}

	public void setBuildingId(String buildingId) {
		this.buildingId = buildingId;
	}

	public String getBuildingType() {
		return buildingType;
	}

	public void setBuildingType(String buildingType) {
		this.buildingType = buildingType;
	}

	public String getColtRegion() {
		return coltRegion;
	}

	public void setColtRegion(String coltRegion) {
		this.coltRegion = coltRegion;
	}

	public String getLocalBuildingName() {
		return localBuildingName;
	}

	public void setLocalBuildingName(String localBuildingName) {
		this.localBuildingName = localBuildingName;
	}

	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	
	public Boolean getCrossConnectAllowed() {
		return crossConnectAllowed;
	}

	public void setCrossConnectAllowed(Boolean crossConnectAllowed) {
		this.crossConnectAllowed = crossConnectAllowed;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
