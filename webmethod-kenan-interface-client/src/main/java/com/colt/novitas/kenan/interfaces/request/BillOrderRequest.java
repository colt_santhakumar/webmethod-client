package com.colt.novitas.kenan.interfaces.request;

public class BillOrderRequest {

	private String customerCountryCode;
	private String customerCountryName;
	private String ocn;
	private String bcn;
	private String urlCallback;
	private String novitasServiceId;
	private Integer customerId;
	private String serviceStartDate;
	private String billingCurrency;

	public String getCustomerCountryCode() {
		return customerCountryCode;
	}

	public void setCustomerCountryCode(String customerCountryCode) {
		this.customerCountryCode = customerCountryCode;
	}

	public String getCustomerCountryName() {
		return customerCountryName;
	}

	public void setCustomerCountryName(String customerCountryName) {
		this.customerCountryName = customerCountryName;
	}

	public String getOcn() {
		return ocn;
	}

	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	public String getBcn() {
		return bcn;
	}

	public void setBcn(String bcn) {
		this.bcn = bcn;
	}

	public String getUrlCallback() {
		return urlCallback;
	}

	public void setUrlCallback(String urlCallback) {
		this.urlCallback = urlCallback;
	}

	public String getNovitasServiceId() {
		return novitasServiceId;
	}

	public void setNovitasServiceId(String novitasServiceId) {
		this.novitasServiceId = novitasServiceId;
	}

	public String getServiceStartDate() {
		return serviceStartDate;
	}

	public void setServiceStartDate(String serviceStartDate) {
		this.serviceStartDate = serviceStartDate;
	}

	public String getBillingCurrency() {
		return billingCurrency;
	}

	public void setBillingCurrency(String billingCurrency) {
		this.billingCurrency = billingCurrency;
	}


	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	@Override
	public String toString() {
		return "BillOrderRequest [customerCountryCode=" + customerCountryCode
				+ ", customerCountryName=" + customerCountryName + ", ocn="
				+ ocn + ", bcn=" + bcn + ", urlCallback=" + urlCallback
				+ ", novitasServiceId=" + novitasServiceId + ", customerId="
				+ customerId + ", serviceStartDate=" + serviceStartDate
				+ ", billingCurrency=" + billingCurrency + "]";
	}
	
	

}
