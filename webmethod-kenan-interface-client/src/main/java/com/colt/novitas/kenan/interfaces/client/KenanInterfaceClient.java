package com.colt.novitas.kenan.interfaces.client;

import java.math.BigInteger;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;

import net.colt.xml.nbrq.ns.v1.BillingComponent;
import net.colt.xml.nbrq.ns.v1.BillingComponentAction;
import net.colt.xml.nbrq.ns.v1.BillingComponentDetails;
import net.colt.xml.nbrq.ns.v1.BillingComponentType;
import net.colt.xml.nbrq.ns.v1.BillingOrder;
import net.colt.xml.nbrq.ns.v1.ColtCountryCodeEnumeration;
import net.colt.xml.nbrq.ns.v1.CurrencyCodeEnumerationType;
import net.colt.xml.nbrq.ns.v1.HeaderType;
import net.colt.xml.nbrq.ns.v1.Order;
import net.colt.xml.nbrq.ns.v1.OrderType;
import net.colt.xml.nbrq.ns.v1.Party;
import net.colt.xml.nbrq.ns.v1.RentalFee;
import net.colt.xml.nbrq.ns.v1.Service;
import net.colt.xml.nbrq.ns.v1.ServiceAction;
import net.colt.xml.nbrq.ns.v1.ServiceAddress;
import net.colt.xml.nbrq.ns.v1.ServiceAttribute;
import net.colt.xml.nbrq.ns.v1.ServiceAttributeAction;
import net.colt.xml.nbrq.ns.v1.ServiceAttributeGroup;
import net.colt.xml.nbrq.ns.v1.ServiceSite;
import net.colt.xml.nbrq.ns.v1.SiteLabel;
import net.colt.xml.nbrq.ns.v1.TechnicalContact;
import net.colt.xml.ns.cse.v1.StatusType;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import colt.eu.amsise41.coltbillingproxy.billingorder.v1.BillingOrderPortType;
import colt.eu.amsise41.coltbillingproxy.billingorder.v1.BillingOrderRequest;
import colt.eu.amsise41.coltbillingproxy.billingorder.v1.BillingOrderResponse;

import com.colt.novitas.kenan.interfaces.request.BillOrderRequest;
import com.colt.novitas.soap.SoapClient;

public class KenanInterfaceClient {

	private static final Logger LOG = Logger.getLogger(KenanInterfaceClient.class.getName());

	private String KENAN_SOAP_URL = "http://localhost:8099/";
	private String KENAN_SOAP_USERNAME = "jBPMUser";
	private String KENAN_SOAP_PASSWORD = "jBPMUser";
	
	public KenanInterfaceClient(final String KENAN_SOAP_URL) {
		this.KENAN_SOAP_URL = KENAN_SOAP_URL;
	}

	public KenanInterfaceClient(String KENAN_SOAP_URL, String KENAN_SOAP_USERNAME, String KENAN_SOAP_PASSWORD) {
		this.KENAN_SOAP_URL = KENAN_SOAP_URL;
		this.KENAN_SOAP_USERNAME = KENAN_SOAP_USERNAME;
		this.KENAN_SOAP_PASSWORD = KENAN_SOAP_PASSWORD;
	}

	public KenanInterfaceClient() {

	}

	public String receiveBillingOrder(BillOrderRequest billOrderRequest) {
		LOG.debug("Start >>>  KenanInterfaceClient.receiveBillingOrder()");
		SoapClient soapClient = new SoapClient();
		BillingOrderRequest billingOrderRequest = new BillingOrderRequest();
		Order order = new Order();
		billingOrderRequest.setOrder(order);
		
		HeaderType headerType = new HeaderType();
		headerType.setSenderSystem("Novitas");
		
		order.setServiceHeader(headerType);
		order.setOrderNumber(String.valueOf(billOrderRequest.getCustomerId()));
		order.setPurchaseOrder(" ");
		order.setOrderCountry(ColtCountryCodeEnumeration.valueOf(billOrderRequest.getCustomerCountryCode()));
		order.setUrlCallback(billOrderRequest.getUrlCallback());
		
		Party party = new Party();
		order.setCustomerDetails(party);
		
		party.setPartyNumber(billOrderRequest.getOcn());
		party.setPartyName(" ");

		BillingOrder billingOrder = new BillingOrder();
		order.setBillingOrder(billingOrder);
		billingOrder.setOrderType(OrderType.NEW);

		BillingComponent billingComponent = new BillingComponent();
		billingComponent.setBillingAccountNumber(billOrderRequest.getBcn());
		billingComponent.setBillingComponentID("COLTOD");
		billingComponent.setRelatedServiceID(" ");
		billingComponent.setObjectID(" ");
		billingComponent.setBillingCurrency(CurrencyCodeEnumerationType.valueOf(billOrderRequest.getBillingCurrency()));
		billingComponent.setQuantity(BigInteger.valueOf(1));
		billingComponent.setBillingComponentAction(BillingComponentAction.ADD);
		billingComponent.setBillingComponentDescription(" ");
		billingComponent.setBillingComponentType(BillingComponentType.RENTAL);

		RentalFee rentalFee = new RentalFee();
		rentalFee.setPrice("0");
		rentalFee.setBillingPeriod(" ");
		//rentalFee.setStartDate(billOrderRequest.getServiceStartDate());
		rentalFee.setStartDate(getGregorianDate());
		rentalFee.setEndDate(null);
		BillingComponentDetails billingComponentDetails = new BillingComponentDetails();
		billingComponentDetails.setRentalFee(rentalFee);
		billingComponent.setBillingComponentDetails(billingComponentDetails);

		List<BillingComponent> billingComponentList = billingOrder.getBillingComponent();

		billingComponentList.add(billingComponent);
		List<Service> serviceList = billingOrder.getService();

		Service service = new Service();
		service.setServiceInstanceID(billOrderRequest.getNovitasServiceId());
		service.setParentServiceInstanceID(billOrderRequest.getNovitasServiceId());
		service.setRootServiceInstanceID(billOrderRequest.getNovitasServiceId());
		service.setNetworkID(null);
		service.setProductCode("Colt Product");
		service.setProductName("Novitas");
		service.setBillingAccountNumber(billOrderRequest.getBcn());
		service.setQuantity(BigInteger.valueOf(1));
		service.setStatus(" ");
		service.setCircuitId(" ");
		service.setCustomerReferenceNumber(" ");
		service.setPurchaseOrderNumber(" ");
		service.setServiceStartDate(getGregorianDate());
		service.setServiceEndDate(null);
		service.setServiceAction(ServiceAction.NEW);
		service.setServiceDescription(" ");

		ServiceAttributeGroup serviceAttributeGroup = new ServiceAttributeGroup();
		List<ServiceAttribute> serviceAttributeList = serviceAttributeGroup.getServiceAttribute();
		ServiceAttribute serviceAttribute = new ServiceAttribute();
		serviceAttribute.setAttributeName("circuitId");
		serviceAttribute.setAttributeAction(ServiceAttributeAction.NEW);
		serviceAttribute.setAttributePreviousValue(" ");
		serviceAttribute.setAttributeValue(billOrderRequest.getNovitasServiceId());
		serviceAttributeList.add(serviceAttribute);

		service.setServiceAttributeGroup(serviceAttributeGroup);

		List<ServiceSite> serviceSiteList = service.getServiceSite();

		ServiceAddress serviceAddressPrimary = new ServiceAddress();
		serviceAddressPrimary.setFloorSuite(" ");
		serviceAddressPrimary.setBuildingName(" ");
		serviceAddressPrimary.setDeptartmentBranch(" ");
		serviceAddressPrimary.setPremisesNumber(" ");
		serviceAddressPrimary.setStreetName(" ");
		serviceAddressPrimary.setCityTown(" ");
		serviceAddressPrimary.setState(" ");
		serviceAddressPrimary.setPostalZipCode(" ");
		serviceAddressPrimary.setCountry(billOrderRequest.getCustomerCountryName());
		
		TechnicalContact technicalContactPrimary = new TechnicalContact();
		technicalContactPrimary.setEMailAddress(" ");
		technicalContactPrimary.setFaxNumber(" ");
		technicalContactPrimary.setFirstName(" ");
		technicalContactPrimary.setLastName(" ");
		technicalContactPrimary.setMobileNumber(" ");
		technicalContactPrimary.setPreferredContactMethod(" ");
		technicalContactPrimary.setTelephone(" ");
		technicalContactPrimary.setTitle(" ");
		
		ServiceAddress serviceAddressSecondary = new ServiceAddress();
		serviceAddressSecondary.setFloorSuite(" ");
		serviceAddressSecondary.setBuildingName(" ");
		serviceAddressSecondary.setDeptartmentBranch(" ");
		serviceAddressSecondary.setPremisesNumber(" ");
		serviceAddressSecondary.setStreetName(" ");
		serviceAddressSecondary.setCityTown(" ");
		serviceAddressSecondary.setState(" ");
		serviceAddressSecondary.setPostalZipCode(" ");
		serviceAddressSecondary.setCountry(billOrderRequest.getCustomerCountryName());
		
		TechnicalContact technicalContactSecondary = new TechnicalContact();
		technicalContactSecondary.setEMailAddress(" ");
		technicalContactSecondary.setFaxNumber(" ");
		technicalContactSecondary.setFirstName(" ");
		technicalContactSecondary.setLastName(" ");
		technicalContactSecondary.setMobileNumber(" ");
		technicalContactSecondary.setPreferredContactMethod(" ");
		technicalContactSecondary.setTelephone(" ");
		technicalContactSecondary.setTitle(" ");

		ServiceSite serviceSitePrimary = new ServiceSite();
		serviceSitePrimary.setSiteLabel(SiteLabel.PRIMARY);
		serviceSitePrimary.setServiceParty(party);
		serviceSitePrimary.setServiceAddress(serviceAddressPrimary);
		serviceSitePrimary.setTechnicalContact(technicalContactPrimary);

		ServiceSite serviceSiteSecondary = new ServiceSite();
		serviceSiteSecondary.setSiteLabel(SiteLabel.SECONDARY);
		serviceSiteSecondary.setServiceParty(party);
		serviceSiteSecondary.setServiceAddress(serviceAddressSecondary);
		serviceSiteSecondary.setTechnicalContact(technicalContactSecondary);

		serviceSiteList.add(serviceSitePrimary);
		serviceSiteList.add(serviceSiteSecondary);

		List<BillingComponent> billingComponentListService = service.getBillingComponent();
		billingComponentListService.add(billingComponent);

		serviceList.add(service);



		try {
			BillingOrderPortType serviceBillingOrder = soapClient.getServiceObject(KENAN_SOAP_URL,
					colt.eu.amsise41.coltbillingproxy.billingorder.v1.BillingOrder.SERVICE, BillingOrderPortType.class);
			Map<String, Object> req_ctx = ((BindingProvider) serviceBillingOrder).getRequestContext();
			soapClient.setAuthenticationToken(req_ctx, KENAN_SOAP_USERNAME, KENAN_SOAP_PASSWORD);
			soapClient.addLogHandler(serviceBillingOrder);
			BillingOrderResponse billingOrderResponse = serviceBillingOrder.receiveBillingOrder(billingOrderRequest);
            net.colt.xml.ns.cse.v1.HeaderType head =  billingOrderResponse.getOrderSyncResponse().getOutputHeader();
			
            if(head.getStatus().equals(StatusType.SUCCESS) && StringUtils.isNotBlank(head.getTransactionId())){
            	
            	return head.getTransactionId();
            }
            
            
            return null;

		} catch (Exception ex) {
			LOG.error("Error >>>  KenanInterfaceClient.receiveBillingOrder()" + ex.getMessage());
			System.out.println("FAILED: " + ex.getMessage());
			return null;
		}
	}
    
	private String getGregorianDate(){
		GregorianCalendar c = new GregorianCalendar();
		c.setTime(new Date());
		XMLGregorianCalendar date2 = null;
		try {
			 date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
		} catch (DatatypeConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return date2.toString();
	}
	
	public static void main(String[] args) {

		BillOrderRequest billOrderRequest = new BillOrderRequest();
		billOrderRequest.setBcn("BCN");
		billOrderRequest.setBillingCurrency("GBP");
		billOrderRequest.setCustomerCountryCode("GB");
		billOrderRequest.setCustomerCountryName("UNITED KINGDOM");
		billOrderRequest.setOcn("OCN");
		billOrderRequest.setServiceStartDate("2016-02-03");
		billOrderRequest.setUrlCallback("http://sample");
		billOrderRequest.setNovitasServiceId("1");
        KenanInterfaceClient kenan = new KenanInterfaceClient();
        kenan.receiveBillingOrder(billOrderRequest);

	}

}
