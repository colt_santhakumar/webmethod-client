package com.colt.novitas.property;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.colt.novitas.resource.client.ResourceAPIClient;
import com.colt.novitas.resource.service.ConfigurationService;
import com.colt.novitas.resource.service.ConfigurationServiceImpl;

public class PropertyReader {
	
	private static final Logger LOG = Logger.getLogger(PropertyReader.class);
	private static final String FILE_PATH_PROPERTY = "JBPM_ENV";
	
	private static final String DEFAULT_PROPERTY_FILE = "/jbpm-env.properties";
	private static final String RESOURCE_API_URL_KEY = "resource.new.api.url";
	private static final String RESOURCE_API_ENV_KEY = "resource.new.api.env";
	private Map<String, String> propertiesMap = new HashMap<String, String>();
	private String filePath;
	 private boolean loadFromFile;
	 
	public PropertyReader(String filePath) {
		this.filePath = filePath;
	}
	
	public PropertyReader() {
	}
	
   public ConfigurationService getConfigClient() {
		
		Properties properties = loadProperties();
		ConfigurationService client = null;
		 if(!loadFromFile) {
	            // use the new configurations service to load the properties
	            String configServiceUrl = properties.getProperty(RESOURCE_API_URL_KEY);
	            String environment = properties.getProperty(RESOURCE_API_ENV_KEY);
	            client =  new ConfigurationServiceImpl(configServiceUrl,environment);

	        }
		return client;
		
	}
	
	public EnvironmentProperties getDefaultProperties() {
		
		Properties properties = loadProperties();
		
		 if(!loadFromFile) {
	            // use the new configurations service to load the properties
	            String configServiceUrl = properties.getProperty(RESOURCE_API_URL_KEY);
	            String environment = properties.getProperty(RESOURCE_API_ENV_KEY);

	          ResourceAPIClient client = new ResourceAPIClient(configServiceUrl);

	          propertiesMap = client.getPropertiesAsMap(environment, PropertyKeys.getKeys());

	        }
		
		EnvironmentProperties environmentProp = new EnvironmentProperties();
		environmentProp.setSoapLogRequired(getProperty("is.soap.log.required", properties));
		environmentProp.setRequestLogFilePath(getProperty("request.message.log.filepath", properties));
		environmentProp.setResponseFilepath(getProperty("response.message.log.filepath", properties));
		environmentProp.setFaultMessagePath(getProperty("fault.message.log.filepath", properties));
		
		environmentProp.setXngPortTimeOut(getProperty("xng.port.timeout.milliseconds", properties));
		environmentProp.setXngConnectionTimeOut(getProperty("xng.connection.timeout.milliseconds", properties));
		environmentProp.setPremiseTimeOut(getProperty("premise.timeout.milliseconds", properties));
		
		return environmentProp;
		
	}
	
	public String getProperty(String property) {
		return getProperty(property, null);
	}

	 public String getProperty(String property, Properties prop) {

	        String val = null;

	        if(loadFromFile) {

	            if (prop == null) {
	                prop = loadProperties();
	            }

	            val = prop.getProperty(property);

	        } else {
	            val = propertiesMap.get(property);
	        }

	        return val;

	    }
	
	
	public Properties loadProperties() {
		
		Properties prop = new Properties();
		InputStream input = null;

		try {
			
			if (filePath == null || "".equals(filePath.trim())) {
				
				if (System.getProperty(FILE_PATH_PROPERTY) != null 
						&& !"".equals(System.getProperty(FILE_PATH_PROPERTY))) {
					filePath = System.getProperty(FILE_PATH_PROPERTY);
				} else if (System.getenv(FILE_PATH_PROPERTY) != null 
						&& !"".equals(System.getenv(FILE_PATH_PROPERTY))) {
					filePath = System.getenv(FILE_PATH_PROPERTY);
				}
				else {
					String currentUsersHomeDir = System.getProperty("user.home");
					filePath = currentUsersHomeDir + DEFAULT_PROPERTY_FILE;
				}
			}

			//System.out.println("Loading properties from " + filePath);
			input = new FileInputStream(filePath);

			// load a properties file
			prop.load(input);
			
			return prop;

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}
	
}
