package com.colt.novitas.property;


public enum SystemName {
	
	XNG_PORT("xng_port"),
	XNG_CONNECTION("xng_connection"),
	PREMISE_MASTER("premise_master");
	
	 private final String value;

	 SystemName(String v) {
	        value = v;
	    }

	    public String value() {
	        return value;
	    }

	    public static SystemName fromValue(String v) {
	        for (SystemName c: SystemName.values()) {
	            if (c.value.equals(v)) {
	                return c;
	            }
	        }
	        throw new IllegalArgumentException(v);
	    }

}
