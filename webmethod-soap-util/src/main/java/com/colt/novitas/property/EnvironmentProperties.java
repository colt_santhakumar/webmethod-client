package com.colt.novitas.property;

import java.io.Serializable;

public class EnvironmentProperties implements Serializable {

	

	/**
	 * 
	 */
	private static final long serialVersionUID = 6204990583568351056L;
	
	private String soapLogRequired;
	private String requestLogFilePath;
	private String responseFilepath;
	private String faultMessagePath;
	
	//time out properties
	private String xngPortTimeOut;
	private String xngConnectionTimeOut;
	private String premiseTimeOut;

	
	
	/**
	 * @return the soapLogRequired
	 */
	public String getSoapLogRequired() {
		return soapLogRequired;
	}
	/**
	 * @param soapLogRequired the soapLogRequired to set
	 */
	public void setSoapLogRequired(String soapLogRequired) {
		this.soapLogRequired = soapLogRequired;
	}
	/**
	 * @return the requestLogFilePath
	 */
	public String getRequestLogFilePath() {
		return requestLogFilePath;
	}
	/**
	 * @param requestLogFilePath the requestLogFilePath to set
	 */
	public void setRequestLogFilePath(String requestLogFilePath) {
		this.requestLogFilePath = requestLogFilePath;
	}
	/**
	 * @return the responseFilepath
	 */
	public String getResponseFilepath() {
		return responseFilepath;
	}
	/**
	 * @param responseFilepath the responseFilepath to set
	 */
	public void setResponseFilepath(String responseFilepath) {
		this.responseFilepath = responseFilepath;
	}
	/**
	 * @return the faultMessagePath
	 */
	public String getFaultMessagePath() {
		return faultMessagePath;
	}
	/**
	 * @param faultMessagePath the faultMessagePath to set
	 */
	public void setFaultMessagePath(String faultMessagePath) {
		this.faultMessagePath = faultMessagePath;
	}
	
	/**
	 * @return the premiseTimeOut
	 */
	public String getPremiseTimeOut() {
		return premiseTimeOut;
	}
	/**
	 * @param premiseTimeOut the premiseTimeOut to set
	 */
	public void setPremiseTimeOut(String premiseTimeOut) {
		this.premiseTimeOut = premiseTimeOut;
	}
	/**
	 * @return the xngPortTimeOut
	 */
	public String getXngPortTimeOut() {
		return xngPortTimeOut;
	}
	/**
	 * @param xngPortTimeOut the xngPortTimeOut to set
	 */
	public void setXngPortTimeOut(String xngPortTimeOut) {
		this.xngPortTimeOut = xngPortTimeOut;
	}
	/**
	 * @return the xngConnectionTimeOut
	 */
	public String getXngConnectionTimeOut() {
		return xngConnectionTimeOut;
	}
	/**
	 * @param xngConnectionTimeOut the xngConnectionTimeOut to set
	 */
	public void setXngConnectionTimeOut(String xngConnectionTimeOut) {
		this.xngConnectionTimeOut = xngConnectionTimeOut;
	}
 
	
    
 
	
}
