/**
 * 
 */
package com.colt.novitas.property;

import java.util.ArrayList;
import java.util.List;

/**
 * @author omerio
 *
 */
public enum PropertyKeys {
    
    IS_SOAP_LOG_REQUIRED("is.soap.log.required"),
	REQ_LOG_FILE_PATH("request.message.log.filepath"),
	RES_LOG_FILE_PATH("response.message.log.filepath"),
	FAULT_LOG_FILE_PATH("fault.message.log.filepath"),
	XNG_PORT_TIME_OUT("xng.port.timeout.milliseconds"),
	XNG_CONNECTION_TIME_OUT("xng.connection.timeout.milliseconds"),
	PREMISE_MASTER_TIME_OUT("premise.timeout.milliseconds"),
    AZURE_API_VERSION_ID("azure.api.version.id");
    
    
    public final String key;
    
    private PropertyKeys(String key) {
        this.key = key;
    }
    
    /**
     * Get all the keys as a list
     * @return
     */
    public static List<String> getKeys() {
        List<String> keys = new ArrayList<String>();
        
        for(PropertyKeys key: PropertyKeys.values()) {
            keys.add(key.key);
        }
        
        return keys;
    }

}
