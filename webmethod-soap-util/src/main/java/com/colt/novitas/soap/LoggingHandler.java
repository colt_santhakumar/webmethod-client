package com.colt.novitas.soap;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.apache.log4j.Logger;

import com.colt.novitas.property.EnvironmentProperties;
import com.colt.novitas.property.PropertyReader;

public class LoggingHandler implements SOAPHandler<SOAPMessageContext> {

	private static final Logger LOG = Logger.getLogger(LoggingHandler.class.getName());

	public boolean handleMessage(SOAPMessageContext c) {
		SOAPMessage msg = c.getMessage();

		boolean request = ((Boolean) c.get(SOAPMessageContext.MESSAGE_OUTBOUND_PROPERTY)).booleanValue();
		boolean required = null != getEnvironmentProperties().getSoapLogRequired()
				? Boolean.valueOf(getEnvironmentProperties().getSoapLogRequired())
				: false;

			try {
				if (request) { // This is a request message.
					LOG.info("Logging Request into file:::" + soapMessageToString(msg));
					if (required)  msg.writeTo(getRequestOutStream(c, getEnvironmentProperties().getRequestLogFilePath()));
				} else {
					LOG.info("Logging Response into file:::" + soapMessageToString(msg));
					if (required)  msg.writeTo(getRequestOutStream(c, getEnvironmentProperties().getResponseFilepath())); // This is
																											// the
																											// response
																											// message
				}

			} catch (Exception e) {
				LOG.info("Filed to log message " + e.getMessage());
				return true;
			}
		return true;
	}

	public boolean handleFault(SOAPMessageContext c) {
		SOAPMessage msg = c.getMessage();
		boolean required = null != getEnvironmentProperties().getSoapLogRequired()
				? Boolean.valueOf(getEnvironmentProperties().getSoapLogRequired())
				: false;
		LOG.info("Fault Message" + soapMessageToString(msg));
			try {
				if (required)  msg.writeTo(getRequestOutStream(c, getEnvironmentProperties().getFaultMessagePath()));
			} catch (Exception e) {
				LOG.info("Filed to log message " + e.getMessage());
				return true;
			}
		return true;
	}

	private String getOperationName(SOAPMessageContext context) {

		/*
		 * QName svcn = (QName) context.get(MessageContext.WSDL_SERVICE);
		 * svcn.getLocalPart();
		 */
		QName opn = (QName) context.get(MessageContext.WSDL_OPERATION);
		return opn.getLocalPart();

	}

	private FileOutputStream getRequestOutStream(SOAPMessageContext context, String filePath) {

		String fileName1 = getOperationName(context);
		String fileName2 = new SimpleDateFormat("dd-MM-yyyy:hh:mm:ss:ms'.txt'").format(new Date());

		File file = new File(filePath + fileName1 + fileName2);
		FileOutputStream fop = null;
		try {
			fop = new FileOutputStream(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return fop;
	}

	@Override
	public void close(MessageContext context) {
		// TODO Auto-generated method stub

	}

	@Override
	public Set<QName> getHeaders() {
		// TODO Auto-generated method stub
		return null;
	}

	public static EnvironmentProperties getEnvironmentProperties() {
		return new PropertyReader().getDefaultProperties();

	}

	private String soapMessageToString(SOAPMessage message) {
		String result = null;

		if (message != null) {
			ByteArrayOutputStream baos = null;
			try {
				baos = new ByteArrayOutputStream();
				message.writeTo(baos);
				result = baos.toString();
			} catch (Exception e) {
			} finally {
				if (baos != null) {
					try {
						baos.close();
					} catch (IOException ioe) {
					}
				}
			}
		}
		
		result = result.replaceAll("\\n", "");
		result = result.replaceAll("[>]\\s+[<]","");
		return result;
	}
	
	
	
	

}