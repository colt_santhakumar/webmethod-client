package com.colt.novitas.soap;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.ws.Binding;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;

import org.apache.log4j.Logger;

import com.colt.novitas.property.EnvironmentProperties;
import com.colt.novitas.property.PropertyReader;
import com.colt.novitas.property.SystemName;

public class SoapClient {
	private static final Logger LOG = Logger.getLogger(SoapClient.class);
	public  <T> T getServiceObject(String urlString, QName qname, Class<T> interfaceTye) {

		T proxyObject = null;
		try {
			URL url = new URL(urlString);
			Service service = Service.create(url, qname);
			proxyObject = service.getPort(interfaceTye);
			BindingProvider bindingProvider = (BindingProvider) proxyObject;
			String tmUrl = urlString.toLowerCase();
			int lastInx = tmUrl.contains("?wsdl") ? tmUrl.indexOf("?wsdl") : urlString.length();
			bindingProvider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,urlString.substring(0, lastInx));
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		return proxyObject;

	}

	public  void setAuthenticationToken(Map<String, Object> req_ctx, String userName, String password) {
		req_ctx.put(BindingProvider.USERNAME_PROPERTY, userName);
		req_ctx.put(BindingProvider.PASSWORD_PROPERTY, password);
	}
	
	public void setTimeOutValues(Map<String, Object> req_ctx,SystemName systemName){
		
		EnvironmentProperties prop = new PropertyReader().getDefaultProperties();
		int timeOut = 60000;
		if(systemName.equals(SystemName.XNG_PORT)){
			timeOut = null != prop.getXngPortTimeOut() ? Integer.valueOf(prop.getXngPortTimeOut()) : timeOut ;
			LOG.info("Setting timeout for XNG Port client is ::"+timeOut);
		}else if(systemName.equals(SystemName.XNG_CONNECTION)){
			timeOut = null != prop.getXngConnectionTimeOut() ? Integer.valueOf(prop.getXngConnectionTimeOut()) : timeOut ;
			LOG.info("Setting timeout for XNG Cnnection client is ::"+timeOut);
		}else if(systemName.equals(SystemName.PREMISE_MASTER)){
			timeOut = null != prop.getPremiseTimeOut() ? Integer.valueOf(prop.getPremiseTimeOut()) : timeOut ;
			LOG.info("Setting timeout for Premise Master client is ::"+timeOut);
		}
		
		req_ctx.put("javax.xml.ws.client.connectionTimeout", timeOut); 
		req_ctx.put("javax.xml.ws.client.receiveTimeout", timeOut); 
	}
	
	public <T> void addLogHandler(T proxyObject){
		try{
			
			boolean isRequestLogRequire = Boolean.valueOf(new PropertyReader().getDefaultProperties().getSoapLogRequired());
			if(isRequestLogRequire){
			BindingProvider bp = (BindingProvider) proxyObject;
			Binding binding = bp.getBinding();
	
			// Add the logging handler
			List handlerList = binding.getHandlerChain();
			if (handlerList == null)
			   handlerList = new ArrayList();
			LoggingHandler loggingHandler = new LoggingHandler();
			handlerList.add(loggingHandler);
			binding.setHandlerChain(handlerList);
			}
		}catch(Exception ex){
		    
	  }
	}
}
