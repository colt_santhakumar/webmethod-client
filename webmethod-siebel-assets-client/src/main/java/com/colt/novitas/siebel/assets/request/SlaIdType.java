package com.colt.novitas.siebel.assets.request;


public enum SlaIdType {

	SLA_01("SLA_01"), 
	SLA_02("SLA_02");
	private final String value;

	SlaIdType(String v) {
		value = v;
	}

	public String value() {
		return value;
	}

	public static SlaIdType fromValue(String v) {
		for (SlaIdType c : SlaIdType.values()) {
			if (c.value.equals(v)) {
				return c;
			}
		}
		throw new IllegalArgumentException(v);
	}

}
