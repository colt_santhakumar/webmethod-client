package com.colt.novitas.siebel.assets.request;

import java.util.List;
import java.util.Map;

public class SiebelAssestRequest {

	private String serviceId;
	private String serviceName;
	private String ocn;
	private String novitasServiceId;
	private String slaId;
	private String status;
	private Map<String,String> attributesMap;
	private String urlCallback;
	private String pamFlag;
	private String resourceId;
	private String productName;
	private List<Contact> contactDetails;

	/**
	 * @return the serviceId
	 */
	public String getServiceId() {
		return serviceId;
	}

	/**
	 * @param serviceId
	 *            the serviceId to set
	 */
	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	/**
	 * @return the ocn
	 */
	public String getOcn() {
		return ocn;
	}

	/**
	 * @param ocn
	 *            the ocn to set
	 */
	public void setOcn(String ocn) {
		this.ocn = ocn;
	}

	/**
	 * @return the novitasServiceId
	 */
	public String getNovitasServiceId() {
		return novitasServiceId;
	}

	/**
	 * @param novitasServiceId
	 *            the novitasServiceId to set
	 */
	public void setNovitasServiceId(String novitasServiceId) {
		this.novitasServiceId = novitasServiceId;
	}

	/**
	 * @return the slaId
	 */
	public String getSlaId() {
		return slaId;
	}

	/**
	 * @param slaId
	 *            the slaId to set
	 */
	public void setSlaId(String slaId) {
		this.slaId = slaId;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the urlCallback
	 */
	public String getUrlCallback() {
		return urlCallback;
	}

	/**
	 * @param urlCallback
	 *            the urlCallback to set
	 */
	public void setUrlCallback(String urlCallback) {
		this.urlCallback = urlCallback;
	}

	/**
	 * @return the attributesMap
	 */
	public Map<String, String> getAttributesMap() {
		return attributesMap;
	}

	/**
	 * @param attributesMap the attributesMap to set
	 */
	public void setAttributesMap(Map<String, String> attributesMap) {
		this.attributesMap = attributesMap;
	}

	/**
	 * @return the pamFlag
	 */
	public String getPamFlag() {
		return pamFlag;
	}

	/**
	 * @param pamFlag the pamFlag to set
	 */
	public void setPamFlag(String pamFlag) {
		this.pamFlag = pamFlag;
	}

	/**
	 * @return the resourceId
	 */
	public String getResourceId() {
		return resourceId;
	}

	/**
	 * @param resourceId the resourceId to set
	 */
	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}


	public List<Contact> getContactDetails() {
		return contactDetails;
	}

	public void setContactDetails(List<Contact> contactDetails) {
		this.contactDetails = contactDetails;
	}

	@Override
	public String toString() {
		return "SiebelAssestRequest [serviceId=" + serviceId + ", ocn=" + ocn
				+ ", novitasServiceId=" + novitasServiceId + ", slaId=" + slaId
				+ ", status=" + status + ", attributesMap=" + attributesMap
				+ ", urlCallback=" + urlCallback + ", pamFlag=" + pamFlag
				+ ", resourceId=" + resourceId + ", productName=" + productName
				+ ", contactDetails=" + contactDetails + "]";
	}

    

	
	

	
}
