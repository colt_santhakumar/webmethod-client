package com.colt.novitas.siebel.assets.request;

import java.io.Serializable;

public class Contact implements Serializable{
  
	private String title;
	private String firstName;
	private String lastName;
	private String mobilePhone;
	private String landlinePhone;
	private String email;
	private String preferredLanguage;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getMobilePhone() {
		return mobilePhone;
	}
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}
	public String getLandlinePhone() {
		return landlinePhone;
	}
	public void setLandlinePhone(String landlinePhone) {
		this.landlinePhone = landlinePhone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPreferredLanguage() {
		return preferredLanguage;
	}
	public void setPreferredLanguage(String preferredLanguage) {
		this.preferredLanguage = preferredLanguage;
	}
	@Override
	public String toString() {
		return "ContactDetails [title=" + title + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", mobilePhone=" + mobilePhone
				+ ", landlinePhone=" + landlinePhone + ", email=" + email
				+ ", preferredLanguage=" + preferredLanguage + "]";
	}
	
	
	
}
