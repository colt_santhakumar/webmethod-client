package com.colt.novitas.siebel.assests.client;

import java.util.List;
import java.util.Map;

import javax.xml.ws.BindingProvider;

import org.apache.log4j.Logger;

import com.colt.novitas.property.SystemName;
import com.colt.novitas.siebel.assets.request.Contact;
import com.colt.novitas.siebel.assets.request.SiebelAssestRequest;
import com.colt.novitas.soap.SoapClient;
import com.novitas.xml.coltnovitasassetmgmt.AssetAttributes;
import com.novitas.xml.coltnovitasassetmgmt.AssetMessage;
import com.novitas.xml.coltnovitasassetmgmt.AssetMgmtAsset;
import com.novitas.xml.coltnovitasassetmgmt.AssetMgmtAssetHeader;
import com.novitas.xml.coltnovitasassetmgmt.AssetMgmtAssetXa;
import com.novitas.xml.coltnovitasassetmgmt.ContactType;
import com.novitas.xml.coltnovitasassetmgmt.HeaderType;
import com.novitas.xml.coltnovitasassetmgmt.ListOfAssetManagement;
import com.novitas.xml.coltnovitasassetmgmt.ListOfContactType;

import net.colt.xml.ns.cse.v1.StatusType;
import net.colt.xml.ns.novitas.assetmgmt.v1.AssetMgmt;
import net.colt.xml.ns.novitas.assetmgmt.v1.AssetMgmtPortType;
import net.colt.xml.ns.novitas.assetmgmt.v1.AssetMgmtRequest;
import net.colt.xml.ns.novitas.assetmgmt.v1.AssetMgmtSyncResponse;

public class SiebelAssetsClient {

	private static final Logger LOG = Logger.getLogger(SiebelAssetsClient.class
			.getName());

	private String SIEBEL_SOAP_URL = "http://localhost:8099/";
	private String SIEBEL_SOAP_USERNAME = "jBPMUser";
	private String SIEBEL_SOAP_PASSWORD = "jBPMUser";

	public SiebelAssetsClient(final String SIEBEL_SOAP_URL) {
		this.SIEBEL_SOAP_URL = SIEBEL_SOAP_URL;
	}

	public SiebelAssetsClient(String SIEBEL_SOAP_URL,
			String SIEBEL_SOAP_USERNAME, String SIEBEL_SOAP_PASSWORD) {
		this.SIEBEL_SOAP_URL = SIEBEL_SOAP_URL;
		this.SIEBEL_SOAP_USERNAME = SIEBEL_SOAP_USERNAME;
		this.SIEBEL_SOAP_PASSWORD = SIEBEL_SOAP_PASSWORD;
	}

	public SiebelAssetsClient() {

	}

	public String receiveAssetMgmtRequest(SiebelAssestRequest request) {
		LOG.info("receiveAssetMgmtRequest ------>"+request);
		SoapClient soapClient = new SoapClient();
		AssetMgmtRequest assetMgmtRequest = new AssetMgmtRequest();
		
		AssetMessage assetMessage = new AssetMessage();
		assetMgmtRequest.setAssetMessage(assetMessage);
		ListOfAssetManagement listOfAssetManagement = new ListOfAssetManagement();
		assetMessage.setListOfAssetManagement(listOfAssetManagement);
		List<AssetMgmtAssetHeader> assetMgmtAssetHeaderList = listOfAssetManagement.getAssetMgmtAssetHeader();
		AssetMgmtAssetHeader assetMgmtAssetHeader = new AssetMgmtAssetHeader();
		assetMgmtAssetHeaderList.add(assetMgmtAssetHeader);
		List<AssetMgmtAsset> assetMgmtAsset = assetMgmtAssetHeader.getAssetMgmtAsset();
		AssetMgmtAsset asset = new AssetMgmtAsset();
		assetMgmtAsset.add(asset);
		
		HeaderType serviceHeader = new HeaderType();
		serviceHeader.setSenderSystem("Novitas");
		asset.setServiceHeader(serviceHeader);
		asset.setServiceId(request.getServiceId());
		asset.setSLAProductId(request.getSlaId());
		asset.setPAMFlag(request.getPamFlag());
		if(null != request.getNovitasServiceId()){
			asset.setParentServiceID(request.getNovitasServiceId());
		}
		
		asset.setStatus(request.getStatus());
		asset.setUrlCallback(request.getUrlCallback());
		asset.setProductName(request.getProductName());
		asset.setOCN(request.getOcn());
		asset.setResourceId(request.getResourceId());
		AssetAttributes assetAttributes = new AssetAttributes();
		List<AssetMgmtAssetXa> assetMgmtAssetXaList = assetAttributes.getAssetMgmtAssetXa();
		getNameValuePairList(request.getAttributesMap(),"",assetMgmtAssetXaList);
		asset.setAssetAttributes(assetAttributes);
		asset.setCustomerReference(request.getServiceName());
		if(null != request.getContactDetails() && request.getContactDetails().size() > 0){
		  ListOfContactType listOfContacts = new ListOfContactType();
		  asset.setListOfContacts(listOfContacts);
		  List<ContactType> contactTypes = listOfContacts.getContacts();
		  populateContactTypes(request.getContactDetails(), contactTypes);
		}
		
		try {
			LOG.info("receiveAssetMgmtRequest  calling sibel system>>>>> ::::" + SIEBEL_SOAP_URL);
			AssetMgmtPortType servicePort = soapClient.getServiceObject(SIEBEL_SOAP_URL, AssetMgmt.SERVICE,
					AssetMgmtPortType.class);
		    Map<String, Object> req_ctx = ((BindingProvider) servicePort).getRequestContext();
		    soapClient.setAuthenticationToken(req_ctx, SIEBEL_SOAP_USERNAME, SIEBEL_SOAP_PASSWORD);
		    soapClient.setTimeOutValues(req_ctx, SystemName.XNG_PORT);
		    soapClient.addLogHandler(servicePort);
			
		    AssetMgmtSyncResponse assetMgmtSyncResponse = servicePort.receiveAssetMgmtRequest(assetMgmtRequest);
		
		    net.colt.xml.ns.cse.v1.HeaderType outputHeader = assetMgmtSyncResponse.getAssetMsgSyncResponse().getOutputHeader();
		
			if(outputHeader.getStatus().equals(StatusType.SUCCESS)){
				LOG.info("receiveAssetMgmtResponse transactionId >>>>>");
				return outputHeader.getTransactionId();
			}
			return null;
		}catch(Exception ex){
			LOG.error("receiveAssetMgmtResponse >>>>>"+ex.getMessage());
			return null;
		}
	}
	
	private void getNameValuePairList(Map<String,String> attributeMap,String operationName,List<AssetMgmtAssetXa> assetMgmtAssetXaList){
		
		if (null != attributeMap && !attributeMap.isEmpty()) {
			
			// Removing "A06_B-AccountName" because value is not in English
			if(attributeMap.containsKey("A06_B-AccountName")) {
				attributeMap.remove("A06_B-AccountName");
			}
			for( Map.Entry<String,String> entry:attributeMap.entrySet()){
				AssetMgmtAssetXa assetMgmtAssetXa = new AssetMgmtAssetXa();
				assetMgmtAssetXa.setAttributeName(entry.getKey());
				assetMgmtAssetXa.setDisplayName(entry.getKey().toUpperCase());
				assetMgmtAssetXa.setAttributeValue(entry.getValue());
				assetMgmtAssetXaList.add(assetMgmtAssetXa);
				
				// Adding key "A06_B-AccountName" with with value in English 
				if(entry.getKey().equalsIgnoreCase("A05_A-AccountName") && null != entry.getValue()) {
					AssetMgmtAssetXa assetMgmtAssetXaTemp = new AssetMgmtAssetXa();
					assetMgmtAssetXaTemp.setAttributeName("A06_B-AccountName");
					assetMgmtAssetXaTemp.setDisplayName("A06_B-AccountName".toUpperCase());
					assetMgmtAssetXaTemp.setAttributeValue(entry.getValue());
					assetMgmtAssetXaList.add(assetMgmtAssetXaTemp);
				}
			}
		}
		
	}
	
	private void populateContactTypes(List<Contact> contactDetails, List<ContactType> contactTypes){
		LOG.info("Adding contact info to Siebel Request");
		if(null != contactDetails && contactDetails.size() > 0){
			for (Contact contact : contactDetails) {
				ContactType contactType = new ContactType();
				contactType.getContactRole().add("PAM");
				contactType.setContactMethod("Email");
				contactType.setTitle(contact.getTitle());;
				contactType.setFirstName(contact.getFirstName());
				contactType.setLastName(contact.getLastName());;
				contactType.setMobilePhone(contact.getMobilePhone());
				contactType.setWorkPhone(contact.getLandlinePhone());
				contactType.setEmail(contact.getEmail());
				contactType.setPreferredLanguageCode(contact.getPreferredLanguage());
				
				contactTypes.add(contactType);
			}
		}
	   LOG.info("Added contact info to siebel size is :: "+contactTypes.size());
	}
		
}
