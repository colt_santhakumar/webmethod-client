package com.colt.novitas.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.colt.novitasresourceinventory.ports.v1.GetPortsSummaryResponse2;
import net.colt.xml.ns.cse.v1.StatusType;
import net.colt.xml.ns.novitasxngportinterface.v1.PortAttribType;
import net.colt.xml.ns.novitasxngportinterface.v1.PortSummType;
import net.colt.xml.ns.novitasxngportinterface.v1.ServiceOutput3;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.colt.novitas.ports.response.PortResponse;
import com.colt.novitas.ports.response.ProductXNGResponse;

public class PortRequestResponseUtil {
	
	private static final Logger LOG = Logger.getLogger(PortRequestResponseUtil.class.getName());
	
	public static PortResponse  getPortResponse(PortAttribType  portAttribType,PortResponse portResponse){
		LOG.debug("Start >>>  getPortResponse()");
		
		portResponse.setPortId(portAttribType.getPortID());
		portResponse.setPortName(portAttribType.getPortName());
        portResponse.setStatus(portAttribType.getStatus().value());
		portResponse.setDeviceId(portAttribType.getDeviceID());
		portResponse.setIpAddr(portAttribType.getIpAddr());
        portResponse.setSmartsPortName(portAttribType.getSmartsPortName());
		
		portResponse.setPresentationLabel(portAttribType.getPanelName());
		portResponse.setPresentationCabinet(portAttribType.getCabinetName());
		
		
		LOG.debug("End >>>  getPortResponse()");
		return portResponse;
	}
   
	public static void  getPortResponseList(List<PortAttribType>  portAttribTypes,List<PortResponse> portResponseList,Map<String, String> requestParamMap) {
		LOG.info("Start >>>  getPortResponseList()");
		
		for (PortAttribType portAttribType : portAttribTypes) {
			PortResponse portResponse = new PortResponse();
			
			portResponse.setPortId(portAttribType.getPortID());
			portResponse.setPortName(portAttribType.getPortName());
	        portResponse.setStatus(portAttribType.getStatus().value());
			portResponse.setDeviceId(portAttribType.getDeviceID());
			portResponse.setIpAddr(portAttribType.getIpAddr());
	        portResponse.setSmartsPortName(portAttribType.getSmartsPortName());
			portResponse.setPresentationLabel(portAttribType.getPanelName());
			portResponse.setPresentationCabinet(portAttribType.getCabinetName());
			
			portResponse.setPortType(requestParamMap.get("type"));
			portResponse.setConnector(requestParamMap.get("connector"));
			portResponse.setBandwidth(requestParamMap.get("bandwidth"));
			portResponse.setSiteId(requestParamMap.get("siteId"));
            
			portResponseList.add(portResponse);
		}
	}
	
}
