package com.colt.novitas.util;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.BindingProvider;

/**
*
* @author Dharma Rao
*
*/
public class SoapClientUtil {

	public static <T> T getServiceObjet(String urlString, QName qname,Class<T> interfaceTye){
		
		 T proxyObject = null;
		try {
			URL url = new URL(urlString);
			Service service = Service.create(url, qname);
			proxyObject = service.getPort(interfaceTye);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	    
	    return proxyObject;

	}
	
	public static void setAuthenticationToken(Map<String, Object> req_ctx,String userName,String password){
	        req_ctx.put(BindingProvider.USERNAME_PROPERTY, userName);
	        req_ctx.put(BindingProvider.PASSWORD_PROPERTY, password);
	}

}
