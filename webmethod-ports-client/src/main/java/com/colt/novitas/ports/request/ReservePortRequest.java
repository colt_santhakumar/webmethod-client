package com.colt.novitas.ports.request;


public class ReservePortRequest {

	private String portID;
	private String serviceID;
	private String bandWidth;
	private String customerName;
	private String awsRef;
	private String urlCallback;
	
	public ReservePortRequest(){
		
	}

	public ReservePortRequest(String portID, String serviceID,
			String bandWidth, String customerName, String awsRef,
			String urlCallback) {
		super();
		this.portID = portID;
		this.serviceID = serviceID;
		this.bandWidth = bandWidth;
		this.customerName = customerName;
		this.awsRef = awsRef;
		this.urlCallback = urlCallback;
	}


	public String getAwsRef() {
		return awsRef;
	}

	public void setAwsRef(String awsRef) {
		this.awsRef = awsRef;
	}

	public String getPortID() {
		return portID;
	}

	public void setPortID(String portID) {
		this.portID = portID;
	}

	public String getServiceID() {
		return serviceID;
	}

	public void setServiceID(String serviceID) {
		this.serviceID = serviceID;
	}

	public String getBandWidth() {
		return bandWidth;
	}

	public void setBandWidth(String bandWidth) {
		this.bandWidth = bandWidth;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getUrlCallback() {
		return urlCallback;
	}

	public void setUrlCallback(String urlCallback) {
		this.urlCallback = urlCallback;
	}

	@Override
	public String toString() {
		return "ReservePortRequest [portID=" + portID + ", serviceID=" + serviceID + ", bandWidth=" + bandWidth
				+ ", customerName=" + customerName + ", urlCallback=" + urlCallback + "]";
	}

}
