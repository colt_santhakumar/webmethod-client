
package com.colt.novitas.ports.request;

public enum NovitasPortType {

    HUNDRED_BASE_TX("100BASE-TX"),
    THOUSAND_BASE_T("1000BASE-T"),
    THOUSAND_BASE_TX("1000BASE-TX"),
    THOUSAND_BASE_SX("1000BASE-SX"),
    THOUSAND_BASE_LX("1000BASE-LX"),
    TEN_GBASE_SR("10GBASE-SR"),
    TEN_GBASE_SX("10GBASE-LR"),
    TEN_GBase_ER("10GBASE-ER");
   
    private final String value;

    NovitasPortType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static NovitasPortType fromValue(String v) {
        for (NovitasPortType c: NovitasPortType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
