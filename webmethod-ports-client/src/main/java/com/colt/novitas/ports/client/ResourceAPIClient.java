package com.colt.novitas.ports.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.ws.BindingProvider;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.colt.novitas.ports.request.NovitasPortType;
import com.colt.novitas.ports.request.XNGPortType;
import com.colt.novitas.ports.response.PortResponse;
import com.colt.novitas.property.SystemName;
import com.colt.novitas.soap.SoapClient;
import com.colt.novitas.util.PortRequestResponseUtil;

import net.colt.novitasresourceinventory.ports.v1.GetPortDetailsRequest;
import net.colt.novitasresourceinventory.ports.v1.GetPortDetailsResponse2;
import net.colt.novitasresourceinventory.ports.v1.GetPortsPortType;
import net.colt.novitasresourceinventory.ports.v1.GetPortsRequest;
import net.colt.novitasresourceinventory.ports.v1.GetPortsResponse;
import net.colt.novitasresourceinventory.ports.v1.GetPortsSummaryRequest;
import net.colt.novitasresourceinventory.ports.v1.GetPortsSummaryResponse2;
import net.colt.novitasresourceinventory.ports.v1.Port;
import net.colt.novitasresourceinventory.ports.v1.ReleasePortRequest;
import net.colt.novitasresourceinventory.ports.v1.ReleasePortResponse2;
import net.colt.novitasresourceinventory.ports.v1.ReservePortRequest;
import net.colt.novitasresourceinventory.ports.v1.ReservePortResponse2;
import net.colt.xml.ns.cse.v1.HeaderType;
import net.colt.xml.ns.cse.v1.StatusType;
import net.colt.xml.ns.novitasxngportinterface.v1.ConnectorType;
import net.colt.xml.ns.novitasxngportinterface.v1.GetportdetailsInputType;
import net.colt.xml.ns.novitasxngportinterface.v1.GetportsInputType;
import net.colt.xml.ns.novitasxngportinterface.v1.GetportssummaryInputType;
import net.colt.xml.ns.novitasxngportinterface.v1.InputHeaderType;
import net.colt.xml.ns.novitasxngportinterface.v1.PortAttribType;
import net.colt.xml.ns.novitasxngportinterface.v1.ReleaseportInputType;
import net.colt.xml.ns.novitasxngportinterface.v1.ReleasevlanInputType;
import net.colt.xml.ns.novitasxngportinterface.v1.ReserveportInputType;
import net.colt.xml.ns.novitasxngportinterface.v1.ReservevlanInputType;
import net.colt.xml.ns.novitasxngportinterface.v1.ServiceOutput;
import net.colt.xml.ns.novitasxngportinterface.v1.ServiceOutput3;
import net.colt.xml.ns.novitasxngportinterface.v1.ServiceOutput6;

/**
 *
 * @author Dharma Rao
 *
 */
public class ResourceAPIClient {

	//private String RESOURCE_SOAP_URL = "http://wbmeaiis21:6666/ws/ColtNovitasResourceInventory.common.webSvcProvider:port/ColtNovitasResourceInventory_common_webSvcProvider_getPorts_Port";
	private String RESOURCE_SOAP_URL = "http://amsnov02:8080/webmethod-mock-api/xngport?wsdl";
	private String RESOURCE_SOAP_USERNAME = "jBPMUser";
	private String RESOURCE_SOAP_PASSWORD = "jBPMUser";
	private static final Logger LOG = Logger.getLogger(ResourceAPIClient.class.getName());
	
	private SoapClient soapClient =new SoapClient();

	public ResourceAPIClient(final String RESOURCE_SOAP_URL) {
		this.RESOURCE_SOAP_URL = RESOURCE_SOAP_URL;
	}

	public ResourceAPIClient(String RESOURCE_SOAP_URL, String RESOURCE_SOAP_USERNAME, String RESOURCE_SOAP_PASSWORD) {
		this.RESOURCE_SOAP_URL = RESOURCE_SOAP_URL;
		this.RESOURCE_SOAP_USERNAME = RESOURCE_SOAP_USERNAME;
		this.RESOURCE_SOAP_PASSWORD = RESOURCE_SOAP_PASSWORD;
	}

	public ResourceAPIClient() {
	}

	public Object getPorts(Map<String, String> requestParamMap) {
		LOG.info("Start >>>  ResourceAPIClient.getPorts()");
		System.out.println("Request Map >>>>>>>>>>>"+requestParamMap);
		List<PortResponse> portsList = new ArrayList<PortResponse>();
		GetPortsRequest getPortsRequest = new GetPortsRequest();
		GetportsInputType getportsInputType = new GetportsInputType();
		getPortsRequest.setGetPortsRequest(getportsInputType);

		// requestParamMap.put("status", "AVAILABLE");
		InputHeaderType inputHeaderType = new InputHeaderType();
		// inputHeaderType.setCreationTime(); need to find value
		inputHeaderType.setSenderSystem("Novitas");
		getportsInputType.setServiceHeader(inputHeaderType);

		// setting siteID
		String siteId = requestParamMap.get("siteId");
		LOG.info("Request Map site >>>>>>>>>>>"+siteId);
		if (StringUtils.isBlank(siteId)) {
			return null;
		}
		getportsInputType.setSiteID(siteId);
		// setting bandWidth
		String bandWidth =requestParamMap.get("bandwidth");
		LOG.info("Request Map bandWidth >>>>>>>>>>>"+bandWidth);
		if (StringUtils.isBlank(bandWidth))
			return null;
		getportsInputType.setBandWidth(String.valueOf(bandWidth));

		// setting connectorType
		ConnectorType connector = getXNGConnectorType(requestParamMap.get("connector"));
		LOG.info("Request Map connector  >>>>>>>>>>>"+connector);
		if (null != connector) {
			getportsInputType.setConnectorType(connector);
			
		} else {
			return null;
		}

		// setting portType
		String portType = getXNGPortType(requestParamMap.get("portType"));
		LOG.info("Request Map portType  >>>>>>>>>>>"+portType);
		if (StringUtils.isBlank(portType))
			return null;
		getportsInputType.setPortType(portType);
		
		GetPortsResponse getPortsResponse;
		try {
			GetPortsPortType servicePort = soapClient.getServiceObject(RESOURCE_SOAP_URL, Port.SERVICE,
					GetPortsPortType.class);
			Map<String, Object> req_ctx = ((BindingProvider) servicePort).getRequestContext();
			soapClient.setAuthenticationToken(req_ctx, RESOURCE_SOAP_USERNAME, RESOURCE_SOAP_PASSWORD);
			soapClient.addLogHandler(servicePort);
			soapClient.setTimeOutValues(req_ctx, SystemName.XNG_PORT);
			
			getPortsResponse = servicePort.getPortsList(getPortsRequest);
			StatusType statusType =  getPortsResponse.getGetPortsResponse().getServiceOutput().getOutputHeader().getStatus();
			List<PortAttribType> portAttributeList = getPortsResponse.getGetPortsResponse().getServiceOutput().getResults().getPort();
			if (statusType.equals(StatusType.SUCCESS) && !portAttributeList.isEmpty() ) {
				LOG.info("End >>>  ResourceAPIClient.getPorts()");
			PortRequestResponseUtil.getPortResponseList(portAttributeList, portsList,requestParamMap);
			return portsList;
			}else{
				LOG.info("Failed to retrive Available Ports :: "+getPortsResponse.getGetPortsResponse().getServiceOutput().getOutputHeader().getErrorMessage().getErrorDescription());
				return portsList;
			}
		} catch (Exception ex) {
			LOG.error("Error >>>  ResourceAPIClient.getPorts()" + ex.getMessage());
			return null;
		}
	}

	public Object getPort(String portId) {
		LOG.info("Start >>>  ResourceAPIClient.getPort()");
		GetPortDetailsRequest getPortRequest = new GetPortDetailsRequest();
		GetportdetailsInputType getportInputType = new GetportdetailsInputType();
		getPortRequest.setGetPortDetailsRequest(getportInputType);
		InputHeaderType inputHeaderType = new InputHeaderType();
		inputHeaderType.setSenderSystem("Novitas");
		getportInputType.setServiceHeader(inputHeaderType);
		PortResponse portResponse = new PortResponse();
		if (portId == null)
			return "FAILED";
		getportInputType.setPortID(portId);
		try {
			GetPortsPortType servicePort = soapClient.getServiceObject(RESOURCE_SOAP_URL, Port.SERVICE,
					GetPortsPortType.class);
			Map<String, Object> req_ctx = ((BindingProvider) servicePort).getRequestContext();
			soapClient.setAuthenticationToken(req_ctx, RESOURCE_SOAP_USERNAME, RESOURCE_SOAP_PASSWORD);
			soapClient.addLogHandler(servicePort);
			soapClient.setTimeOutValues(req_ctx, SystemName.XNG_PORT);
			 GetPortDetailsResponse2 response =servicePort.getPortDetails(getPortRequest);
			 ServiceOutput serviceOutPut = response.getGetPortDetailsResponse().getServiceOutput();
			 if(serviceOutPut.getOutputHeader().getStatus().equals(StatusType.SUCCESS)){
				 return PortRequestResponseUtil.getPortResponse(serviceOutPut.getPortinfo(), portResponse);
			 }else{
				 LOG.info("Failed to retrive Port details :: "+serviceOutPut.getOutputHeader().getErrorMessage().getErrorDescription());
				 return "FAILED";
			 }
		} catch (Exception ex) {
			return "FAILED";
		}

	}

	public Object updatePort(String resourcePortId){
		LOG.info("Start >>>  ResourceAPIClient.releasePort()");
		ReleasePortRequest releasePortRequest = new ReleasePortRequest();
		ReleaseportInputType releasePortInputRequest = new ReleaseportInputType();
		InputHeaderType serviceHeader = new InputHeaderType();
		serviceHeader.setSenderSystem("Novitas");
		releasePortInputRequest.setServiceHeader(serviceHeader);

		if (resourcePortId == null)
			return "FAILED";
		releasePortInputRequest.setPortID(resourcePortId);
		releasePortRequest.setReleasePortRequest(releasePortInputRequest);

		try {
			GetPortsPortType servicePort = soapClient.getServiceObject(RESOURCE_SOAP_URL, Port.SERVICE,GetPortsPortType.class);
			Map<String, Object> req_ctx = ((BindingProvider) servicePort).getRequestContext();
			soapClient.setAuthenticationToken(req_ctx, RESOURCE_SOAP_USERNAME, RESOURCE_SOAP_PASSWORD);
			soapClient.addLogHandler(servicePort);
			soapClient.setTimeOutValues(req_ctx, SystemName.XNG_PORT);
			ReleasePortResponse2 releasePortResponse2 = servicePort.releasePort(releasePortRequest);
			HeaderType outputHeader = releasePortResponse2.getReleasePortResponse().getServiceOutput().getOutputHeader();
			if(outputHeader.getStatus().equals(StatusType.SUCCESS) ){
				return outputHeader.getStatus().value();
			}
			LOG.info("Failed to release the Port :: "+outputHeader.getErrorMessage().getErrorDescription());
			return "FAILED";

		} catch (Exception ex) {
			LOG.error("Error >>>  ResourceAPIClient.releasePort()" + ex.getMessage());
			return "FAILED";
		}

	}

	public Object reservePort(com.colt.novitas.ports.request.ReservePortRequest request) {
		LOG.info("Start >>>  ResourceAPIClient.reservePort()");
		ReservePortRequest reservePortRequest = new ReservePortRequest();
		ReserveportInputType reserveportInputType = new ReserveportInputType();
		reservePortRequest.setReservePortRequest(reserveportInputType);
		InputHeaderType reserveHeader = new InputHeaderType();
		reserveportInputType.setServiceHeader(reserveHeader);
		reserveHeader.setSenderSystem("Novitas");

		// setting portId
		reserveportInputType.setPortID(request.getPortID());

		// setting serviceId
		reserveportInputType.setServiceID(request.getServiceID());

		// setting bandWidth
		reserveportInputType.setBandWidth(String.valueOf(request.getBandWidth()));

		// setting customerName
		reserveportInputType.setCustomerName(request.getCustomerName());

		// setting urlCallBack
		reserveportInputType.setUrlCallback(request.getUrlCallback());
		
		//Aws Dedicated Required
		if(StringUtils.isNotBlank(request.getAwsRef())){
			reserveportInputType.setAwsConnectionID(request.getAwsRef());
		}

		try {
			GetPortsPortType servicePort = soapClient.getServiceObject(RESOURCE_SOAP_URL, Port.SERVICE,
					GetPortsPortType.class);
			Map<String, Object> req_ctx = ((BindingProvider) servicePort).getRequestContext();
			soapClient.setAuthenticationToken(req_ctx, RESOURCE_SOAP_USERNAME, RESOURCE_SOAP_PASSWORD);
			soapClient.addLogHandler(servicePort);
			soapClient.setTimeOutValues(req_ctx, SystemName.XNG_PORT);
			ReservePortResponse2 reservePortResponse2 = servicePort.reservePort(reservePortRequest);
			HeaderType outputHeader = reservePortResponse2.getReservePortResponse().getServiceOutput().getOutputHeader();
			if(outputHeader.getStatus().equals(StatusType.SUCCESS) && StringUtils.isNotEmpty(outputHeader.getTransactionId())){
				return outputHeader.getTransactionId();
			}
			LOG.info("Failed to reserve the Port :: "+outputHeader.getErrorMessage().getErrorDescription());
			return "FAILED";
		} catch (Exception ex) {
			LOG.error("Error >>>  ResourceAPIClient.reservePort()" + ex.getMessage());
			return "FAILED";
		}

	}
	
	public String reserveVlan(String circuitId,String serviceId,String bandwidth,String customerName) {
		LOG.info("Start >>> Reserve Vlan");
		net.colt.novitasresourceinventory.ports.v1.ReserveVlanRequest reserveVlanRequest = new net.colt.novitasresourceinventory.ports.v1.ReserveVlanRequest();
		ReservevlanInputType reserveVlanInput = new ReservevlanInputType();
		reserveVlanRequest.setReserveVlanRequest(reserveVlanInput);
		InputHeaderType reserveHeader = new InputHeaderType();
		reserveVlanInput.setServiceHeader(reserveHeader);
		reserveHeader.setSenderSystem("Novitas");

		// setting circuitID
		reserveVlanInput.setCircuitID(circuitId);

		// setting serviceId
		reserveVlanInput.setServiceID(serviceId);

		// setting bandWidth
		reserveVlanInput.setBandWidth(bandwidth);

		// setting customerName
		reserveVlanInput.setCustomerName(customerName);


		try {
			GetPortsPortType servicePort = soapClient.getServiceObject(RESOURCE_SOAP_URL, Port.SERVICE,
					GetPortsPortType.class);
			Map<String, Object> req_ctx = ((BindingProvider) servicePort).getRequestContext();
			soapClient.setAuthenticationToken(req_ctx, RESOURCE_SOAP_USERNAME, RESOURCE_SOAP_PASSWORD);
			soapClient.addLogHandler(servicePort);
			soapClient.setTimeOutValues(req_ctx, SystemName.XNG_PORT);
			ServiceOutput6 serviceOutput = servicePort.reserveVlan(reserveVlanRequest).getReserveVlanResponse().getServiceOutput();
			HeaderType outputHeader = serviceOutput.getOutputHeader();
			if(outputHeader.getStatus().equals(StatusType.SUCCESS) && StringUtils.isNotEmpty(serviceOutput.getVlanID())){
				return serviceOutput.getVlanID();
			}
			LOG.info("Failed to Reserve Vlan :: "+outputHeader.getErrorMessage().getErrorDescription());
			return "FAILED";
		} catch (Exception ex) {
			LOG.error("Error >>>  Reserve Vlan" + ex.getMessage());
			return "FAILED";
		}

	}
	
	public String releaseVlan(String logicalPortId) {
		LOG.info("Start >>> Release Vlan");
		net.colt.novitasresourceinventory.ports.v1.ReleaseVlanRequest releaseVlanRequest = new net.colt.novitasresourceinventory.ports.v1.ReleaseVlanRequest();
		ReleasevlanInputType releaseVlan = new ReleasevlanInputType();
		releaseVlanRequest.setReleaseVlanRequest(releaseVlan);
		InputHeaderType reserveHeader = new InputHeaderType();
		releaseVlan.setServiceHeader(reserveHeader);
		reserveHeader.setSenderSystem("Novitas");
		// setting logicalPortId
		releaseVlan.setLogicalPortID(logicalPortId);
		try {
			GetPortsPortType servicePort = soapClient.getServiceObject(RESOURCE_SOAP_URL, Port.SERVICE,
					GetPortsPortType.class);
			Map<String, Object> req_ctx = ((BindingProvider) servicePort).getRequestContext();
			soapClient.setAuthenticationToken(req_ctx, RESOURCE_SOAP_USERNAME, RESOURCE_SOAP_PASSWORD);
			soapClient.addLogHandler(servicePort);
			soapClient.setTimeOutValues(req_ctx, SystemName.XNG_PORT);
			HeaderType outputHeader = servicePort.releaseVlan(releaseVlanRequest).getReleaseVlanResponse().getServiceOutput().getOutputHeader();
			if(outputHeader.getStatus().equals(StatusType.SUCCESS)){
				return "SUCCESS";
			}
			LOG.info("Failed to Release Vlan :: "+outputHeader.getErrorMessage().getErrorDescription());
			return "FAILED";
		} catch (Exception ex) {
			LOG.error("Error >>>  Release Vlan" + ex.getMessage());
			return "FAILED";
		}

	}

	private ConnectorType getXNGConnectorType(String connectorType) {

		if (connectorType.startsWith("RJ")) {
			return ConnectorType.RJ_45;
		} else if (connectorType.startsWith("FC")) {
			return ConnectorType.FC_PC;
		} else if (connectorType.startsWith("LC")) {
			return ConnectorType.LC_PC;
		} else if (connectorType.startsWith("SC")) {
			return ConnectorType.SC_PC;
		}

		return null;
	}

	private String getXNGPortType(String portType) {

		if (portType.equals(NovitasPortType.THOUSAND_BASE_T.value())) {
			return XNGPortType.THOUSAND_BASE_T.value();
		}else if (portType.equals(NovitasPortType.THOUSAND_BASE_TX.value())) {
			return XNGPortType.THOUSAND_BASE_T.value();
		} else if (portType.equals(NovitasPortType.THOUSAND_BASE_SX.value())) {
			return XNGPortType.THOUSAND_BASE_SX.value();
		} else if (portType.equals(NovitasPortType.THOUSAND_BASE_LX.value())) {
			return XNGPortType.THOUSAND_BASE_LX.value();
		} else if (portType.equals(NovitasPortType.HUNDRED_BASE_TX.value())) {
			return XNGPortType.HUNDRED_BASE_T.value();
		} else if (portType.equals(NovitasPortType.TEN_GBase_ER.value())) {
			return XNGPortType.TEN_GBase_ER.value();
		} else if (portType.equals(NovitasPortType.TEN_GBASE_SR.value())) {
			return XNGPortType.TEN_GBASE_SR.value();
		} else if (portType.equals(NovitasPortType.TEN_GBASE_SX.value())) {
			return XNGPortType.TEN_GBASE_SX.value();
		}
        LOG.info("No mapping found for incoming portType.....>>>");
		return null;
	}
	
	public Object getProductsByLocation(String siteId) {

		LOG.info("Start >>>  ResourceAPIClient.getProductsByLocation()");
		
		net.colt.novitasresourceinventory.ports.v1.GetPortsSummaryRequest getPortsSummaryRequest = new GetPortsSummaryRequest();
		GetportssummaryInputType getportssummaryInputType = new GetportssummaryInputType();

		InputHeaderType inputHeaderType = new InputHeaderType();
		inputHeaderType.setSenderSystem("Novitas");
		getportssummaryInputType.setServiceHeader(inputHeaderType);
		if (siteId == null)
			return "FAILED";
		getportssummaryInputType.setSiteID(siteId);
		getPortsSummaryRequest.setGetPortsSummaryRequest(getportssummaryInputType);
		
		try {
			GetPortsPortType servicePort = soapClient.getServiceObject(RESOURCE_SOAP_URL, Port.SERVICE,
					GetPortsPortType.class);
			Map<String, Object> req_ctx = ((BindingProvider) servicePort).getRequestContext();
			soapClient.setAuthenticationToken(req_ctx, RESOURCE_SOAP_USERNAME, RESOURCE_SOAP_PASSWORD);
			soapClient.addLogHandler(servicePort);
			soapClient.setTimeOutValues(req_ctx, SystemName.XNG_PORT);
			GetPortsSummaryResponse2 getPortsSummaryResponse = servicePort.getPortsSummary(getPortsSummaryRequest);
			ServiceOutput3 response = getPortsSummaryResponse.getGetPortsSummaryResponse().getServiceOutput();
			
			if (response.getOutputHeader().getStatus().equals(StatusType.SUCCESS) && response.getSummresults().getPortsumm().size() > 0) {
				return response.getSummresults().getPortsumm();
			} else {
				LOG.info("Failed to retrive Product Summary details :: "
						+ response.getOutputHeader().getErrorMessage().getErrorDescription());
				return "FAILED";
			}
		} catch (Exception ex) {
			return "FAILED";
		}
	}
	
	public static void main(String[] args) {
		
		ResourceAPIClient port = new ResourceAPIClient();
		
		Map<String, String> requestParamMap = new HashMap<String, String>();
		requestParamMap.put("status", "AVAILABLE");
		requestParamMap.put("bandwidth", "1000" );
		requestParamMap.put("connector", "RJ45");
		requestParamMap.put("portType", "1000BASE-T");
		requestParamMap.put("siteId", "LON99904/096");
		
		//System.out.println("Get Port Details ::::: "+port.getPort("DDF-BB14:V4Q:1"));
		
		System.out.println("Get Port List >>>>>>>>>>" + NovitasPortType.THOUSAND_BASE_T.value());
		
		//System.out.println("Release Port >>>>>>>>>>>>"+port.updatePort("BGT_LB1036:406TRIB3:PT09"));
		
		/*com.colt.novitas.ports.request.ReservePortRequest req =new com.colt.novitas.ports.request.ReservePortRequest();
		req.setBandWidth(new BigInteger("400"));
		req.setCustomerName("Novitas");
		req.setPortID("DDF-BB14:V4Q:1");
		req.setServiceID("BCN000000055");
		req.setUrlCallback("http://lcalhost:8080/");*/
		
		System.out.println("Reserve Port :::: "+port.getProductsByLocation("LON00001/001"));
		
		System.out.println(port.getProductsByLocation("LON00136/001"));
	}
}
