
package com.colt.novitas.ports.request;

public enum XNGPortType {

    HUNDRED_BASE_T("100BaseT"),
    THOUSAND_BASE_T("1000BaseT"),
    THOUSAND_BASE_SX("1000BaseSX"),
    THOUSAND_BASE_LX("1000BaseLX"),
    TEN_GBASE_SR("10GBaseSR"),
    TEN_GBASE_SX("10GBaseLR"),
    TEN_GBase_ER("10GBaseER");
   
    private final String value;

    XNGPortType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static XNGPortType fromValue(String v) {
        for (XNGPortType c: XNGPortType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
