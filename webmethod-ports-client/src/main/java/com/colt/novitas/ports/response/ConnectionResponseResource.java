package com.colt.novitas.ports.response;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class ConnectionResponseResource implements Serializable {

	private static final long serialVersionUID = 1400331818644454200L;

	private String circuitName;
	private String circuitType;
	private Integer evcId;
	private String status;
	private Integer fromPortId;
	private Integer toPortId;
	private Date lastUpdatedAt;
	// VLAN Related
	private String fromVLANMapping;
	private String toVLANMapping;
	private String fromVLANType;
	private String toVLANType;
	private List<ResourceVLANIdRange> fromPortVLANIdRange;
	private List<ResourceVLANIdRange> toPortVLANIdRange;

	public ConnectionResponseResource() {
		super();
	}

	public ConnectionResponseResource(String circuitName, String circuitType, Integer evcId, String status,
			Integer fromPortId, Integer toPortId, String fromUni, String toUni, Date lastUpdatedAt) {
		super();
		this.circuitName = circuitName;
		this.circuitType = circuitType;
		this.evcId = evcId;
		this.status = status;
		this.fromPortId = fromPortId;
		this.toPortId = toPortId;
		this.lastUpdatedAt = lastUpdatedAt;
	}

	public String getCircuitName() {
		return circuitName;
	}

	public void setCircuitName(String circuitName) {
		this.circuitName = circuitName;
	}

	public String getCircuitType() {
		return circuitType;
	}

	public void setCircuitType(String circuitType) {
		this.circuitType = circuitType;
	}

	public Integer getEvcId() {
		return evcId;
	}

	public void setEvcId(Integer evcId) {
		this.evcId = evcId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getFromPortId() {
		return fromPortId;
	}

	public void setFromPortId(Integer fromPortId) {
		this.fromPortId = fromPortId;
	}

	public Integer getToPortId() {
		return toPortId;
	}

	public void setToPortId(Integer toPortId) {
		this.toPortId = toPortId;
	}

	public Date getLastUpdatedAt() {
		return lastUpdatedAt;
	}

	public void setLastUpdatedAt(Date lastUpdatedAt) {
		this.lastUpdatedAt = lastUpdatedAt;
	}

	public String getFromVLANMapping() {
		return fromVLANMapping;
	}

	public void setFromVLANMapping(String fromVLANMapping) {
		this.fromVLANMapping = fromVLANMapping;
	}

	public String getToVLANMapping() {
		return toVLANMapping;
	}

	public void setToVLANMapping(String toVLANMapping) {
		this.toVLANMapping = toVLANMapping;
	}

	public String getFromVLANType() {
		return fromVLANType;
	}

	public void setFromVLANType(String fromVLANType) {
		this.fromVLANType = fromVLANType;
	}

	public String getToVLANType() {
		return toVLANType;
	}

	public void setToVLANType(String toVLANType) {
		this.toVLANType = toVLANType;
	}

	public List<ResourceVLANIdRange> getFromPortVLANIdRange() {
		return fromPortVLANIdRange;
	}

	public void setFromPortVLANIdRange(List<ResourceVLANIdRange> fromPortVLANIdRange) {
		this.fromPortVLANIdRange = fromPortVLANIdRange;
	}

	public List<ResourceVLANIdRange> getToPortVLANIdRange() {
		return toPortVLANIdRange;
	}

	public void setToPortVLANIdRange(List<ResourceVLANIdRange> toPortVLANIdRange) {
		this.toPortVLANIdRange = toPortVLANIdRange;
	}

	@Override
	public String toString() {
		return "ConnectionResponseResource [circuitName=" + circuitName + ", circuitType=" + circuitType + ", evcId="
				+ evcId + ", status=" + status + ", fromPortId=" + fromPortId + ", toPortId=" + toPortId
				+ ", lastUpdatedAt=" + lastUpdatedAt + ", fromVLANMapping=" + fromVLANMapping + ", toVLANMapping="
				+ toVLANMapping + ", fromVLANType=" + fromVLANType + ", toVLANType=" + toVLANType
				+ ", fromPortVLANIdRange=" + fromPortVLANIdRange + ", toPortVLANIdRange=" + toPortVLANIdRange + "]";
	}

}
