package com.colt.novitas.ports.response;

import java.io.Serializable;

public class LocationResponse implements Serializable {

	private static final long serialVersionUID = -1020745780038246984L;

	private Integer id;
	private String name;
	private String address;
	private String type;

	public LocationResponse() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "LocationResponse [id=" + id + ", name=" + name + ", address=" + address + ", type=" + type + "]";
	}

}
