package com.colt.novitas.ports.response;

import java.io.Serializable;

public class ProductXNGResponse implements Serializable {

	private static final long serialVersionUID = -2556409621309704307L;

	private String portType;
	private String bandwidth;
	private String connectorType;

	public ProductXNGResponse() {
		super();
	}

	public ProductXNGResponse(String portType, String bandwidth, String connectorType) {
		super();
		this.portType = portType;
		this.bandwidth = bandwidth;
		this.connectorType = connectorType;
	}

	public String getPortType() {
		return portType;
	}

	public void setPortType(String portType) {
		this.portType = portType;
	}

	public String getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(String bandwidth) {
		this.bandwidth = bandwidth;
	}

	public String getConnectorType() {
		return connectorType;
	}

	public void setConnectorType(String connectorType) {
		this.connectorType = connectorType;
	}
	
	@Override
	public String toString() {
		return "ProductXNGResponse [portType=" + portType + ", bandwidth=" + bandwidth + ", connectorType="
				+ connectorType + "]";
	}

}
