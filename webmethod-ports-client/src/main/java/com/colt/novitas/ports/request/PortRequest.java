package com.colt.novitas.ports.request;

import java.io.Serializable;

public class PortRequest implements Serializable {

	private static final long serialVersionUID = -4642083548643419039L;

	private String status;

	public PortRequest() {
		super();
	}

	public PortRequest(String status) {
		super();
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "PortRequest [status=" + status + "]";
	}

}
