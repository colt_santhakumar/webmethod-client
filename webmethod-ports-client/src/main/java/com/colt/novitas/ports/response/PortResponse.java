package com.colt.novitas.ports.response;

import java.io.Serializable;

public class PortResponse implements Serializable {

	private static final long serialVersionUID = -1296337781774269561L;

	private String portId;
	private String portName;
	private Integer locationId;
	private String deviceId;
	private String portType;
	private String bandwidth;
	private String connector;
	private String presentationLabel;
	private String presentationCabinet;
	private String status;
	private Integer platformId;
	private String siteId;
	private String smartsPortName;
	private String deviceID;
	private String ipAddr;

	public PortResponse() {
		super();
	}

	public PortResponse(String portId, String portName, Integer locationId,
			String deviceId, String portType, String bandwidth,
			String connector, String presentationLabel,
			String presentationCabinet, String status, Integer platformId,
			String siteId, String smartsPortName, String deviceID, String ipAddr) {
		super();
		this.portId = portId;
		this.portName = portName;
		this.locationId = locationId;
		this.deviceId = deviceId;
		this.portType = portType;
		this.bandwidth = bandwidth;
		this.connector = connector;
		this.presentationLabel = presentationLabel;
		this.presentationCabinet = presentationCabinet;
		this.status = status;
		this.platformId = platformId;
		this.siteId = siteId;
		this.smartsPortName = smartsPortName;
		this.deviceID = deviceID;
		this.ipAddr = ipAddr;
	}

	/**
	 * @return the portId
	 */
	public String getPortId() {
		return portId;
	}

	/**
	 * @param portId
	 *            the portId to set
	 */
	public void setPortId(String portId) {
		this.portId = portId;
	}

	/**
	 * @return the portName
	 */
	public String getPortName() {
		return portName;
	}

	/**
	 * @param portName
	 *            the portName to set
	 */
	public void setPortName(String portName) {
		this.portName = portName;
	}

	/**
	 * @return the locationId
	 */
	public Integer getLocationId() {
		return locationId;
	}

	/**
	 * @param locationId
	 *            the locationId to set
	 */
	public void setLocationId(Integer locationId) {
		this.locationId = locationId;
	}

	/**
	 * @return the deviceId
	 */
	public String getDeviceId() {
		return deviceId;
	}

	/**
	 * @param deviceId
	 *            the deviceId to set
	 */
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	/**
	 * @return the portType
	 */
	public String getPortType() {
		return portType;
	}

	/**
	 * @param portType
	 *            the portType to set
	 */
	public void setPortType(String portType) {
		this.portType = portType;
	}

	/**
	 * @return the bandwidth
	 */
	public String getBandwidth() {
		return bandwidth;
	}

	/**
	 * @param bandwidth
	 *            the bandwidth to set
	 */
	public void setBandwidth(String bandwidth) {
		this.bandwidth = bandwidth;
	}

	/**
	 * @return the connector
	 */
	public String getConnector() {
		return connector;
	}

	/**
	 * @param connector
	 *            the connector to set
	 */
	public void setConnector(String connector) {
		this.connector = connector;
	}

	/**
	 * @return the presentationLabel
	 */
	public String getPresentationLabel() {
		return presentationLabel;
	}

	/**
	 * @param presentationLabel
	 *            the presentationLabel to set
	 */
	public void setPresentationLabel(String presentationLabel) {
		this.presentationLabel = presentationLabel;
	}

	/**
	 * @return the presentationCabinet
	 */
	public String getPresentationCabinet() {
		return presentationCabinet;
	}

	/**
	 * @param presentationCabinet
	 *            the presentationCabinet to set
	 */
	public void setPresentationCabinet(String presentationCabinet) {
		this.presentationCabinet = presentationCabinet;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the platformId
	 */
	public Integer getPlatformId() {
		return platformId;
	}

	/**
	 * @param platformId
	 *            the platformId to set
	 */
	public void setPlatformId(Integer platformId) {
		this.platformId = platformId;
	}

	/**
	 * @return the siteId
	 */
	public String getSiteId() {
		return siteId;
	}

	/**
	 * @param siteId
	 *            the siteId to set
	 */
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	/**
	 * @return the smartsPortName
	 */
	public String getSmartsPortName() {
		return smartsPortName;
	}

	/**
	 * @param smartsPortName
	 *            the smartsPortName to set
	 */
	public void setSmartsPortName(String smartsPortName) {
		this.smartsPortName = smartsPortName;
	}

	/**
	 * @return the eviceID
	 */
	public String getDeviceID() {
		return deviceID;
	}

	/**
	 * @param eviceID
	 *            the eviceID to set
	 */
	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}

	/**
	 * @return the ipAddr
	 */
	public String getIpAddr() {
		return ipAddr;
	}

	/**
	 * @param ipAddr
	 *            the ipAddr to set
	 */
	public void setIpAddr(String ipAddr) {
		this.ipAddr = ipAddr;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PortResponse [portId=" + portId + ", portName=" + portName
				+ ", locationId=" + locationId + ", deviceId=" + deviceId
				+ ", portType=" + portType + ", bandwidth=" + bandwidth
				+ ", connector=" + connector + ", presentationLabel="
				+ presentationLabel + ", presentationCabinet="
				+ presentationCabinet + ", status=" + status + ", platformId="
				+ platformId + ", siteId=" + siteId + ", smartsPortName="
				+ smartsPortName + ", eviceID=" + deviceID + ", ipAddr="
				+ ipAddr + "]";
	}

}
